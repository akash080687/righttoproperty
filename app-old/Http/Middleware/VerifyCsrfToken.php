<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

    /**
     * Routes we want to exclude.
     *
     * @var array
     */
    protected $routesToSkipp = [
        '/',
       
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        foreach ($this->routesToSkipp as $route) {
            if ($request->is($route)) {
                return parent::addCookieToResponse($request, $next($request));
            }
        }
        return parent::handle($request, $next);
    }

}
