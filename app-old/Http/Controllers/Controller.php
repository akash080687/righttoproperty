<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Auth;
use App\Models\Report\StateModel;

abstract class Controller extends BaseController {

    use AuthorizesRequests,
        AuthorizesResources,
        DispatchesJobs,
        ValidatesRequests;

    public function __construct() {
        $this->appData = [];
        $this->appData['common_script_view'] = 'common.scripts';
        if (Auth::check()) {
            $user = Auth::user();
            // t($user->toArray());
            //echo $user->id;
            $this->appData['user_id'] = $user->id;
            $this->appData['logged_in_user_boudary_id'] = isset($user->user_boubdary_id) ? $user->user_boubdary_id : '';
            $this->appData['logged_in_user_boudary_type'] = isset($user->user_boundary) ? $user->user_boundary : '';
        }
        $states = StateModel::where(['stt_fl_archive' => 'N', 'is_rtp' => TRUE])->orderBy('stt_state_name')->lists('stt_state_name', 'stt_state_id')->toArray();
        $states = array_merge(array('' => 'Select'), $states);
        $this->appData['states'] = $states;
        // echo url()->current();
    }

    public function pre($param, $do_die = FALSE) {
        echo '<pre>';
        print_r($param);
        echo '</pre><br/>';
        if ($do_die === TRUE) {
            die('STOPPED');
        }
    }

}
