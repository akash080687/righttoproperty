<?php

namespace App\Http\Controllers\ManageRecords;

use App\Http\Controllers\Controller;
use App\Models\Report\ClaimantModel;
use App\Models\Report\ClaimModel;
use App\Models\Report\BlockModel;
use App\Models\Report\VillageModel;
use App\Models\Report\DistrictModel;
use App\Models\Report\PlotModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use DB;

class PlotController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
        $this->appData['section'] = 'plot';
        $this->appData['page'] = 'manage_records';
        $this->appData['include_script_view'][] = 'managerecords.common_scripts';

        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        $this->middleware('auth');
    }

    public function add(Request $request) {
        //echo 'ssfsaf';

        $request->old('state');
        $state = $request->old('state');
        $district = $request->old('district');
        $block = $request->old('block');
        $village = $request->old('plot.vil_id');
        // $claimant = $request->old('claimant');
        $claims = $request->old('plot.claim_id');
        $claimnts = $request->old('claimant');
       // t($claimnts);
        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $this->appData['selected_state'] = isset($state) ? $state : $this->appData['logged_in_user_boudary_id'];
        $this->appData['selected_district'] = $district;
        $this->appData['selected_block'] = $block;
        $this->appData['selected_villages'] = $village;
        $this->appData['selected_claims'] = $claims;
        $this->appData['selected_claimants'] = ($claimnts)  ? implode(',', $claimnts) : '';

        return view('managerecords.plot.add', $this->appData);
    }

    public function edit($id) {
        if (!$id) {
            return 'plot id missing';
        }
        $this->appData['include_script_view'][] = 'managerecords.plot.additional_scripts';
        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $plot_data = PlotModel::where(['ogc_fid' => $id])->get()->toArray();

        //$this->pre($plot_data);
        $wkb_geometry = PlotModel::selectRaw('ST_AsText(wkb_geometry) as wkb_geometry')->where(['ogc_fid' => $id])->get()->toArray();
        $gps_area = PlotModel::selectRaw('(st_area(ST_Transform(st_setsrid(wkb_geometry,4326),utmzone(ST_Centroid(st_setsrid(wkb_geometry,4326)))))/10000) As calc_area_hect_gps')->where(['ogc_fid' => $id])->get()->toArray();
        $this->appData['gps_area'] = isset($gps_area[0]['calc_area_hect_gps']) ? $gps_area[0]['calc_area_hect_gps'] : NULL;
        //$this->pre($wkb_geometry);
        $this->appData['wkb_geometry'] = (isset($wkb_geometry[0]['wkb_geometry'])) ? $wkb_geometry[0]['wkb_geometry'] : '';
        $this->appData['plot_data'] = $plot_data = isset($plot_data[0]) ? $plot_data[0] : '';
        $this->appData['selected_villages'] = $selected_village = isset($plot_data['vil_id']) ? trim($plot_data['vil_id']) : '';
        $this->appData['selected_block'] = $selected_block = VillageModel::where(['vil_id' => "$selected_village"])->value('block_id');
        $this->appData['selected_district'] = $selected_district = BlockModel::where(['id' => $selected_block])->value('dist_id');
        $this->appData['selected_state'] = $selected_state = DistrictModel::where(['id' => $selected_district])->value('state_id');
        $this->appData['selected_claims'] = $selected_claims = isset($plot_data['claim_id']) ? $plot_data['claim_id'] : '';
        //$this->appData['$selected_claimants'] = $selected_claimants = isset($plot_data['claim_id']) ? $plot_data['claim_id'] : '';
        if ($selected_claims) {
            $selected_claimants = ClaimModel::where(['id' => $selected_claims])->value('claimant_id');
            $this->appData['selected_claimants'] = $selected_claimants;
        }

        $this->appData['id'] = $id;
        return view('managerecords.plot.edit', $this->appData);
    }

    public function editGeometry($plot_id, Request $request) {
//        $geojson = $request->input('geoJSON');
        $coords = $request->input('coords');
//        $coords = json_decode($coords)->features[0]->geometry->coordinates[0][0];
//        var_dump(function_exists('implode'));
//        var_dump(function_exists('array_map'));
        $lat_lon_string = implode(',', array_map(function ($entry) {
                    return implode(' ', $entry);
                }, $coords[0][0]));

        $update = PlotModel::where(['ogc_fid' => $plot_id])->update(['wkb_geometry' => DB::raw("st_geomfromtext('MULTIPOLYGON(((" . $lat_lon_string . ")))',4326)")]);
        if ($update) {
            echo 'updated';
        }
    }

    public function plotList(Request $request) {

        $this->appData['appData']['metaTitle'] = $this->metaTitle;

        $posted_data = $request->all();
        //$this->pre($posted_data);
        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];
        $selected_claimants = isset($posted_data['claimant']) ? implode(',', array_filter($posted_data['claimant'])) : '';
        $selected_claims = isset($posted_data['claim']) ? implode(',', $posted_data['claim']) : '';
        $this->appData['selected_claimants'] = $selected_claimants;
        $this->appData['selected_claims'] = $selected_claims;

        $village_id = isset($posted_data['village']) ? $posted_data['village'] : '';
        if (empty($selected_claims) && !empty($selected_claimants)) {

            $res = ClaimModel::selectRaw("array_to_string(array_agg(id), ',') as id_str")->where(['village_id' => "$village_id"])
                            ->whereRaw("claimant_id in ($selected_claimants)")->get()->toArray();
            $selected_claims = isset($res[0]['id_str']) ? $res[0]['id_str'] : '';
        }
        if (isset($selected_claims) && $selected_claims) {
            $results = PlotModel::whereRaw("claim_id in ($selected_claims)")->where(['vil_id' => $posted_data['village']])->orderBy('plt_number', 'asc')->orderBy('ogc_fid', 'desc')->paginate(50);
        } else if ($village_id) {
            $results = PlotModel::where(['vil_id' => $village_id])->orderBy('ogc_fid', 'desc')->paginate(50);
        } else {
            $results = PlotModel::orderBy('ogc_fid', 'desc')->paginate(50);
        }
        $results->appends($posted_data);
        $links = $results->render();
        $results = $results->toArray();
        $plots = isset($results['data']) ? $results['data'] : '';
        //$this->pre($plots);exit;
        $this->appData['plots'] = $plots;
        $this->appData['links'] = $links;
        $this->appData['search'] = isset($posted_data['search']) && $posted_data['search'] == 1 ? $posted_data['search'] : '';
        return view('managerecords.plot.list', $this->appData);
    }

    public function processData(Request $request) {
        $posted_data = $request->all();
        $posted_plot_data = isset($posted_data['plot']) ? $posted_data['plot'] : '';
        $id = $request->get('id');
        if ($id) {
            $res = PlotModel::select('is_old')->where(['ogc_fid' => $id])->get()->toArray();
        }
        if (isset($res[0]['is_old']) && $res[0]['is_old']) {
            $is_old_value = true;
            $claim_field = 'claim';
        } else {
            $is_old_value = false;
            $claim_field = 'claim_id';
        }

        $messages = [
            'plot.plt_number.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The Plot No. field is required!</strong></div>',
            'plot.surveyno.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Survey No should be a number!</strong></div>',
            'plot.compartment_no.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Compartment No should be a number!</strong></div>',
            'plot.areaclaimedh.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Claimed Hectare should be numeric value!</strong></div>',
            'plot.areaapprovedgsh.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Approved GS Hectare should be numeric value!</strong></div>',
            'plot.areaapprovedsdlch.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Approved SDLC Hectare should be a number!</strong></div>',
            'plot.areaapproveddlch.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Approved DLC Hectare should be a number!</strong></div>',
            'plot.areagpssurveyh.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area GPS Survey Hectare  should be a number!</strong></div>',
            'state.validate_state' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>You are not authorized to edit plots for this state</strong></div>',
        ];
        $rules = [
            'plot.plt_number' => 'required',
            'plot.surveyno' => 'integer',
            'plot.compartment_no' => 'integer',
            'plot.areaclaimedh' => 'numeric',
            'plot.areaapprovedgsh' => 'numeric',
            'plot.areaapprovedsdlch' => 'numeric',
            'plot.areaapproveddlch' => 'numeric',
            'plot.areagpssurveyh' => 'numeric',
            'state' => 'validate_state'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        $village_id = isset($posted_plot_data['vil_id']) ? $posted_plot_data['vil_id'] : '';
        $village_old_id = ($village_id) ? VillageModel::where(['vil_id' => "$village_id"])->value('old_id') : '';
        $claim_id = isset($posted_plot_data['claim_id']) ? $posted_plot_data['claim_id'] : '';
        $old_claim_id = ($claim_id) ? ClaimModel::where(['id' => $claim_id])->value('old_id') : '';

        isset($posted_plot_data['areaclaimedh']) && $posted_plot_data['areaclaimedh'] == '' ? $posted_plot_data['areaclaimedh'] = null : '';
        isset($posted_plot_data['areaapprovedgsh']) && $posted_plot_data['areaapprovedgsh'] == '' ? $posted_plot_data['areaapprovedgsh'] = null : '';
        isset($posted_plot_data['areaapprovedsdlch']) && $posted_plot_data['areaapprovedsdlch'] == '' ? $posted_plot_data['areaapprovedsdlch'] = null : '';
        isset($posted_plot_data['areaapproveddlch']) && $posted_plot_data['areaapproveddlch'] == '' ? $posted_plot_data['areaapproveddlch'] = null : '';
        isset($posted_plot_data['areagpssurveyh']) && $posted_plot_data['areagpssurveyh'] == '' ? $posted_plot_data['areagpssurveyh'] = null : '';

        if ($id) {
            if ($validator->fails()) {
                return redirect('plot/update/' . $id)
                                ->withErrors($validator, 'plot')
                                ->withInput();
            }
            $data_to_update = $posted_plot_data;
            $data_to_update['claim'] = $old_claim_id;
            $data_to_update['village'] = $village_old_id;
            $data_to_update['is_old'] = $is_old_value;
            $data_to_update['updated_at'] = Carbon::now();
            $upadte_data = PlotModel::where(['ogc_fid' => $id])->update($data_to_update);
            $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Plot updated successfully!</strong></div>';
            return redirect('plot/update/' . $id)->with('message', $msg);
        } else {
            if ($validator->fails()) {
                return redirect('plot/add')
                                ->withErrors($validator, 'plot')
                                ->withInput();
            }
            $data_to_insert = $posted_plot_data;
            $data_to_insert['claim'] = $old_claim_id;
            $data_to_insert['village'] = $village_old_id;
            $data_to_insert['is_old'] = $is_old_value;
            $data_to_insert['created_at'] = Carbon::now();
            $data_to_insert['updated_at'] = Carbon::now();
            // $this->pre($data_to_insert);exit;
            $insert_id = PlotModel::insert($data_to_insert);
            //$this->pre($insert_id);exit;
            if ($insert_id) {
                $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Plot added successfully!</strong></div>';
                return redirect('plot/list')->with('message', $msg);
            }
        }
    }

}
