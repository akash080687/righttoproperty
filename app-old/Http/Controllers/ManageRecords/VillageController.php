<?php

namespace App\Http\Controllers\ManageRecords;

use App\Http\Controllers\Controller;
use App\Models\Report\ClaimantModel;
use App\Models\Report\BlockModel;
use App\Models\Report\VillageModel;
use App\Models\Report\DistrictModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use DB;

class VillageController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
        $this->appData['section'] = 'village';
        $this->appData['include_script_view'][] = 'managerecords.common_scripts';
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        $this->middleware('auth');
    }

    public function add(Request $request) {
        //echo 'ssfsaf';
        //  t(Request::flash());
        //print_r(Request::flash());
        //$this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];

        $request->old('state');
        $state = $request->old('state');
        $district = $request->old('district');
        $block = $request->old('village.block_id');
        $village = $request->old('village_id');
        //  $claimnts = $request->old('family.claimant_id');
        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $this->appData['selected_state'] = isset($state) ? $state : $this->appData['logged_in_user_boudary_id'];
        $this->appData['selected_district'] = $district;
        $this->appData['selected_block'] = $block;
        $this->appData['selected_villages'] = $village;
        //$this->appData['selected_claimants'] = $claimnts;
        return view('managerecords.village.add', $this->appData);
    }

    public function edit($id) {
        if (!$id) {
            return 'village id missing';
        }

        $village_data = VillageModel::where(['ogc_fid' => $id])->get()->toArray();
        $this->appData['village_data'] = $village_data = isset($village_data[0]) ? $village_data[0] : '';
        $this->appData['selected_block'] = $selected_block = isset($village_data['block_id']) ? $village_data['block_id'] : '';
        $this->appData['selected_district'] = $selected_district = BlockModel::where(['id' => "$selected_block"])->value('dist_id');
        $this->appData['selected_state'] = $selected_state = DistrictModel::where(['id' => $selected_district])->value('state_id');

        $this->appData['id'] = $id;
        return view('managerecords.village.edit', $this->appData);
    }

    public function villageList(Request $request) {

        $this->appData['appData']['metaTitle'] = $this->metaTitle;

        $posted_data = $request->all();
        //$this->pre($posted_data);
        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];
        $selected_villages = isset($posted_data['village']) ? $posted_data['village'] : '';
        $this->appData['selected_villages'] = $selected_villages ? implode(',', array_filter($selected_villages)) : '';
        if ($selected_villages) {
            foreach ($selected_villages as &$value) {
                $value = "'" . trim($value) . "'";
            }
            $selected_villages = implode(', ', $selected_villages);
        }

        if (isset($selected_villages) && !empty($selected_villages)) {
            $village_id_str = $selected_villages;
        }
        $block_id = isset($posted_data['block']) ? $posted_data['block'] : '';
        if ($block_id) {
            if (isset($village_id_str) && $village_id_str) {
                $results = VillageModel::where(['block_id' => $block_id])->whereRaw("vil_id in ($village_id_str)")->orderBy('ogc_fid', 'desc')->paginate(25);
            } else {
                $results = VillageModel::where(['block_id' => $block_id])->orderBy('ogc_fid', 'desc')->paginate(25);
            }
        } else {
            $results = VillageModel::orderBy('ogc_fid', 'desc')->paginate(25);
        }
        // $results = ClaimantModel::orderBy('id', 'desc')->paginate(25);

        $results->appends($posted_data);
        $links = $results->render();
        $results = $results->toArray();
        $villages = isset($results['data']) ? $results['data'] : '';
        //$this->pre($villages);exit;
        $this->appData['villages'] = $villages;
        $this->appData['links'] = $links;
        $this->appData['search'] = isset($posted_data['search']) && $posted_data['search'] == 1 ? $posted_data['search'] : '';
        return view('managerecords.village.list', $this->appData);
    }

    public function processData(Request $request) {
        $posted_data = $request->all();
        $posted_village_data = isset($posted_data['village']) ? $posted_data['village'] : '';
        $id = $request->get('id');
        if ($id != '') {
            $res = VillageModel::select('is_old')->where(['ogc_fid' => $id])->get()->toArray();
        }
        if (isset($res[0]['is_old']) && $res[0]['is_old']) {
            $is_old_value = true;
        } else {
            $is_old_value = false;
        }
        $messages = [
//            'village.code.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The code field is required!</strong></div>',
//            'village.code.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Village code must be number value!</strong></div>',
            'village.pincode.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Pincode must be number value!</strong></div>',
            'village.status.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Status must be number value!</strong></div>',
            'village.status.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Enable should be checked!</strong></div>',
            'village.name.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The name field is required!</strong></div>',
            'village.name.string' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The name field must contain only letters!</strong></div>',
            'state.validate_state' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>You are not authorized to edit villages for this state</strong></div>',
        ];
        $rules = [
            //'village.code' => 'required|integer',
            'village.pincode' => 'integer',
            'village.status' => 'required|integer',
            'village.name' => 'required|string',
            'state' => 'validate_state'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        // $messages = $validator->errors();
        //$this->pre($res);
        $block_id = isset($posted_village_data['block_id']) ? $posted_village_data['block_id'] : '';
        $old_block_id = ($block_id) ? BlockModel::where(['id' => $block_id])->value('old_id') : '';
        if ($id) {
            if ($validator->fails()) {
                return redirect('village/update/' . $id)
                                ->withErrors($validator, 'village')
                                ->withInput();
            }
            $data_to_update = $posted_village_data;
            $data_to_update['tehsil'] = $old_block_id;
            $data_to_update['is_old'] = $is_old_value;
            $data_to_update['code'] = $posted_village_data['code2011'];
            $data_to_update['updated_at'] = Carbon::now();
            $upadte_data = VillageModel::where(['ogc_fid' => $id])->update($data_to_update);
            $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Village updated successfully!</strong></div>';
            return redirect('village/update/' . $id)->with('message', $msg);
        } else {
            if ($validator->fails()) {
                return redirect('village/add')
                                ->withErrors($validator, 'village')
                                ->withInput($posted_data);
            }
            $data_to_insert = $posted_village_data;
            $data_to_insert['tehsil'] = $old_block_id;
            $data_to_insert['is_old'] = $is_old_value;
            $data_to_insert['code'] = $posted_village_data['code2011'];
            $data_to_insert['created_at'] = Carbon::now();
            $data_to_insert['updated_at'] = Carbon::now();
            $insert_id = VillageModel::insert($data_to_insert);
            if ($insert_id) {
                $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Village added successfully!</strong></div>';
                return redirect('village/list')->with('message', $msg);
            }
        }
    }

}
