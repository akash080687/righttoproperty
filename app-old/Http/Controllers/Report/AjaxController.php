<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\Report\StateModel;
use App\Models\Report\DistrictModel;
use App\Models\Report\BlockModel;
use App\Models\Report\VillageModel;
use App\Models\Report\PlotModel;
use App\Models\Report\ClaimantModel;
use App\Models\Report\ClaimModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;

class AjaxController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
    }

    public function state() {
        $states = StateModel::select('stt_state_id as Id', 'stt_state_name as Name')->where(['stt_fl_archive' => 'N', 'is_rtp' => TRUE])->orderBy('stt_state_name', 'ASC')->get()->toArray();
        echo json_encode($states);
    }

    public function district(Request $request) {
        $state_id = $request->get('state_id');
        if ($state_id) {
            $districts = DistrictModel::select('id as Id', 'name as Name')->where(['state_id' => "$state_id"])->orderBy('name', 'ASC')->get()->toArray();
            echo json_encode($districts);
        }
    }

    public function block(Request $request) {
        $dist_id = $request->get('dist_id');
        if ($dist_id) {
            $blocks = DistrictModel::find($dist_id)->blocks()->select('id as Id', 'name as Name')->orderBy('name', 'ASC')->get()->toArray();
            echo json_encode($blocks);
        }
    }

    public function village(Request $request) {
        $block_id = $request->get('block_id');
        $filter = $request->get('filter');
        if ($block_id) {
            if ($filter != 'all') {
                $villages = BlockModel::find($block_id)->villages()->select('vil_id as Id', 'name as Name')
                                ->whereRaw('status=1 and village.ogc_fid in( select distinct village_ogc_id from claimant)')
                                ->orderBy('name', 'ASC')->get()->toArray();
            } else if ($filter == 'all') {
                $villages = BlockModel::find($block_id)->villages()->select('vil_id as Id', 'name as Name')
                                ->orderBy('name', 'ASC')->get()->toArray();
            }
            echo json_encode($villages);
        }
    }

    public function getPlotsByVillageId(Request $request) {
        $village_id = $request->get('village_id');
        if ($village_id) {
            $plots = PlotModel::select('ogc_fid as Id')->where(['vil_id' => "$village_id"])->get()->toArray();
            echo json_encode($plots);
        }
    }

    public function getClaimantsByVillageId(Request $request) {
        $village_id = $request->get('village_id');
        if ($village_id) {
            $village_ogc_id = VillageModel::where(['vil_id' => $village_id])->value('ogc_fid');
            $claimants = VillageModel::find($village_ogc_id)->claimants()->select('id as Id', 'old_id as Name')
                            ->where(['village_id' => "$village_id"])->get()->toArray();

            echo json_encode($claimants);
        }
    }

    public function getClaimsByVillageId(Request $request) {
        $village_id = $request->get('village_id');
        $claimants = $request->get('claimant_id');
        //$this->pre($claimants);

        $claimant_str = ($claimants) ? implode(',', $claimants) : '';
        if ($village_id && $claimant_str) {
            $claims = ClaimModel::select('id as Id', 'number as Name')->where(['village_id' => "$village_id"])
                            ->whereRaw("claimant_id in ($claimant_str)")->orderBy('id', 'asc')->get()->toArray();
        } else {
            $claims = ClaimModel::select('id as Id', 'number as Name')->where(['village_id' => "$village_id"])->orderBy('id', 'asc')->get()->toArray();
        }
        echo json_encode($claims);
    }

    public function PlotGPSGeoJSON(Request $request, $id = NULL) {
        $ogc_fid = $id; //$request->get('id');
        $feature = 'plot';
        $attribute = 'gps'; //$request->get('feature');

        switch ($feature):
            case 'plot':
                if ($attribute == 'sat'):
                    PlotModel::select('ogc_fid as Id')->where(['vil_id' => "$village_id"])->get()->toArray();
                elseif ($attribute == 'gps'): {
                        $sql = "SELECT row_to_json(fc) as geojson FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
                    FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry)::json As geometry
                    , row_to_json((ogc_fid, plt_number)) As properties,areagpssurveyh as area_gps FROM plot As lg WHERE ogc_fid = $ogc_fid  ) As f )  As fc;";

                        $response = DB::select(DB::raw($sql));
//                    print_r($response[0]->geojson);
                        echo ($response[0]->geojson);
                    }
                endif;
                break;
        endswitch;

        //echo json_encode($claims);
    }

    public function PlotSATGeoJSON(Request $request, $id = NULL) {
        $ogc_fid = $id; //$request->get('id');
        $feature = 'plot';
        $attribute = 'gps'; //$request->get('feature');

        switch ($feature):
            case 'plot':
                if ($attribute == 'sat'):
                    PlotModel::select('ogc_fid as Id')->where(['vil_id' => "$village_id"])->get()->toArray();
                elseif ($attribute == 'gps'): {
                        $sql = "SELECT row_to_json(fc) as geojson FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
                    FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry_sat)::json As geometry
                    , row_to_json((ogc_fid, plt_number)) As properties,areagpssurveyh as area_gps FROM plot As lg WHERE ogc_fid = $ogc_fid  ) As f )  As fc;";

                        $response = DB::select(DB::raw($sql));
//                    print_r($response[0]->geojson);
                        echo ($response[0]->geojson);
                    }
                endif;
                break;
        endswitch;

        //echo json_encode($claims);
    }

    public function getGeoJSONofClaimedPlots($vil_id=NULL,$plot_id=NULL) {

        $claims_of_the_village = array();
        $plots_under_claim = $claims_of_the_village;


//        $sql = "SELECT row_to_json(fc) as geojson
//  FROM ( 
//  SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
//  FROM ( SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry)::json As geometry 
//  FROM plot As lg  where claim=6258  ) As f )  As fc";
         $sql = "SELECT row_to_json(fc) as geojson  FROM ( 
  SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
  FROM ( SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry)::json As geometry 
  FROM plot As lg  where claim_id = ANY((SELECT array_agg(id) FROM claim where village_id = '$vil_id')::int[]) AND lg.ogc_fid <> $plot_id   ) As f )  As fc";
        $response = DB::select(DB::raw($sql));
        echo ($response[0]->geojson);

        //echo $result[0]['geojson'];
    }

}
