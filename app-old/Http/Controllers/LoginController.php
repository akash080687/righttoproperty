<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User\UserModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use DB;
use Hash;

class LoginController extends Controller {

    public function __construct() {
        parent:: __construct();
    }

    public function authUserByCred(Request $request) {
        $posted_data = $request->all();
        $email = isset($posted_data['email']) ? $posted_data['email'] : '';
        $password = isset($posted_data['password']) ? $posted_data['password'] : '';
        $user_obj = UserModel::where(['email' => $email])->get();
        if ($email && $password) {
            if ($user_obj) {
                $user = $user_obj->toArray();
                $user_pass = trim($user[0]['pass']);
                if (Hash::check($password, $user_pass)) {

                    session_name('Laravel');
                    session_start();
                    $_SESSION['is_logged'] = 'logged';
                    Auth::loginUsingId($user[0]['id']);
                    //Session::put('is_logged_in', 'logged');
                    echo 1;
                } else {
                    echo 0;
                }
            }
        }
    }

    public function checkValidUser(Request $request) {
        $posted_data = $request->all();
        $email = isset($posted_data['email']) ? $posted_data['email'] : '';
        // t($posted_data);
        if ($email) {
            $res = UserModel::where(['email' => "{$email}"])->get()->toArray();
            // t($res);
            if (empty($res)) {
                echo "false";
                exit;
            } else {
                echo "true";
                exit;
            }
        }
    }

    public function logout() {

        Auth::logout();
        return redirect('/');
    }

}
