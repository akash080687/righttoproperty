<?php

namespace App\Models\Report;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class ClaimantModel extends BaseModel {

    protected $table = null;

    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_CLAIMANT'];
    }

    public function claims() {
        return $this->hasMany('App\Models\Report\ClaimModel', 'claimant_id')->orderBy('number', 'ASC');
    }

}
