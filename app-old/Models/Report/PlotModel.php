<?php

namespace App\Models\Report;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class PlotModel extends BaseModel {

    protected $table = null;
    protected $primaryKey = 'ogc_fid';
    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_PLOT'];
    }

}
