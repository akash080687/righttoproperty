<?php

namespace App\Models\Report;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class StateModel extends BaseModel {

    protected $table = null;
   
    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_STATE'];
    }
    
    public function getDistricts(){
        return $this->hasMany('App\Models\Report\DistrictModel', 'state_id','stt_state_id');
    }
    
    

}
