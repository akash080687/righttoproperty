<?php

namespace App\Models\User;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

use Illuminate\Foundation\Auth\User as Authenticatable;
class UserModel extends Authenticatable {

    protected $table = null;

    public function __construct() {
        parent::__construct();
        $this->table = 'user';
    }

}
