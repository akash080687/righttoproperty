<?php

namespace App\Models\ManageRecords;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class FamilyModel extends BaseModel {

    protected $table = null;

    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_FAMILY'];
    }

}
