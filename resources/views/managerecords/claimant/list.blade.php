@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-1 search">
                <a href="javascript:void(0);" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"></span>
                </a>
            </div>
            <div class="col-sm-11">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>                       
                        <li class="active">Claimant</li>
                        <li class="pull-right"><a href="<?= url('claimant/add') ?>">Add New Claimant</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div>{!! Session::get('message')!!}</div>
    <div class="search-container">

        <div class="container">
            <?php
            $display_content_style = ($search == 1) ? 'style="display:block"' : 'style="display:none"';
            ?>
            <div class="display_panel_content" <?= $display_content_style ?>>
                <h2>Search Claimant</h2>
                {!! Form::open(['url' => url('claimant/list'),'class' => 'form-horizontal search-form','method' => 'GET', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">                       
                                        {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','style' => 'width:200px','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" style="width: 200px;" id="district" name="district" onchange="populateBlock($(this).val())">
                                            <option value="">Select District</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" style="width: 200px;" id="block" name="block" onchange="populateVillage($(this).val())">
                                            <option value="">Select Block/Tehsil</option>                      
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Village<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" style="width: 200px;" id="village" name="village" onchange="populateClaimant($(this).val());populateMoholla($(this).val());">
                                            <option value="">Select Village</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Mohalla:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="moholla" style="width: 200px;" name="moholla[]" multiple=""  onchange="populateClaimantByMoholla($(this).val());">
                                            <option value="">Select Mohalla</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Claimant:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control  chosen-select chosen_select_custom" style="width: 200px;" id="claimant" name="claimant[]" multiple="">
                                            <option value="">Select Claimant</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                       

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-5">
                            <button class="btn btn-info pull-right" type="submit" name="search" value="1">Search</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>


    <h2>Claimant List</h2>
    <!--    <p>The .table-condensed class makes a table more compact by cutting cell padding in half:</p>    -->
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Claimant Id</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Voter ID</th>
                    <th>Adhar ID</th>
                    <th>ST/OFD</th>
                    <th>Village</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($claimants) {
                    foreach ($claimants as $key => $value) {
                        $vil_id = $value['village_id_master'];
                        $rtp_status = App\Models\Report\VillageModel::where(['vil_id' => "$vil_id"])->value('rtp_status');
                        if ($rtp_status == true && $value['old_id']) {
						//	if (1) {
                            ?>
                            <tr>
                                <td><?= $value['old_id'] ?></td>
                                <td><?= $value['name'] ?></td>
                                <td><?= $value['age'] ?></td>
                                <td><?= $value['voterid'] ?></td>
                                <td><?= $value['adharid'] ?></td>
                                <td><?= $value['stofd'] ?></td>
                                <td><?php
                                    //$vil_id = $value['village_id'];
                                    $village_name = App\Models\Report\VillageModel::where(['vil_id' => "$vil_id"])->value('village_name');
                                    $village_ogc_fid = App\Models\Report\VillageModel::where(['vil_id' => "$vil_id"])->value('ogc_fid');
                                    ?>
                                    <a href="<?= url('village/update/' . $village_ogc_fid) ?>"><?= $village_name ?></a>
                                </td>
                                <td><a href="<?= url('claimant/update/' . $value['id']) ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>

                            </tr>
                            <?php
                        }
                    }
                }
                ?>
            </tbody>
        </table>
        <?= $links ?>
    </div>
</div>

@endsection

