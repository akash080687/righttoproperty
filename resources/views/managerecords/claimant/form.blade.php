<?php
$mesages = '';
if (isset($errors) && !empty($errors)) {
    $mesages = $errors->claimant->all();
}
?>
<div class="container">
    <?php
    if ($mesages) {
        foreach ($mesages as $key => $value) {
            echo $value;
        }
    }
    ?>   
    {!! Form::open(['url' => url('claimant/submit-claimant-data'),'class' => 'form-horizontal add-edit-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
    <div class="box-body">

        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">                       
                            {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="district" name="district" onchange="populateBlock($(this).val())">
                                <option value="">Select District</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="block" name="block" onchange="populateVillage($(this).val())">
                                <option value="">Select Block/Tehsil</option>                      
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"  for="inputEmail3">Claim Village<span style="color: red">*</span>:</label>
                        <div class="col-sm-8">
                            <select class="form-control required" id="village" name="claimant[village_id_master]" onchange="populateMoholla($(this).val())">
                                <option value="">Select Village</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"  for="inputEmail3">Mohalla:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="moholla" style="width: 200px;" name="claimant[mohallah_id_master]"  onchange="">
                                <option value="">Select Mohalla</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"  for="inputEmail3">Claimant ID<span style="color: red">*</span>:</label>
                        <div class="col-sm-8">
                            {{ Form::text('claimant[old_id]', isset($claimant_data['old_id']) ? $claimant_data['old_id'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Claimant ID','required' => 'true')) }}
                        </div>

                    </div>
                </div>
            </div>

        </div> 

        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label"  for="inputEmail3">Claimant Name<span style="color: red">*</span>:</label>
                        <div class="col-sm-7">
                            {{ Form::text('claimant[name]', isset($claimant_data['name']) ? $claimant_data['name'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'Name')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Age:</label>
                        <div class="col-sm-9">                       
                            {{ Form::text('claimant[age]', isset($claimant_data['age']) &&  $claimant_data['age'] != 0 ? $claimant_data['age'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Age')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Voter ID:</label>
                        <div class="col-sm-9">
                            {{ Form::text('claimant[voterid]', isset($claimant_data['voterid']) ? $claimant_data['voterid'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Voter ID')) }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Adhar ID:</label>
                        <div class="col-sm-9">
                            {{ Form::text('claimant[adharid]', isset($claimant_data['adharid']) ? $claimant_data['adharid'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Adhar ID')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">ST/OFD<span style="color: red">*</span>:</label>
                        <div class="col-sm-8">                       
                            {{ Form::text('claimant[stofd]', isset($claimant_data['stofd']) ? $claimant_data['stofd'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'ST/OFD')) }}
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label"  for="inputEmail3">Residence Village<span style="color: red">*</span>:</label>
                        <div class="col-sm-7">
                            <select class="form-control required" id="residence_village" name="claimant[claimant_resi_master_village_id]">
                                <option value="">Select Residence Village</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="is_old" value="<?= isset($claimant_data['is_old']) ? $claimant_data['is_old'] : '' ?>">
        <input type="hidden" name="id" value="<?= isset($id) ? $id : '' ?>">
        <div class="box-footer">
            <div class="col-sm-5">
                <button class="btn btn-info pull-right" type="submit">Save</button>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}