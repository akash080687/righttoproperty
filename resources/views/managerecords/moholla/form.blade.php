<?php
$mesages = '';
if (isset($errors) && !empty($errors)) {
    $mesages = $errors->moholla->all();
}
?>
<div class="container">
    <?php
    if ($mesages) {
        foreach ($mesages as $key => $value) {
            echo $value;
        }
    }
    // t($moholla_data);
    ?>   
    {!! Form::open(['url' => url('moholla/submit-moholla-data'),'class' => 'form-horizontal add-edit-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
    <div class="box-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">                       
                            {{Form::select('moholla[state_id]', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="district" name="moholla[dist_id]" onchange="populateBlock($(this).val())">
                                <option value="">Select District</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="block" name="moholla[block_id]" filter="all" onchange="populateVillage($(this).val())">
                                <option value="">Select Block/Tehsil</option>                      
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"  for="inputEmail3">Village<span style="color: red">*</span>:</label>
                        <div class="col-sm-8">
                            <select class="form-control required" id="village" name="moholla[village_master_id]" onchange="">
                                <option value="">Select Village</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="inputEmail3">Mohalla Code(2011)<span style="color: red">*</span>:</label>
                                    <div class="col-sm-8">
                                        {{ Form::text('moholla[census2011]', isset($moholla_data['census2011']) ? $moholla_data['census2011'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'Mohalla Code')) }}
                                    </div>
                                </div>
                            </div>
                        </div>-->
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Mohalla/Para Name<span style="color: red">*</span>:</label>
                        <div class="col-sm-7">
                            {{ Form::text('moholla[moholla_name]', isset($moholla_data['moholla_name']) ? $moholla_data['moholla_name'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'Mohalla Name')) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <?php
            $rtp_status = isset($moholla_data['rtp_status']) ? $moholla_data['rtp_status'] : '';
            ?>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Enable:</label>
                        <div class="col-sm-8">
                            <?php if (isset($id) && $id) { ?>
                                {{ Form::checkbox('moholla[rtp_status]', $rtp_status,$rtp_status == true ?true:false )}}                             
                            <?php } else { ?>
                                {{ Form::checkbox('moholla[rtp_status]', $rtp_status,true)}}
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Pincode:</label>
                        <div class="col-sm-8">
                            {{ Form::text('moholla[pincode]', isset($moholla_data['pincode']) ? $moholla_data['pincode'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Pincode')) }}
                        </div>
                    </div>
                </div>
            </div>

            <!--            <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">GPS:</label>
            
                                    <div class="col-sm-9">
                                        {{ Form::textarea('moholla[gps]', isset($moholla_data['gps']) ? $moholla_data['gps'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'GPS','size' => '30x3')) }}
                                    </div>
            
                                </div>
                            </div>
                        </div>-->
        </div>

        <input type="hidden" name="is_old" value="<?= isset($moholla_data['is_old']) ? $moholla_data['is_old'] : '' ?>">
        <input type="hidden" name="moholla[is_rtp]" value="<?= true ?>">
        <input type="hidden" name="id" value="<?= isset($id) ? $id : '' ?>">
        <?php if (isset($id) && $id) { ?>
            <input type="hidden" name="moholla[census2011]" value="<?= isset($moholla_data['census2011']) ? $moholla_data['census2011'] : '' ?>">
        <?php } ?>
        <div class="box-footer">
            <div class="col-sm-5">
                <button class="btn btn-info pull-right" type="submit">Save</button>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}