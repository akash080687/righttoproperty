<?php
$mesages = '';
if (isset($errors) && !empty($errors)) {

    $mesages = $errors->family->all();
}
?>
<div class="container">
    <?php
    if ($mesages) {
        foreach ($mesages as $key => $value) {
            echo $value;
        }
    }
    //echo $selected_state;
    ?>   
    {!! Form::open(['url' => url('family/submit-family-data'),'class' => 'form-horizontal add-edit-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
    <div class="box-body">

        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">                       
                            {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="district" name="district" onchange="populateBlock($(this).val())">
                                <option value="">Select District</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="block" name="block" onchange="populateVillage($(this).val())">
                                <option value="">Select Block/Tehsil</option>                      
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"  for="inputEmail3">Village<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="village" name="village" onchange="populateClaimant($(this).val())">
                                <option value="">Select Village</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"  for="inputEmail3">Claimant ID<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required chosen-select chosen_select_custom" id="claimant" name="family[claimant_id]">
                                <option value="">Select Claimant ID</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"  for="inputEmail3">Claimant Name<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            {{ Form::text('family[name]', isset($family_data['name']) ? $family_data['name'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'Claimant Name')) }}
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"  for="inputEmail3">Age:</label>
                        <div class="col-sm-9">
                            <?php
                            $age = isset($family_data['age']) ? $family_data['age'] : ''
                            ?>
                            {{ Form::text('family[age]', ($age == 0) ? '' : $age, array('class'=>'form-control col-xs-5','placeholder' => 'Age')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"  for="inputEmail3">Relation<span>*</span>:</label>
                        <div class="col-sm-9">
                            {{ Form::text('family[relation]', isset($family_data['relation']) ? $family_data['relation'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'Relation')) }}
                        </div>
                    </div>
                </div>
            </div>

        </div>        
        <input type="hidden" name="is_old" value="<?= isset($family_data['is_old']) ? $family_data['is_old'] : '' ?>">
        <input type="hidden" name="id" value="<?= isset($id) ? $id : '' ?>">
        <div class="row">
            <div class="box-footer">
                <div class="col-sm-6">
                    <button class="btn btn-info pull-right" type="submit">Save</button>
                </div>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}