
<script>
    selected_state = "{{ isset($selected_state) ? $selected_state : Request::get('state') }}";
    selected_dist = "{{ isset($selected_district) ? $selected_district :Request::get('district') }}";
    selected_block = "{{ isset($selected_block) ? $selected_block : Request::get('block') }}";
    selected_village = "{{ isset($selected_village) ? $selected_village :Request::get('village') }}";
    selected_claimant = "{{ isset($selected_claimants)? $selected_claimants : '' }}";
    selected_claim = "{{ isset($selected_claims)? $selected_claims : '' }}";
    alert(selected_state);
    function populateDistrict(state_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#district').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/districts') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'state_id': state_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
                //alert(selected_dist);

                $.each(data, function (index, value) {
                    var selected = (selected_dist == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#district').append('<option ' + selected + 'value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_block) {
                    populateBlock($('#district').val());
                }
                $('.display_panel_content').css('cursor', '');
//                setTimeout(function () {
//                    $(".chosen-select").chosen();
//                }, 500);
            },
        });
    }

    function populateBlock(dist_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#block').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/blocks') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'dist_id': dist_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)

                $.each(data, function (index, value) {
                    var selected = (selected_block == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#block').append('<option ' + selected + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_block) {
                    populateVillage($('#block').val());
                }
                $('.display_panel_content').css('cursor', '');
//                setTimeout(function () {
//                    $(".chosen-select").chosen();
//                }, 500);
            },
        });
    }

    function populateVillage(block_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#village').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/villages') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'block_id': block_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)

                $.each(data, function (index, value) {
                    var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#village').append('<option ' + selected + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_village != '') {
                    populateClaimant($('#village').val());
                }
                $('.display_panel_content').css('cursor', '');
//                setTimeout(function () {
//                    $(".chosen-select").chosen();
//                }, 500);
            },
        });
    }

    function populateClaimant(village_id) {
        $('#claimant').children(':not(:first-child)').remove();
        $('.display_panel_content').css('cursor', 'wait');
       
        $.ajax({
            url: '<?= url('ajax/claimants-by-village') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'village_id': village_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                // console.log(data)
                $.each(data, function (index, value) {
                    //var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#claimant').append('<option ' + '' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                $('#claimant').val(selected_claimant.split(','));
                 
                $('.display_panel_content').css('cursor', '');
                setTimeout(function () {
                    $(".chosen-select").chosen();
                }, 500);
            },
        });
    }

//    function populateClaims(village_id, claimant_id) {
//        $('#claim').children(':not(:first-child)').remove();
//        $('.display_panel_content').css('cursor', 'wait');
//        $.ajax({
//            url: '<?= url('ajax/claims-by-village') ?>',
//            type: 'POST',
//            data: {
//                '_token': '<?= csrf_token() ?>',
//                'village_id': village_id
//            },
//            error: function () {
//                // $('#info').html('<p>An error has occurred</p>');
//            },
//            dataType: 'json',
//            success: function (data) {
//                // console.log(data)
////                $.each(data, function (index, value) {
////                    //var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
////                    $('#claimant').append('<option ' + '' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
////                });
////                $('#claimant').val(selected_claimant.split(','));
////                $('.display_panel_content').css('cursor', '');
//            },
//        });
//    }
    $(function () {
        $('.close').on('click', function () {
            $(this).parents('.alert-success').hide();
            $(this).parents('.alert-danger').hide();
        });

        $('.family-form').on('submit', function () {
            var error = 0;
            $('.required').each(function () {
                var val = $(this).val();
                if (!val) {
                    error++;
                    $(this).parents('.form-group').addClass('has-error');
                } else {
                    $(this).parents('.form-group').removeClass('has-error');
                }
            });
            if (error > 0) {
                return false;
            }
        });

        $('.family-search-form').on('submit', function () {
            var error = 0;
            $('.required').each(function () {
                var val = $(this).val();
                if (!val) {
                    error++;
                    $(this).parents('.form-group').addClass('has-error');
                } else {
                    $(this).parents('.form-group').removeClass('has-error');
                }
            });
            if (error > 0) {
                return false;
            }
        });
    });
    $(window).on('load', function () {
        $.ajax({
            url: '<?= url('ajax/states') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>'
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
                $.each(data, function (index, value) {
                    var selected = (selected_state == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#state').append('<option ' + selected + '  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_state) {
                    populateDistrict($('#state').val());
                }
//                setTimeout(function () {
//                    $(".chosen-select").chosen();
//                }, 500);
            },
        });
    });
</script>
