@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">

    <div class="container">
        <div class="row">
            <div class="col-sm-1 search">
                <a href="javascript:void(0);" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"></span>
                </a>
            </div>
            <div class="col-sm-11">
                <div>
                    <ol class="breadcrumb">
                         <li><a href="<?= url('/') ?>">Home</a></li>                        
                        <li class="active">Family</li>
                        <li class="pull-right"><a href="<?= url('family/add') ?>">Add New Family</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div>{!! Session::get('message')!!}</div>
    <div class="search-container">

        <div class="container">
            <?php
            $display_content_style = ($search == 1) ? 'style="display:block"' : 'style="display:none"';
            ?>
            <div class="display_panel_content" <?= $display_content_style ?>>
                <h2>Search Family</h2>
                {!! Form::open(['url' => url('family/list'),'class' => 'form-horizontal search-form','method' => 'GET', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">                       
                                         {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','style' => 'width:200px','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" id="district" style="width: 200px;" name="district" onchange="populateBlock($(this).val())">
                                            <option value="">Select District</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" id="block" style="width: 200px;" name="block" onchange="populateVillage($(this).val())">
                                            <option value="">Select Block/Tehsil</option>                      
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Village<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" style="width: 200px;" id="village" name="village" onchange="populateClaimant($(this).val())">
                                            <option value="">Select Village</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Claimant:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control  chosen-select chosen_select_custom" style="width: 200px;" id="claimant" name="claimant[]" multiple="">
                                            <option value="">Select Claimant</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                       

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-5">
                            <button class="btn btn-info pull-right" type="submit" name="search" value="1">Search</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>



    <h2>Family List</h2>
    <!--    <p>The .table-condensed class makes a table more compact by cutting cell padding in half:</p>    -->
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Relation Id</th>
                    <th>Member Name</th>
                    <th>Age</th>
                    <th>Relation</th>
    <!--                    <th>Claimant</th>-->
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($family) {
                    foreach ($family as $key => $value) {
                        ?>
                        <tr>
                            <td><?= $value['id'] ?></td>
                            <td><?= $value['name'] ?></td>
                            <td><?= ($value['age'] == 0) ? '' :$value['age']  ?></td>
                            <td><?= $value['relation'] ?></td>
                            <?php if ($value['is_old'] == 1) { ?>
                                                                                                            <!--                                <td><?= $value['claimant_old'] ?></td>-->
                            <?php } else { ?>
                                                                                                            <!--                                <td><?= $value['claimant_id'] ?></td>-->
                            <?php } ?>
                            <td><a href="<?= url('family/update/' . $value['id']) ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>

                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
        <?= $links ?>
    </div>
</div>

@endsection

