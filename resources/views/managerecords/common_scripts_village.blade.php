<script>
    selected_state = "{{ isset($selected_state) ? $selected_state : Request::get('state') }}";
    selected_dist = "{{ isset($selected_district) ? $selected_district :Request::get('district') }}";
    selected_block = "{{ isset($selected_block) ? $selected_block : Request::get('block') }}";
    selected_village = "{{ isset($selected_villages) ? $selected_villages :Request::get('village') }}";
    selected_resi_village = "{{ isset($selected_resi_village) ? $selected_resi_village :Request::get('selected_resi_village') }}";
    selected_moholla = "{{ isset($selected_moholla) ? $selected_moholla :Request::get('moholla') }}";
    selected_claimant = "{{ isset($selected_claimants)? $selected_claimants : '' }}";
    selected_claim = "{{ isset($selected_claims)? $selected_claims : '' }}";
    //alert(selected_state);
    logged_in_user_boudary_id = "{{ isset($logged_in_user_boudary_id)? $logged_in_user_boudary_id : '' }}";
    //alert(selected_village);
    selected_village_id = "{{ isset($id)? $id : '' }}";
    //alert(selected_village_id);
     
           
           
    function populateDistrictMaster(state_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#district').children(':not(:first-child)').remove();
        $('#loader').css('visibility', 'visible');
        $.ajax({
            url: '<?= url('ajax/districts-master') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'state_id': state_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
                //alert(selected_dist);

                $.each(data, function (index, value) {
                    var selected = (selected_dist == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#district').append('<option ' + selected + 'value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_block) {
                    populateBlockMaster($('#district').val());
                }
                $('.display_panel_content').css('cursor', '');
                $('#loader').css('visibility', 'hidden');
                if(selected_village_id){
                      $('#district').attr('disabled','true');
                      $('.add-edit-form').append('<input type="hidden" name="district" value="'+selected_dist+'" >');
                }
            },
        });
    }
    function populateDistrict(state_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#district').children(':not(:first-child)').remove();
        $('#loader').css('visibility', 'visible');
        $.ajax({
            url: '<?= url('ajax/districts') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'state_id': state_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
                //alert(selected_dist);

                $.each(data, function (index, value) {
                    var selected = (selected_dist == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#district').append('<option ' + selected + 'value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_block) {
                    //  populateBlock($('#district').val());
                }
                $('.display_panel_content').css('cursor', '');
                $('#loader').css('visibility', 'hidden');
                 if(selected_village_id){
                      $('#district').attr('disabled','true');
                      $('.add-edit-form').append('<input type="hidden" name="district" value="'+selected_dist+'" >');
                }
            },
        });
    }

    function populateBlockMaster(dist_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#block').children(':not(:first-child)').remove();
        $('#loader').css('visibility', 'visible');
        $.ajax({
            url: '<?= url('ajax/blocks-master') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'dist_id': dist_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)

                $.each(data, function (index, value) {
                    var selected = (selected_block == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#block').append('<option ' + selected + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_block) {
                    populateVillageMaster($('#block').val());
                }
                $('.display_panel_content').css('cursor', '');
                $('#loader').css('visibility', 'hidden');
                 if(selected_village_id){
                     $('#block').attr('disabled','true');
                     $('.add-edit-form').append('<input type="hidden" name="village[block_id]" value="'+selected_block+'" >');
                }
            },
        });
    }

    function populateBlock(dist_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#block').children(':not(:first-child)').remove();
        $('#loader').css('visibility', 'visible');
        $.ajax({
            url: '<?= url('ajax/blocks') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'dist_id': dist_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)

                $.each(data, function (index, value) {
                    var selected = (selected_block == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#block').append('<option ' + selected + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_block) {
                    populateVillage($('#block').val());
                }
                $('.display_panel_content').css('cursor', '');
                $('#loader').css('visibility', 'hidden');
                if(selected_village_id){
                     $('#block').attr('disabled','true');
                     $('.add-edit-form').append('<input type="hidden" name="village[block_id]" value="'+selected_block+'" >');
                }
            },
        });
    }

    function populateVillageMaster(block_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#village').children(':not(:first-child)').remove();
        $('#residence_village').children(':not(:first-child)').remove();
        // alert(block_id);
        $('#loader').css('visibility', 'visible');
        $.ajax({
            url: '<?= url('ajax/villages-master') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'block_id': block_id,
                'filter': $('#block').attr('filter')
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
                $.each(data, function (index, value) {
                    //  var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#village').append('<option ' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + " (" + $(this)[0].census2011 + ")" + '</option>')
                    $('#residence_village').append('<option ' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });

//                if (selected_village != '') {
//                    $('#village').val(selected_village.split(','));
//                    $('#residence_village').val(selected_resi_village.split(','));
//                    populateClaimant($('#village').val());
//                    populateClaims($('#village').val(), $('#claimant').val());
//                }
//                if (selected_village && selected_moholla) {
//                    populateMoholla($('#village').val());
//                }

                if (selected_village != '') {
                    $('#village').val(selected_village.split(','));
                    $('#residence_village').val(selected_resi_village.split(','));
                    // populateMoholla($('#village').val());
                    populateClaimant($('#village').val());
                    populateClaims($('#village').val(), $('#claimant').val());
                }
                if (selected_village && selected_moholla) {
                    //  alert('sdfds');
                    $('#village').val(selected_village.split(','));
                    $('#residence_village').val(selected_resi_village.split(','));
                    populateMoholla($('#village').val());
                }
                $('.display_panel_content').css('cursor', '');
                $('#loader').css('visibility', 'hidden');
                if(selected_village_id){
                     $('#village').attr('disabled','true');
                     $('.add-edit-form').append('<input type="hidden" name="village_id" value="'+selected_village+'" >');
                }
            },
        });
    }

    function populateVillage(block_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#village').children(':not(:first-child)').remove();
        $('#residence_village').children(':not(:first-child)').remove();
        // alert(block_id);
        $('#loader').css('visibility','visible');
        $.ajax({
            url: '<?= url('ajax/villages') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'block_id': block_id,
                'filter': $('#block').attr('filter'),
                'villages_for_rtp': $('#block').attr('villages_for_rtp')
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
                $.each(data, function (index, value) {
                    //  var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#village').append('<option ' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + " (" + $(this)[0].census2011 + ")" + '</option>')
                    $('#residence_village').append('<option ' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });

//                if (selected_village != '') {
//                    $('#village').val(selected_village.split(','));
//                    $('#residence_village').val(selected_resi_village.split(','));
//                    populateClaimant($('#village').val());
//                    populateClaims($('#village').val(), $('#claimant').val());
//                }
//                if (selected_village && selected_moholla) {
//                    populateMoholla($('#village').val());
//                }

                if (selected_village != '') {
                    $('#village').val(selected_village.split(','));
                    $('#residence_village').val(selected_resi_village.split(','));
                    // populateMoholla($('#village').val());
                    populateClaimant($('#village').val());
                    populateClaims($('#village').val(), $('#claimant').val());
                }
                if (selected_village && selected_moholla) {
                    //  alert('sdfds');
                    $('#village').val(selected_village.split(','));
                    $('#residence_village').val(selected_resi_village.split(','));
                    populateMoholla($('#village').val());
                }
                $('.display_panel_content').css('cursor', '');
                $('#loader').css('visibility','hidden');
                 if(selected_village_id){
                     $('#village').attr('disabled','true');
                     $('.add-edit-form').append('<input type="hidden" name="village_id" value="'+selected_village+'" >');
                }
            },
        });
    }


    function populateMoholla(village_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#moholla').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/mohollas') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'village_id': village_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)

                $.each(data, function (index, value) {
                    var selected = (selected_moholla == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#moholla').append('<option ' + selected + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });

                if (selected_moholla) {
                    // populateClaims(village_id, claimant_id)
                    populateClaimantByMoholla($('#moholla').val());
                    //alert($('#claimant').val());                  
                }

                $('.display_panel_content').css('cursor', '');
            },
        });
    }

    function populateClaimant(village_id) {
        $('#claimant').children(':not(:first-child)').remove();
        $('.display_panel_content').css('cursor', 'wait');
        $.ajax({
            url: '<?= url('ajax/claimants-by-village') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'village_id': village_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                // console.log(data)
                $.each(data, function (index, value) {
                    //var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#claimant').append('<option ' + '' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_claimant) {
                    $('#claimant').val(selected_claimant.split(','));
                    // populateClaims(village_id, claimant_id)
                }

                $('.display_panel_content').css('cursor', '');
            },
        });
    }

    function populateClaimantByMoholla(moholla_id) {
        $('#claimant').children(':not(:first-child)').remove();
        $('#claim').children(':not(:first-child)').remove();
        $('.display_panel_content').css('cursor', 'wait');
        $.ajax({
            url: '<?= url('ajax/claimants-by-moholla') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'moholla_id': moholla_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                // console.log(data)
                $.each(data, function (index, value) {
                    //var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#claimant').append('<option ' + '' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_claimant) {
                    $('#claimant').val(selected_claimant.split(','));
                    populateClaims($('#village').val(), $('#claimant').val());
                }

                $('.display_panel_content').css('cursor', '');
            },
        });
    }


    function populateClaims(village_id, claimant_id) {
        $('#claim').children(':not(:first-child)').remove();
        $('.display_panel_content').css('cursor', 'wait');
        var moholla_id = $('#moholla').val();
        if (!claimant_id) {
            claimant_id = $('#claimant').val();
        }
        $.ajax({
            url: '<?= url('ajax/claims-by-village') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'village_id': village_id,
                'claimant_id': claimant_id,
                'moholla_id': moholla_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //  console.log(data)
                $.each(data, function (index, value) {
                    $('#claim').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>');
                });
                if (selected_claim) {
                    $('#claim').val(selected_claim.split(','));
                }
                $('.display_panel_content').css('cursor', '');
            },
        });
    }
    $(function () {

        $('select').select2(
                {
                    placeholder: 'Select'
                }
        );
        $('.close').on('click', function () {
            $(this).parents('.alert-success').hide();
            $(this).parents('.alert-danger').hide();
        });

        $('.search').on('click', function () {
            $(".display_panel_content").toggle('slow');
        });

//        $('form').on('submit', function () {
//            var error = 0;
//            $('.required').each(function () {
//                var val = $(this).val();
//                if (!val) {
//                    error++;
//                    $(this).parents('.form-group').addClass('has-error');
//                } else {
//                    $(this).parents('.form-group').removeClass('has-error');
//                }
//            });
//            if (error > 0) {
//                return false;
//            }
//        });

        //------------- Form validation -------------//
        $('.add-edit-form, .search-form').validate({
            ignore: null,
            ignore: 'input[type="hidden"]',
                    errorPlacement: function (error, element) {
                        var place = element.closest('.input-group');
                        if (!place.get(0)) {
                            place = element;
                        }
                        if (place.get(0).type === 'checkbox') {
                            place = element.parent();
                        }
                        if (error.text() !== '') {
                            place.after(error);
                        }
                    },
            errorClass: 'help-block',
            rules: {
                email: {
                    required: true,
                    email: true
                },
                text: {
                    required: true,
                },
                select2: "required",
                password: {
                    required: true,
                    minlength: 5
                },
                textarea: {
                    required: true,
                    minlength: 10
                },
                maxLenght: {
                    required: true,
                    maxlength: 10
                },
                rangelenght: {
                    required: true,
                    rangelength: [10, 20]
                },
                url: {
                    required: true,
                    url: true
                },
                range: {
                    required: true,
                    range: [5, 10]
                },
                minval: {
                    required: true,
                    min: 13
                },
                maxval: {
                    required: true,
                    max: 13
                },
                date: {
                    required: true,
                    date: true
                },
                number: {
                    required: true,
                    number: true
                },
                digits: {
                    required: true,
                    digits: true
                },
                ccard: {
                    required: true,
                    creditcard: true
                },
                agree: "required"
            },
            messages: {
                password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                agree: "Please accept our policy",
                textarea: "Write some info for you",
                select2: "Please select something"
            },
            highlight: function (label) {
                $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            }
        });

        $("#datepicker-13").datepicker(
                {dateFormat: 'dd/mm/yy'}
        );
        //$("#datepicker-13").datepicker("show");

    });
    $(window).on('load', function () {
        if (selected_state != 'all') {
            populateDistrictMaster($('#state').val());
        }
        if(selected_village_id){
            $('#state').attr('disabled','true');
           $('.add-edit-form').append('<input type="hidden" name="state" value="'+selected_state+'" >'); 
         
        }
//        $.ajax({
//            url: '<?= url('ajax/states') ?>',
//            type: 'POST',
//            data: {
//                '_token': '<?= csrf_token() ?>'
//            },
//            error: function () {
//                // $('#info').html('<p>An error has occurred</p>');
//            },
//            dataType: 'json',
//            success: function (data) {
//                //console.log(data)
//                if (logged_in_user_boudary_id) {
//                    selected_state = logged_in_user_boudary_id;
//                }
//                $.each(data, function (index, value) {
//                    var selected = (selected_state == $(this)[0].Id) ? 'selected="selected"' : '';
//                    $('#state').append('<option ' + selected + '  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
//                });
//
//                if (selected_state) {
//                    populateDistrict($('#state').val());
//                }
//            },
//        });
    });

//    $(document).ready(function () {
//        function toggleNavbarMethod() {
//            if ($(window).width() > 768) {
//                $('.navbar .dropdown').on('mouseover', function () {
//                    $('.dropdown-toggle', this).trigger('click');
//                }).on('mouseout', function () {
//                    $('.dropdown-toggle', this).trigger('click').blur();
//                });
//            }
//            else {
//                $('.navbar .dropdown').off('mouseover').off('mouseout');
//            }
//        }
//        toggleNavbarMethod();
//        $(window).resize(toggleNavbarMethod);
//
//    });
</script>
