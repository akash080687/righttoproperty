@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>                         
                        <li><a href="<?= url('plot/list') ?>">Plot</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div>{!! Session::get('message')!!}</div>
    <h2>Edit Plot </h2>
    @include('managerecords.plot.form')
</div>
<div>
    <script src="{{ URL::asset('includes/js/ol.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('includes/css/ol.css') }}" />
</div>
<br/>
<div id="map" class="map" style="height: 400px;width: 100%;"></div>
<br/>
<button onclick="updatePlotGeometry();$(this).remove();">Update Plot Geometry</button>
<script>
    var map = new ol.Map({
        //        interactions: ol.interaction.defaults().extend([select, modify]),
        controls: ol.control.defaults().extend([
          new ol.control.FullScreen(),
          new ol.control.ScaleLine()
        ]),
        layers: [new ol.layer.Tile({
                visible: true,
//                        preload: Infinity,
                source: new ol.source.BingMaps({
                    key: 'Aq8wYwFzJ6vc0gaqMNZAHC42yBa1629gN5ybhNp0A-Ojzwvw4aAJUdhJHfcUfx9Y',
                    imagerySet: 'AerialWithLabels',
                    maxZoom: 19
                            // use maxZoom 19 to see stretched tiles instead of the BingMaps
                            // "no photos at this zoom level" tiles
                            // maxZoom: 19
                })
            })],
        target: 'map',
        view: new ol.View({
            center: [0, 0],
            zoom: 9
        })
    });
    var plotSourceSAT = new ol.source.Vector({
        url: '<?php echo url('ajax/plotGpsGeoJSON/' . $id); ?>',
        format: new ol.format.GeoJSON({
            defaultDataProjection: 'EPSG:4326',
            projection: 'EPSG:3857'
        }),
    });
    plotSourceSAT.on('change', function (event) {
        map.getView().fit(plotSourceSAT.getExtent(), map.getSize())
    });
    var plotSAT = new ol.layer.Vector({
        name: 'PlotGPS',
        //            style: createPolygonStyleFunction
    });
    plotSAT.setSource(plotSourceSAT);
    map.addLayer(plotSAT);
    var adjPlotSource = new ol.source.Vector({
        url: '<?php echo url('ajax/GeoJSONplots/' . $selected_villages . '/' .$id); ?>',
        format: new ol.format.GeoJSON({
            defaultDataProjection: 'EPSG:4326',
            projection: 'EPSG:3857'
        }),
    });
    var adjplotSAT = new ol.layer.Vector({
        name: 'PlotGPS',
        source: adjPlotSource
    });
    map.addLayer(adjplotSAT);

    var select = new ol.interaction.Select({
        wrapX: false,
        layers: [plotSAT]
    });
    map.addInteraction(select);

    var modify = new ol.interaction.Modify({
        features: select.getFeatures()
    });

//            map.addInteraction(modify);
    map.addInteraction(modify);
    function updatePlotGeometry() {
//        format = new ol.format.GeoJSON();
        var features = plotSAT.getSource().getFeatures();
        var coords;
        features.forEach(function (feature) {
            var newCoord = feature.getGeometry().transform(new ol.proj.Projection({code: "EPSG:3857"}), new ol.proj.Projection({code: "EPSG:4326"}));
//        var newCoord = ol.proj.transform(feature.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326');
            coords = newCoord.getCoordinates();
        });
        // data = format.writeFeatures(coords);
        $('#www').html(coords)

        $.post("{{URL::to('/plot/edit-geometry/'.$id)}}", {'_token': '<?php echo csrf_token(); ?>', 'coords': coords}, function (data) {
            window.location.reload();
        })
    }
</script>
@endsection


