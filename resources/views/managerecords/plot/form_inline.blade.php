<?php
$mesages = '';
if (isset($errors) && !empty($errors)) {
    $mesages = $errors->plot->all();
}
?>
<div class="container">
    <?php
    if ($mesages) {
        foreach ($mesages as $key => $value) {
            echo $value;
        }
    }
    ?>   
    {!! Form::open(['url' => url('plot/submit-plot-data'),'class' => 'form-inline plot-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
        <div class="form-group">
            <label for="email">State<span>*</span>:</label>
            <select class="form-control required" name="state" id="state" style="width: 150px;" onchange="populateDistrict($(this).val());">
                <option value="">Select State</option>                        
            </select>
        </div>
        <div class="form-group">
            <label for="pwd">District<span>*</span>:</label>
            <select class="form-control required" id="district" name="district" onchange="populateBlock($(this).val())">
                <option value="">Select District</option>
            </select>           
        </div>
        <div class="form-group">
            <label for="email">Block<span>*</span>:</label>
            <select class="form-control required" id="block" name="block" onchange="populateVillage($(this).val())">
                <option value="">Select Block/Tehsil</option>                      
            </select>
        </div>
        <div class="form-group">
            <label for="pwd">Village<span>*</span>:</label>
            <select class="form-control required" id="village" name="plot[vil_id]" onchange="populateClaimant($(this).val());
            populateClaims($(this).val(), '')">
                <option value="">Select Village</option>                    
            </select>        
        </div>
        <br/>
        <div class="form-group">
            <label for="email">Claimant:</label>
            <select class="form-control" id="claimant" name="claimant" multiple="" onchange="populateClaims($('#village').val(), $(this).val());">
                <option value="">Select Claimant</option>                    
            </select>
        </div>
        <div class="form-group">
            <label for="pwd">Claim Number<span>*</span>:</label>
            <select class="form-control required" id="claim" name="plot[claim_id]">
                <option value="">Select Claim Number</option>                    
            </select>        
        </div>
        <div class="form-group">
            <label for="pwd">Plot No.<span>*</span>:</label>
            {{ Form::text('plot[plt_number]', isset($plot_data['plt_number']) ? $plot_data['plt_number'] : 0, array('class'=>'form-control required','placeholder' => 'Plot No')) }}
        </div>
        <div class="form-group">
            <label for="pwd">Survey No.: </label>
            {{ Form::text('plot[surveyno]', isset($plot_data['surveyno']) ? $plot_data['surveyno'] : 0, array('class'=>'form-control','placeholder' => 'Survey No')) }}
        </div>
        <div class="form-group">
            <label for="pwd">Compartment No.:</label>
            {{ Form::text('plot[compartment_no]', isset($plot_data['compartment_no']) ? $plot_data['compartment_no'] : 0, array('class'=>'form-control','placeholder' => 'Compartment No')) }}
        </div>
        <br/>
        <h4><u>Area</u></h4>
        <br/>
        <div class="form-group">
            <label for="pwd">Claimed Hectare:</label>
            {{ Form::text('plot[areaclaimedh]', isset($plot_data['areaclaimedh']) ? $plot_data['areaclaimedh'] : 0, array('class'=>'form-control','placeholder' => 'Area Claimed Hectare')) }}
        </div>
        <div class="form-group">
            <label for="pwd">Approved GS Hectare:</label>
            {{ Form::text('plot[areaapprovedgsh]', isset($plot_data['areaapprovedgsh']) ? $plot_data['areaapprovedgsh'] : 0, array('class'=>'form-control','placeholder' => 'Area Approved GS Hectare')) }}
        </div>
        <div class="form-group">
            <label for="pwd">Approved SDLC Hectare:</label>
            {{ Form::text('plot[areaapprovedsdlch]', isset($plot_data['areaapprovedsdlch']) ? $plot_data['areaapprovedsdlch'] : 0, array('class'=>'form-control','placeholder' => 'Area Approved SDLC Hectare')) }}
        </div>
        <div class="form-group">
            <label for="pwd">Approved DLC Hectare:</label>
            {{ Form::text('plot[areaapproveddlch]', isset($plot_data['areaapproveddlch']) ? $plot_data['areaapproveddlch'] : 0, array('class'=>'form-control','placeholder' => 'Area Approved DLC Hectare')) }}
        </div>
        <div class="form-group">
            <label for="pwd">GPS Survey Hectare:</label>
            {{ Form::text('plot[areagpssurveyh]', isset($plot_data['areagpssurveyh']) ? $plot_data['areagpssurveyh'] : 0, array('class'=>'form-control','placeholder' => 'Area GPS Survey Hectare')) }}
        </div>

        <br/>
        <input type="hidden" name="is_old" value="<?= isset($plot_data['is_old']) ? $plot_data['is_old'] : '' ?>">
        <input type="hidden" name="id" value="<?= isset($id) ? $id : '' ?>">
        <button type="submit" class="btn btn-default">Save</button>
    {!! Form::close() !!}
    