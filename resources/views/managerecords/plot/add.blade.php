@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div>
                    <ol class="breadcrumb">
                           <li><a href="<?= url('/') ?>">Home</a></li>                       
                          <li><a href="<?= url('claimant/list') ?>">Plot</a></li>
                        <li class="active">Add</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <h2>Add New PLot</h2>
    @include('managerecords.plot.form')
</div>
@endsection

