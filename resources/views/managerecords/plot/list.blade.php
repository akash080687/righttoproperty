@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-1 search">
                <a href="javascript:void(0);" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"></span>
                </a>
            </div>
            <div class="col-sm-11">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>                       
                        <li class="active">Plot</li>
                        <li class="pull-right"><a href="<?= url('plot/add') ?>">Add New Plot</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div>{!! Session::get('message')!!}

    </div>
    <div class="search-container">

        <div class="container">
            <?php
            $display_content_style = ($search == 1) ? 'style="display:block"' : 'style="display:none"';
            ?>
            <div class="display_panel_content" <?= $display_content_style ?>>
                <h2>Search Plot</h2>
                {!! Form::open(['url' => url('plot/list'),'class' => 'form-horizontal search-form','method' => 'GET', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
                <div class="box-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">                       
<!--                                        <select class="form-control required select2-minimum" name="state" id="state" onchange="populateDistrict($(this).val());">
                                            <option value="">Select State</option>                        
                                        </select>-->
                                        {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','style' => 'width:200px','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" id="district" style="width: 200px;" name="district" onchange="populateBlock($(this).val())">
                                            <option value="">Select District</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" id="block" style="width: 200px;" name="block" onchange="populateVillage($(this).val())">
                                            <option value="">Select Block/Tehsil</option>                      
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Village<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" id="village" style="width: 200px;" name="village" onchange="populateClaimant($(this).val());populateMoholla($(this).val());">
                                            <option value="">Select Village</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Mohalla:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="moholla" style="width: 200px;" name="moholla[]" multiple=""  onchange="populateClaimantByMoholla($(this).val());">
                                            <option value="">Select Mohalla</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Claimant:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="claimant" style="width: 200px;" name="claimant[]" onchange="populateClaims($('#village').val(), $(this).val())" multiple="">
                                            <option value="">Select Claimant</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div> 
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Claims:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" style="width: 200px;" id="claim" name="claim[]" multiple="">
                                            <option value="">Select Claim</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-5">
                            <button class="btn btn-info pull-right" type="submit" name="search" value="1">Search</button>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>

    <h2>Plot List</h2>
<!--    <p>The .table-condensed class makes a table more compact by cutting cell padding in half:</p>    -->
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Plot Id</th>
                    <th>Claim Id</th>
                    <th>Village</th>
                    <th>Plot No.</th>
                    <th>Survey No.</th>
                    <th>Compartment No.</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($plots) {
                    foreach ($plots as $key => $value) {
                        $vil_id = trim($value['village_id_master']);
                        $rtp_status = App\Models\Report\VillageModel::where(['vil_id' => "$vil_id"])->value('rtp_status');
                        if ($rtp_status == true) {
                            ?>
                            <tr>
                                <td><?= $value['is_old'] == true ? $value['old_id'] : $value['ogc_fid'] ?></td>
                                <td><?= $value['is_old'] == true ? $value['claim'] : $value['claim_id'] ?></td>
                                <td><?php
                                    //$vil_id = trim($value['vil_id']);

                                    $village_name = App\Models\Report\VillageModel::where(['vil_id' => "$vil_id"])->value('village_name');
                                    $village_ogc_fid = App\Models\Report\VillageModel::where(['vil_id' => "$vil_id"])->value('ogc_fid');
                                    ?>
                                    <a href="<?= url('village/update/' . $village_ogc_fid) ?>"><?= $village_name ?></a>
                                </td>
                                <td><?= $value['plt_number'] ?></td>
                                <td><?= $value['surveyno'] ?></td>
                                <td><?= $value['compartment_no'] ?></td>
                                <td><a href="<?= url('plot/update/' . $value['ogc_fid']) ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>

                            </tr>
                            <?php
                        }
                    }
                }
                ?>
            </tbody>
        </table>
        <?= $links ?>
    </div>
</div>
@endsection

