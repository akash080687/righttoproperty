<?php
$mesages = '';
if (isset($errors) && !empty($errors)) {
    $mesages = $errors->plot->all();
}
?>
<div class="container">
    <?php
    if ($mesages) {
        foreach ($mesages as $key => $value) {
            echo $value;
        }
    }
    //echo $wkb_geometry;
    ?>   
    {!! Form::open(['url' => url('plot/submit-plot-data'),'class' => 'form-horizontal add-edit-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
    <div class="box-body">

        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">                       
<!--                            <select class="form-control required" name="state" id="state" onchange="populateDistrict($(this).val());">
                                <option value="">Select State</option>                        
                            </select>-->
                            {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="district" name="district" onchange="populateBlock($(this).val())">
                                <option value="">Select District</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="block" name="block" onchange="populateVillage($(this).val())">
                                <option value="">Select Block/Tehsil</option>                      
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"  for="inputEmail3">Village<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">                       
                            <select class="form-control required" id="village" name="plot[village_id_master]" onchange="populateMoholla($(this).val());populateClaimant($(this).val());
                                    populateClaims($(this).val(), '')">
                                <option value="">Select Village</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"  for="inputEmail3">Mohalla:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="moholla" style="width: 200px;" name="plot[mohallah_id_master]"  onchange="populateClaimantByMoholla($(this).val());">
                                <option value="">Select Mohalla</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Claimant<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="claimant" name="claimant" onchange="populateClaims($('#village').val(), $(this).val());">
                                <option value="">Select Claimant</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Claim Number<span style="color: red">*</span>:</label>
                        <div class="col-sm-7">
                            <select class="form-control required" id="claim" name="plot[claim_id]">
                                <option value="">Select Claim Number</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"  for="inputEmail3">Plot No.<span style="color: red">*</span>:</label>
                        <div class="col-sm-8">                       
                            {{ Form::text('plot[plt_number]', isset($plot_data['plt_number']) ? $plot_data['plt_number'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'Plot No')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Survey No.: </label>
                        <div class="col-sm-8">
                            {{ Form::text('plot[surveyno]', isset($plot_data['surveyno']) ? $plot_data['surveyno'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Survey No')) }}
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Compartment No.:</label>
                        <div class="col-sm-7">
                            {{ Form::text('plot[compartment_no]', isset($plot_data['compartment_no']) ? $plot_data['compartment_no'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Compartment No')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label"  for="inputEmail3">Area Claimed Hectare:</label>
                        <div class="col-sm-7">                       
                            {{ Form::text('plot[areaclaimedh]', isset($plot_data['areaclaimedh']) ? $plot_data['areaclaimedh'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Area Claimed Hectare')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Area Approved GS Hectare.: </label>
                        <div class="col-sm-7">
                            {{ Form::text('plot[areaapprovedgsh]', isset($plot_data['areaapprovedgsh']) ? $plot_data['areaapprovedgsh'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Area Approved GS Hectare')) }}
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Area Approved SDLC Hectare.:</label>
                        <div class="col-sm-7">
                            {{ Form::text('plot[areaapprovedsdlch]', isset($plot_data['areaapprovedsdlch']) ? $plot_data['areaapprovedsdlch'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Area Approved SDLC Hectare')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label"  for="inputEmail3">Area Approved DLC Hectare:</label>
                        <div class="col-sm-7">                       
                            {{ Form::text('plot[areaapproveddlch]', isset($plot_data['areaapproveddlch']) ? $plot_data['areaapproveddlch'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Area Approved DLC Hectare')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Area GPS Survey Hectare: </label>
                        <div class="col-sm-7">
                            {{ Form::text('plot[areagpssurveyh]', isset($plot_data['areagpssurveyh']) ? $plot_data['areagpssurveyh'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Area GPS Survey Hectare')) }}
                        </div>
                    </div>
                </div>
            </div>           

        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Auto calculated area (Hectare): </label>
                        <div class="col-sm-7">
                            <input type="text" disabled="true" value="{{isset($gps_area) ?$gps_area : '' }}" />
                        </div>
                    </div>
                </div>
            </div>   
        </div>
        <input type="hidden" name="is_old" value="<?= isset($plot_data['is_old']) ? $plot_data['is_old'] : '' ?>">
        <input type="hidden" name="id" value="<?= isset($id) ? $id : '' ?>">
        <div class="box-footer">
            <div class="col-sm-5">
                <button class="btn btn-info pull-right" type="submit">Save</button>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}