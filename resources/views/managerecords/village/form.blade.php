<?php
$mesages = '';
if (isset($errors) && !empty($errors)) {
    $mesages = $errors->village->all();
}
?>
<div class="container">
    <?php
    if ($mesages) {
        foreach ($mesages as $key => $value) {
            echo $value;
        }
    }
    // t($village_data);
    ?>   
    {!! Form::open(['url' => url('village/submit-village-data'),'class' => 'form-horizontal add-edit-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
    <div class="box-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">                       
                            {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','id' => 'state','onchange' => 'populateDistrictMaster($(this).val())'))}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="district" name="district" onchange="populateBlockMaster($(this).val())">
                                <option value="">Select District</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="block" name="village[block_id]" filter="all" onchange="populateVillageMaster($(this).val())">
                                <option value="">Select Block/Tehsil</option>                      
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"  for="inputEmail3">Village (Census 2011)<span style="color: red">*</span>:</label>
                        <div class="col-sm-8">
                            <select class="form-control required" id="village" name="village_id" onchange="populateMoholla($(this).val())">
                                <option value="">Select Village</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>   
            <?php
            $rtp_status = isset($village_data['rtp_status']) ? $village_data['rtp_status'] : true;
            ?>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Enable:</label>
                        <div class="col-sm-8">
                            <?php if (isset($id) && $id) { ?>
                                {{ Form::checkbox('village[rtp_status]', $rtp_status,$rtp_status == true ?true:false )}}                             
                            <?php } else { ?>
                                {{ Form::checkbox('village[rtp_status]', $rtp_status,false)}}
                            <?php } ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Pincode:</label>
                        <div class="col-sm-8">
                            {{ Form::text('village[pincode]', isset($village_data['pincode']) ? $village_data['pincode'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Pincode')) }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--        <div class="row">
                   
                    
        
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputEmail3">GPS:</label>
        
                                <div class="col-sm-9">
                                    {{ Form::textarea('village[gps]', isset($village_data['gps']) ? $village_data['gps'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'GPS','size' => '30x3')) }}
                                </div>
        
                            </div>
                        </div>
                    </div>
                </div>-->


<!--        <input type="hidden" name="is_old" value="<?= isset($village_data['is_old']) ? $village_data['is_old'] : '' ?>">-->
        <input type="hidden" name="id" value="<?= isset($id) ? $id : '' ?>">
        <div class="box-footer">
            <div class="col-sm-5">
                <button class="btn btn-info pull-right" type="submit">Save</button>
                <i style="font-size: 24px; visibility: hidden;" id="loader" class="fa fa-spinner fa-spin"></i>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}