@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-1 search">
                <a href="javascript:void(0);" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"></span>
                </a>
            </div>
            <div class="col-sm-11">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>         
                        <li class="active">Village</li>
                        <li class="pull-right"><a href="<?= url('village/add') ?>">Add New Village</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div>{!! Session::get('message')!!}</div>
    <div class="search-container">

        <?php
        $display_content_style = ($search == 1) ? 'style="display:block"' : 'style="display:none"';
        ?>
        <div class="container">
            <div class="display_panel_content" <?= $display_content_style ?>>
                <h2>Search Village</h2>
                {!! Form::open(['url' => url('village/list'),'class' => 'form-horizontal search-form','method' => 'GET', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">                       
                                         {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','style' => 'width:200px','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" style="width: 200px;" id="district" name="district" onchange="populateBlock($(this).val())">
                                            <option value="">Select District</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control required" style="width: 200px;" filter="all" villages_for_rtp="true" id="block" name="block" onchange="populateVillage($(this).val())">
                                            <option value="">Select Block/Tehsil</option>                      
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"  for="inputEmail3">Village:</label>
                                    <div class="col-sm-9">
                                        <select class="form-control " id="village" style="width: 200px;" name="village[]"  multiple="" onchange="">
                                            <option value="">Select Village</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                       

                    <!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-sm-5">
                            <button class="btn btn-info pull-right" type="submit" name="search" value="1">Search</button>
                        </div>
                    </div>
                </div>
                <!-- /.box-footer -->
                {!! Form::close() !!}
            </div>
        </div>
    </div>



    <h2>Village List</h2>
<!--    <p>The .table-condensed class makes a table more compact by cutting cell padding in half:</p>    -->
    <div class="table-responsive">
        <table class="table table-condensed">
            <thead>
                <tr>
                    <!--<th>Village Id</th>-->
                    <th>Village Code</th>
                    <th>Village Name</th>
                    <th>Enable</th>
                    <th>Tehsil</th>
<!--                    <th>Pincode</th>                   -->
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($villages) {
                    foreach ($villages as $key => $value) {
                        ?>
                        <tr>
                    <!--<td><?php // echo  $value['is_old'] == true ? $value['old_id'] : $value['ogc_fid'] ?></td>-->
                            <td><?= $value['census2011'] ?></td>
                            <td><?= $value['village_name'] ?></td>
                            <td><?= $value['rtp_status'] == 1 ? 'Yes' : 'No' ?></td>
                            <td><?php
                                $block_id = $value['block_id'];
                                echo $block_name = App\Models\Report\BlockModel::where(['ogc_fid' => $block_id])->value('block_name');
                                // $value['village'] 
                                ?>
                            </td>
<!--                            <td></td>-->

                            <td>
                                <a href="<?= url('village/update/' . $value['ogc_fid']) ?>" title="Edit Village"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                <a href="<?= url('delete-survey-data/' . $value['vil_id']) ?>" onclick="return confirm('To delete all survey data under this village please confirm.');" title="Delete Survey Data" target="_blank" ><i class="fa fa-trash-o" style="color: red;" aria-hidden="true"></i></a>
                            </td>

                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
        <?= $links ?>
    </div>
</div>
@endsection

