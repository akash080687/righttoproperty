@extends('layouts.common')
@section('content')
<div class="container">
    {!! Form::open(['class' => 'form-horizontal','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
    <div class="box-body">
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputEmail3">Village Id:</label>

            <div class="col-sm-5">
                <input type="email" placeholder="Village Id"  class="form-control col-xs-5">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputEmail3">Village Code: </label>

            <div class="col-sm-5">
                <input type="email" placeholder="Village Code"  class="form-control col-xs-5">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputEmail3">Village Name:</label>
            <div class="col-sm-5">
                <input type="email" placeholder="Village Name"  class="form-control col-xs-5">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputEmail3">GPS:</label>
            <div class="col-sm-5">
                <textarea placeholder="Enter ..." rows="3" class="form-control col-xs-5"></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputEmail3">Enable:</label>
            <div class="col-sm-5">
                <input type="email"   class="form-control col-xs-5">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputEmail3">Pincode:</label>
            <div class="col-sm-5">
                <input type="email" placeholder="Pincode"  class="form-control col-xs-5">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputEmail3">State:</label>
            <div class="col-sm-5">
                <select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="inputEmail3">District:</label>
            <div class="col-sm-5">
                <select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                </select>
            </div>
        </div>
        <!--                <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox"> Remember me
                              </label>
                            </div>
                          </div>
                        </div>-->
    </div>
    <!--    <button type="submit" class="btn btn-default">Submit</button>-->
    {!! Form::close() !!}
</div>
@endsection

