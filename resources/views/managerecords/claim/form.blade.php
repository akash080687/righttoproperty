<?php
$mesages = '';
if (isset($errors) && !empty($errors)) {
    $mesages = $errors->claim->all();
}
?>
<div class="container">
    <?php
    if ($mesages) {
        foreach ($mesages as $key => $value) {
            echo $value;
        }
    }
    ?>   
    {!! Form::open(['url' => url('claim/submit-claim-data'),'class' => 'form-horizontal add-edit-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
    <div class="box-body">


        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">                       
                            {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id  : $selected_state,array('class'=>'form-control select2-minimum required','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="district" name="district" onchange="populateBlock($(this).val())">
                                <option value="">Select District</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">
                            <select class="form-control required" id="block"  name="block" onchange="populateVillage($(this).val())">
                                <option value="">Select Block/Tehsil</option>                      
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Village<span style="color: red">*</span>:</label>
                        <div class="col-sm-9">                       
                            <select class="form-control required" id="village" name="claim[village_id_master]" onchange="populateMoholla($(this).val());
                                    populateClaimant($(this).val());">
                                <option value="">Select Village</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"  for="inputEmail3">Mohalla:</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="moholla" style="width: 200px;" name="claim[mohallah_id_master]"  onchange="populateClaimantByMoholla($(this).val());">
                                <option value="">Select Mohalla</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <!--            <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="inputEmail3">Claimant Id:</label>
            <?php if (isset($claim_data['is_old']) && $claim_data['is_old'] == 1) { ?>
                                            <div class="col-sm-8">
                                                {{ Form::text('claim[claimant]', isset($claim_data['claimant']) ? $claim_data['claimant'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'Claimant ID')) }}
                                            </div>
            <?php } else { ?>
                                            <div class="col-sm-8">
                                                {{ Form::text('claim[claimant_id]', isset($claim_data['claimant_id']) ? $claim_data['claimant_id'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'Claimant ID')) }}
                                            </div>
            <?php } ?>
                                </div>
                            </div>
                        </div>-->
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label"  for="inputEmail3">Claimant ID<span style="color: red">*</span>:</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="claimant" name="claim[claimant_id]" required="">
                                <option value="">Select Claimant</option>                    
                            </select>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Claim No.<span style="color: red">*</span>:</label>
                        <div class="col-sm-8">
                            {{ Form::text('claim[number]', isset($claim_data['number']) ? $claim_data['number'] : '', array('class'=>'form-control col-xs-5 required','placeholder' => 'Claim No.')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Total Area Claimed:</label>
                        <div class="col-sm-8">                       
                            {{ Form::text('claim[tareaclaimed]', isset($claim_data['tareaclaimed']) ? $claim_data['tareaclaimed'] : 0, array('class'=>'form-control col-xs-5','placeholder' => 'Total Area Claimed')) }}
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($is_old): ?>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">GS Decision.:</label>
                            <div class="col-sm-8">
                                {{ Form::text('claim[gsdecision]', isset($claim_data['gsdecision']) ? $claim_data['gsdecision'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'GS Decision')) }}
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-5 control-label" for="inputEmail3">Area Approved GS:</label>
                        <div class="col-sm-7">
                            {{ Form::text('claim[areaapprovedgsha]', isset($claim_data['areaapprovedgsha']) ? $claim_data['areaapprovedgsha'] : 0, array('class'=>'form-control col-xs-5','placeholder' => 'Area Approved SDLC')) }}
                        </div>
                    </div>
                </div>
            </div>
            <?php if (!$is_old): ?>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-5 control-label" for="inputEmail3">Area GPS:</label>
                            <div class="col-sm-7">
                                {{ Form::text('claim[t_area_gps_ha]', isset($claim_data['t_area_gps_ha']) ? $claim_data['t_area_gps_ha'] : 0, array('class'=>'form-control col-xs-5','placeholder' => 'Area GPS')) }}
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="row">
            <?php if ($is_old) { ?>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">SDLC Decision:</label>
                            <div class="col-sm-8">                       
                                {{ Form::text('claim[sdlcdecision]', isset($claim_data['sdlcdecision']) ? $claim_data['sdlcdecision'] : 0, array('class'=>'form-control col-xs-5','placeholder' => 'Total Area Claimed')) }}
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">Area SAT:</label>
                            <div class="col-sm-8">
                                {{ Form::text('claim[t_area_sat_ha]', isset($claim_data['t_area_sat_ha']) ? $claim_data['t_area_sat_ha'] : 0, array('class'=>'form-control col-xs-5','placeholder' => 'Area SAT')) }}
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-sm-4">
                <?php //if ($is_old) {  ?>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Area Approved SDLC.:</label>
                        <div class="col-sm-8">
                            {{ Form::text('claim[areaapprovedsdlcha]', isset($claim_data['areaapprovedsdlcha']) ? $claim_data['areaapprovedsdlcha'] : 0, array('class'=>'form-control col-xs-5','placeholder' => 'Area Approved SDLC')) }}
                        </div>
                    </div>
                </div>
                <?php if (!$is_old) { ?>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">Area GS2:</label>
                            <div class="col-sm-8">
                                {{ Form::text('claim[t_area_gs2_ha]', isset($claim_data['t_area_gs2_ha']) ? $claim_data['t_area_gs2_ha'] : 0, array('class'=>'form-control col-xs-5','placeholder' => 'Area GS2')) }}
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php if ($is_old) { ?>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-5 control-label" for="inputEmail3">DLC Decision:</label>
                            <div class="col-sm-7">
                                {{ Form::text('claim[dlcdecision]', isset($claim_data['dlcdecision']) ? $claim_data['dlcdecision'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'DLC Decision')) }}
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-5 control-label" for="inputEmail3">Area SDLC2:</label>
                            <div class="col-sm-7">
                                {{ Form::text('claim[t_area_sdlc2_ha]', isset($claim_data['t_area_sdlc2_ha']) ? $claim_data['t_area_sdlc2_ha'] : 0, array('class'=>'form-control col-xs-5','placeholder' => 'Area SDLC2')) }}
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <?php //if ($is_old) {  ?>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="inputEmail3">Area Approved DLC:</label>
                        <div class="col-sm-8">                       
                            {{ Form::text('claim[areaapproveddlcha]', isset($claim_data['areaapproveddlcha']) ? $claim_data['areaapproveddlcha'] : 0, array('class'=>'form-control col-xs-5 ','placeholder' => 'Area Approved DLC')) }}
                        </div>
                    </div>
                </div>
            </div>
            <?php // } else { ?>
            <?php if (!$is_old) { ?>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">Status SDLC2:</label>
                            <div class="col-sm-8">                       
                                {{ Form::text('claim[status_sdlc2]', isset($claim_data['status_sdlc2']) ? $claim_data['status_sdlc2'] : '', array('class'=>'form-control col-xs-5 ','placeholder' => 'Status SDLC2')) }}
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-sm-4">
                <?php if ($is_old) { ?>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">Appeal Field:</label>
                            <div class="col-sm-8">
                                {{ Form::text('claim[appealfield]', isset($claim_data['appealfield']) ? $claim_data['appealfield'] : '', array('class'=>'form-control col-xs-5','placeholder' => 'Appeal Field')) }}
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">Area DLC2:</label>
                            <div class="col-sm-8">
                                {{ Form::text('claim[t_area_dlc2_ha]', isset($claim_data['t_area_dlc2_ha']) ? $claim_data['t_area_dlc2_ha'] : 0, array('class'=>'form-control col-xs-5','placeholder' => 'Area DLC2')) }}
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php if (!$is_old) { ?>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">Claim Status:</label>
                            <div class="col-sm-8">                       
                                {{ Form::text('claim[claim_status]', isset($claim_data['claim_status']) ? $claim_data['claim_status'] : '', array('class'=>'form-control col-xs-5 ','placeholder' => 'Claim Status')) }}
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
        <?php if (!$is_old) { ?>
            <div class="row">


                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">Final Status:</label>
                            <div class="col-sm-8">                       
                                {{ Form::text('claim[final_status]', isset($claim_data['final_status']) ? $claim_data['final_status'] : '', array('class'=>'form-control col-xs-5 ','placeholder' => 'Final Status')) }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">Adhikar Patra Recd:</label>
                            <div class="col-sm-8">   
                                {{ Form::checkbox('claim[adhikar_patra_recd]',1,isset($claim_data['adhikar_patra_recd']) && $claim_data['adhikar_patra_recd'] == true? true : false)}}
                                {{-- Form::text('claim[adhikar_patra_recd]', isset($claim_data['adhikar_patra_recd']) && $claim_data['adhikar_patra_recd'] ? 'yes' : 'no', array('class'=>'form-control col-xs-5 ','placeholder' => 'Adhikar Patra Recd')) --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="inputEmail3">Adhikar Patra Date:</label>
                            <div class="col-sm-8">      
                                <?php
                                $adhikar_patra_date = isset($claim_data['adhikar_patra_date']) && !empty($claim_data['adhikar_patra_date']) ? new \DateTime($claim_data['adhikar_patra_date']) : null;
                                $adhikar_patra_date = ($adhikar_patra_date) ? $adhikar_patra_date->format('d/m/Y') : '';
                                ?>
                                {{ Form::text('claim[adhikar_patra_date]', isset($adhikar_patra_date) && ($adhikar_patra_date != '01/01/0001')  ? $adhikar_patra_date : null, array('class'=>'form-control col-xs-5' ,'id' => 'datepicker-13','placeholder' => 'Adhikar Patra Date')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <input type="hidden" name="is_old" value="<?= isset($claim_data['is_old']) ? $claim_data['is_old'] : '' ?>">
        <input type="hidden" name="id" value="<?= isset($id) ? $id : '' ?>">
        <div class="box-footer">
            <div class="col-sm-5">
                <button class="btn btn-info pull-right" type="submit">Save</button>
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}