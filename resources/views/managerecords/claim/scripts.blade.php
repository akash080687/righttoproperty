
<script>
    slected_state = "{{ isset($selected_state) ? $selected_state : Request::get('state') }}";
    slected_dist = "{{ isset($selected_district) ? $selected_district :Request::get('district') }}";
    slected_block = "{{ isset($selected_block) ? $selected_block : Request::get('block') }}";
    slected_village = "{{ isset($selected_village) ? $selected_village :Request::get('village') }}";
    slected_claimant = "{{ isset($selected_claimants)? $selected_claimants : '' }}";
    //alert(slected_claimant);
    function populateDistrict(state_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#district').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/districts') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'state_id': state_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
                //alert(slected_dist);

                $.each(data, function (index, value) {
                    var selected = (slected_dist == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#district').append('<option ' + selected + 'value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (slected_block) {
                    populateBlock($('#district').val());
                }
                $('.display_panel_content').css('cursor', '');
            },
        });
    }

    function populateBlock(dist_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#block').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/blocks') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'dist_id': dist_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)

                $.each(data, function (index, value) {
                    var selected = (slected_block == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#block').append('<option ' + selected + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (slected_block) {
                    populateVillage($('#block').val());
                }
                $('.display_panel_content').css('cursor', '');
            },
        });
    }

    function populateVillage(block_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#village').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/villages') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'block_id': block_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)

                $.each(data, function (index, value) {
                    var selected = (slected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#village').append('<option ' + selected + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (slected_village != '') {
                    populateClaimant($('#village').val());
                }
                $('.display_panel_content').css('cursor', '');
            },
        });
    }

    function populateClaimant(village_id) {
        $('#claimant').children(':not(:first-child)').remove();
        $('.display_panel_content').css('cursor', 'wait');
        $.ajax({
            url: '<?= url('ajax/claimants-by-village') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'village_id': village_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                // console.log(data)
                $.each(data, function (index, value) {
                    //var selected = (slected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#claimant').append('<option ' + '' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                $('#claimant').val(slected_claimant.split(','));
                $('.display_panel_content').css('cursor', '');
            },
        });
    }
    $(function () {
        $('.close').on('click', function () {
            $(this).parents('.alert-success').hide();
            $(this).parents('.alert-danger').hide();
        });

        $('.claim-form').on('submit', function () {
            var error = 0;
            $('.required').each(function () {
                var val = $(this).val();
                if (!val) {
                    error++;
                    $(this).parents('.form-group').addClass('has-error');
                } else {
                    $(this).parents('.form-group').removeClass('has-error');
                }
            });
            if (error > 0) {
                return false;
            }
        });

        $('.claim-search-form').on('submit', function () {
            var error = 0;
            $('.required').each(function () {
                var val = $(this).val();
                if (!val) {
                    error++;
                    $(this).parents('.form-group').addClass('has-error');
                } else {
                    $(this).parents('.form-group').removeClass('has-error');
                }
            });
            if (error > 0) {
                return false;
            }
        });
    });
     $(window).on('load', function () {
        $.ajax({
            url: '<?= url('ajax/states') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>'
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
                $.each(data, function (index, value) {
                    var selected = (selected_state == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#state').append('<option ' + selected + '  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_state) {
                    populateDistrict($('#state').val());
                }
            },
        });
    });
</script>
