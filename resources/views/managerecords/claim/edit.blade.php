@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div>
                    <ol class="breadcrumb">
                         <li><a href="<?= url('/') ?>">Home</a></li>                      
                        <li><a href="<?= url('claim/list') ?>">Claim</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div>{!! Session::get('message')!!}</div>
    <h2>Edit Claim</h2>
    @include('managerecords.claim.form')
</div>
@endsection


