@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <!--            <div class="col-sm-1 search">
                            <a href="javascript:void(0);" class="btn btn-info">
                                <span class="glyphicon glyphicon-search"></span>
                            </a>
                        </div>-->
            <div class="col-sm-12">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>                
                        <li class="active">Import Survey Data</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div>{!! Session::get('message')!!}</div>
    <?php
    $errors = Session::get('errors');
    if ($errors) {
        foreach ($errors as $key => $value) {
            echo $value;
        }
    }
    //  t($states);
    ?>
    <div class="search-container">

        <?php
        //$display_content_style = ($search == 1) ? 'style="display:block"' : 'style="display:none"';
        ?>
        <div class="container">
            <div class="display_panel_content">
                <!--                <h2>Search Village</h2>-->
                {!! Form::open(['url' => url('import/submit-survey-data'),'class' => 'form-horizontal search-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="row">
                                <div class="form-group">

                                    <label class="col-sm-4 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                                    <div class="col-sm-8">                       
<!--                                        <select  class="form-control required select2-minimum" name="state" id="state" onchange="populateDistrict($(this).val());">
                                            <option value="">Select State</option> 
                                            
                                        </select>-->
                                        {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id !== 'all'  ? $logged_in_user_boudary_id : $selected_state,array('class'=>'form-control select2-minimum required','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control required" id="district" name="district" onchange="populateBlock($(this).val())">
                                            <option value="">Select District</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control required" id="block" from="import_page" filter="all" name="block" onchange="populateVillage($(this).val())">
                                            <option value="">Select Block/Tehsil</option>                      
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label"  for="inputEmail3">Village<span style="color: red">*</span>:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control required" id="village" name="village" onchange="populateMoholla($(this).val())">
                                            <option value="">Select Village</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

					
					<div class="row">
					<div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label"  for="inputEmail3"> Mohalla for gpx Upload:</label>
                                    <div class="col-sm-7">
                                        <select class="form-control" id="moholla" style="width: 200px;" name="moholla"  onchange="">
                                            <option value="">Select Mohalla</option>                    
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
					</div>
                    <div class="row">                        
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label"  for="inputEmail3">Select Excel File<span style="color: red">*</span>:</label>
                                    <div class="col-sm-7">
                                        <input class="required" type="file" name="import_file" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="row">
                                <div class="form-group">
                                    <!--                                    <label class="col-sm-5 control-label"  for="inputEmail3">Select Excel File:</label>-->
                                    <div class="col-sm-12">
                                        <button class="btn btn-info" type="submit" name="submit_excel" value="1">Upload Excel File</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                       


                    <!-- /.box-body -->
                    <!--                    <div class="box-footer">
                                            <div class="col-sm-5">
                                                <button class="btn btn-info pull-right" type="submit" name="submit" value="1">Submit</button>
                                            </div>
                                        </div>-->
                </div>
                <!-- /.box-footer -->
                {!! Form::close() !!}
			<div class="row">
                {!! Form::open(['url' => url('import/sat-gpx-data-upload'),'class' => 'form-horizontal sat','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-5 control-label"  for="">Select SAT GPX File<span style="color: red">*</span>:</label>
                            <div class="col-sm-7">
                                <input class="required" type="file" name="sat_gpx_file" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="form-group">
                            <!--                                    <label class="col-sm-5 control-label"  for="inputEmail3">Select Excel File:</label>-->
                            <div class="col-sm-12">
                                <input type="hidden" name="state" class="gpx_state" value="">
                                <input type="hidden" name="district" class="gpx_district" value="">
                                <input type="hidden" name="block" class="gpx_block" value="">
                                <input type="hidden" name="village" class="gpx_village" value="">
                                <input type="hidden" name="moholla" class="gpx_moholla" value="">
                                <button class="btn btn-info" type="submit" name="submit_gpx" value="1">Upload SAT File</button>
                            </div>
                        </div>
                    </div>
                </div>  
			</div>
                {!! Form::close() !!}

                {!! Form::open(['url' => url('import/gps-gpx-data-upload'),'class' => 'form-horizontal gps','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}

                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-5 control-label"  for="">Select GPS GPX File<span style="color: red">*</span>:</label>
                            <div class="col-sm-7">
                                <input class="" type="file" name="gps_gpx_file" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="form-group">                                                  
                            <div class="col-sm-12">
                                <input type="hidden" name="state" class="gpx_state" value="">
                                <input type="hidden" name="district" class="gpx_district" value="">
                                <input type="hidden" name="block" class="gpx_block" value="">
                                <input type="hidden" name="village" class="gpx_village" value="">
                                <input type="hidden" name="moholla" class="gpx_moholla" value="">
                                <button class="btn btn-info" type="submit" name="submit_gpx" value="1">Upload GPS File</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>




</div>
@endsection

