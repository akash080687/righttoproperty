@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-11">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>                       
                        <li class="active">Registration</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div>{!! Session::get('message')!!}</div>



    <?php
    if (!empty($errors)) {
        $mesages = $errors->registration->all();
    }
    ?>

    @if (count($mesages) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->registration->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    {!! Form::open(['url' => url('registration'),'class' => 'form-horizontal search-form','data-toggle'=>'validator','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
    <!--<form class="form-horizontal">-->
    <div class="form-group">

        <div class="col-sm-6">
            <label for="">First Name</label><?php if (!empty($firstname)) echo $firstname; ?>
            <!--<input type="text" id="" class="form-control" name="firstname" placeholder="First Name">-->
            {{ Form::text('firstname', '', array('class'=>'form-control','placeholder' => 'First Name')) }}
        </div>

        <div class="col-sm-6">
            <label for="">Last Name</label>
            <!--<input type="text" id="" class="form-control" name="lastname" placeholder="Last Name">-->
            {{ Form::text('lastname', '', array('class'=>'form-control','placeholder' => 'Last Name')) }}
        </div>
    </div>
    <div class="form-group">

        <div class="col-sm-6">
            <label for="">Email</label>
            <!--<input type="email" id="" name="email" class="form-control" placeholder="Email">-->
            {{ Form::email('email', '', array('class'=>'form-control','placeholder' => 'Email')) }}
        </div>

        <div class="col-sm-6">
            <label for="">Contact</label>
            <!--<input type="text" id="" name="contactno" class="form-control" placeholder="Contact">-->
            {{ Form::text('contactno', '', array('class'=>'form-control','placeholder' => 'Contact')) }}
        </div>
    </div>
    <div class="form-group">

        <div class="col-sm-6">
            <label for="">Password</label>
            <!--<input type="text" id="" name="password" class="form-control" placeholder="Password">-->
            {{ Form::password('password',  array('class'=>'form-control','placeholder' => 'Password')) }}
        </div>

        <div class="col-sm-6">
            <label for="">Confirm Password</label>
            <!--<input type="text" id="" name="confirmPass" class="form-control" placeholder="Confirm Password">-->
            {{ Form::password('confirmPass', array('class'=>'form-control','placeholder' => 'Confirm Password')) }}
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6">
            <label class="control-label" for="comment">How do you want to access the site ?</label>
            <!--<textarea id="comment" name="way_to_use" class="form-control" rows="5" ></textarea>-->
            {{ Form::textarea('way_to_use', '',array('class'=>'form-control','rows'=>5 )) }}
        </div>
        <div class="col-sm-6">
            <label class="control-label" for="comment">&nbsp;&nbsp;</label>
            <script src='https://www.google.com/recaptcha/api.js'></script>
            <!--<div class="g-recaptcha" data-sitekey="6LeNoB4UAAAAABpYf-0h0x0fg4P0zTWt08qAFvHu"></div>
			-->.
			
        </div>
    </div>
    <div class="form-group">
	<button
class="btn btn-info align-center" type="submit" value="Submit"
data-sitekey="6Lc45kcUAAAAALrqjkX3ZeatPLOTkcjInLHCDEPe"
data-callback="YourOnSubmitFn">
Submit
</button>
         <!--<input type="submit" name="submit" class="btn btn-info align-center" value="Submit">-->
    </div>
    <!--</form>-->
    {!! Form::close() !!}

</div>
@endsection

