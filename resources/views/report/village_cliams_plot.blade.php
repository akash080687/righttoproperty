<?php //echo $claimstat;exit;         ?>
@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">      
        <div class="container print_hide">
            <div class="row">
                <div class="col-sm-1 search">
                    <a href="javascript:void(0);" class="btn btn-info">
                        <span class="glyphicon glyphicon-search"></span>
                    </a>
                </div>
                <div class="col-sm-11">
                    <div>
                        <ol class="breadcrumb">
                            <li><a href="<?= url('/') ?>">Home</a></li>                     
                            <li><a href="javascript:void(0);">Report</a></li>
                            <li class="active">Village Claims Plot</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container minbody_height">
    <div>{!! Session::get('message')!!}</div>
    <div class="search-container">

        <?php
		
        $search = 1;
        $display_content_style = ($search == 1) ? 'style="display:block"' : 'style="display:none"';
        ?>
        <div class="display_panel_content" <?= $display_content_style ?>>
            <h2 class="print_hide">Filter Search</h2>
            {!! Form::open(['url' => 'report/villge-claims-plot','class' => 'form-horizontal search-form','method' => 'GET', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
            <div class="box-body print_hide">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputEmail3">State<span style="color: red">*</span>:</label>
                                <div class="col-sm-9">                       
<!--                                            <select class="form-control required select2-minimum" name="state" id="state" onchange="populateDistrict($(this).val());">
                                        <option value="">Select State</option>                        
                                    </select>-->
                                    {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id : $selected_state,array('class'=>'form-control select2-minimum required','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                                </div>
                            </div>
                        </div>
                    </div>
					 
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputEmail3">District<span style="color: red">*</span>:</label>
                                <div class="col-sm-9">
                                    <select class="form-control required" id="district" name="district" onchange="populateBlock($(this).val())">
                                        <option value="">Select District</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="inputEmail3">Block<span style="color: red">*</span>:</label>
                                <div class="col-sm-9">
                                    <select class="form-control required" filter="all" id="block" name="block" onchange="populateVillage($(this).val())">
                                        <option value="">Select Block/Tehsil</option>                      
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"  for="inputEmail3">Village<span style="color: red">*</span>:</label>
                                <div class="col-sm-9">
                                    <select class="form-control required" id="village" name="village" onchange="populateMoholla($(this).val());
                                            populateClaimant($(this).val())">
                                        <option value="">Select Village</option>                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"  for="inputEmail3">Mohalla:</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="moholla" style="width: 200px;" name="moholla"  onchange="populateClaimantByMoholla($(this).val())">
                                        <option value="">Select Mohalla</option>                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-4 control-label"  for="inputEmail3">Claimant ID:</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="claimant" name="claimant[]" onchange="populateClaims($('#village').val(), $(this).val())" multiple="">
                                        <option value="">Select Claimant</option>                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"  for="inputEmail3">Claim No:</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="claim" name="claim[]" multiple="">
                                        <option value="">Select Claim</option>                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"  for="claimstatus">Claim Status:</label>
                                <div class="col-sm-9">
                                    <select class="form-control" id="claimstatus" name="claimstat" >
                                        <option value="-1">Select All</option>                    
                                        <option value="A1" <?php echo($claimstat == 'A1' ? ' selected="true" ' : ''); ?> >A1 - Approved & no dispute</option>                    
                                        <option value="A2" <?php echo( $claimstat == 'A2' ? ' selected="true" ' : ''); ?>>A2 - Approved but area disputed</option>                    
                                        <option value="P" <?php echo($claimstat == 'P' ? ' selected="true" ' : ''); ?>>P - Pending</option>                    
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <!--<div class="col-sm-5">-->
                    <!--<button class="btn btn-info pull-right" type="submit" name="search" value="1">Search</button>-->
                    <!--</div>-->
                    <div class="row">
                        <div style="width:30%; margin:0 auto">



                            <div class="col-sm-4">
                                <button class="btn btn-info pull-right" type="submit" name="search" value="1">Search</button>
                            </div>


                            <div class="col-sm-4">
                                <p class="btn btn-info pull-right"><a href="http://www.righttoproperty:8080/report/villge-claims-plot" >Reset</a></p>
                            </div>

                            <div class="col-sm-4">
                                <button class="btn btn-info pull-right" onclick="$('#printablereport').print();">Print</button>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-footer -->
                    {!! Form::close() !!}
                </div>
                <!--                        <div class="box-footer">
                                            <div class="col-sm-5">
                                                <button class="btn btn-info pull-right" onclick="window.location.href = window.location.href.split('?')[0]">Reset</button>
                                            </div>
                                        </div>-->
                <!--                        <div class="box-footer">
                                            <div class="col-sm-5">
                                                <button class="btn btn-info pull-right" onclick="$('#printablereport').print();">Print</button>
                                            </div>
                                        </div>-->
            </div>
        </div>
        <?php if (isset($claimants) && !empty($claimants)) {
             if($claim_status == 'P'){
                 $claim_status_name = 'P - Pending';
             }else if($claim_status == 'A1'){
                 $claim_status_name = 'A1 - Approved & no dispute';
             }else if($claim_status == 'A2'){
                 $claim_status_name = 'A2 - Approved but area disputed';
             }else {
                 $claim_status_name = '';
             }
            ?>
            <div id="printablereport" >
                <?php if (!$moholla_name) { ?>
                    <h2>Village Claims Report <?= ($claim_status_name) ? '('.$claim_status_name.')' : '' ?></h2>
                    <h3>Forest Rights Committee of Village : <?= $village_name ?>, Taluka: <?= $block_name ?>, Dist:<?= $district_name ?> , State: <?= $state_name ?></h3>
                    <h4>List of Claims for forest lands together with area of each plot as determined by GPS Survey</h4>
                <?php } else { ?>
                    <h2>Mohalla Claims Report <?= ($claim_status_name) ? '('.$claim_status_name.')' : '' ?></h2>
                    <h3>Forest Rights Committee of Mohalla : <?= $moholla_name ?>,Village : <?= $village_name ?>, Taluka: <?= $block_name ?>, Dist:<?= $district_name ?> , State: <?= $state_name ?></h3>
                    <h4>List of Claims for forest lands together with area of each plot as determined by GPS Survey</h4>
                <?php } ?>
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th class="text-center"><h4>Summary</h4></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php if (!$moholla_name) { ?>
                                    <td>Total No of Claims in this village:</td>
                                <?php } else { ?>
                                    <td>Total No of Claims in this mahalla:</td>
                                <?php } ?>
                                <td><?= (isset($total_claims) ? $total_claims : '') ?></td>
                            </tr>
                            <tr>
                                <td>Total No. of Plots:</td>
                                <td><?= isset($total_plots) ? $total_plots : '' ?></td>
                            </tr>
                            <tr>
                                <td>Total Area of claimed Plots (Hectares):</td>
                                <td><?= isset($total_area_claimed) ? number_format($total_area_claimed, 2) : '' ?></td>
                            </tr>
                            <tr>
                                <td>Total No. of Claims by Claimants from same village:</td>
                                <td><?= isset($total_claims_from_same_village) ? $total_claims_from_same_village : '-' ?></td>
                            </tr>
                            <tr>
                                <td>Total No. of Claims by Claimants from other villages:</td>
                                <td><?= isset($total_claims_from_other_village) ? $total_claims_from_other_village : '-' ?></td>
                            </tr>
                        </tbody>
                    </table>

                </div>


                <div class="table-responsive print_table_display">
                    <!---->                <div class=""><!--claim-table-container-->
                        <table id="village_claim_report_tbl" class="responsive"> <!--claim-table-->
                            <thead>
                                <tr>
                                    <th style="width: 5%;border-right: 1px solid black;"><div>S.No</div></th>
                            <th style="width: 5%;border-right: 1px solid black;"><div>Claimant ID</div></th>
                            <th style="width: 40%;border-right: 1px solid black;"><div>Claimant/Spouse Name</div></th>
                            <!--<th>Residence Village</th>-->
                            <!--<th>Claim Village</th>-->
                            <th class="" style="width: 5%;border-right: 1px solid black;"><div>Claim No</div></th>
                            <th class="" style="width: 5%;border-right: 1px solid black;"><div>Claim Status</div></th>
                            <th class="" style="width: 5%;border-right: 1px solid black;"><div>Plot No</div></th>
                           <th style="width: 5%;border-right: 1px solid black;"><div><?= (!$moholla_name) ? 'Plot Village' : 'Plot Mohalla' ?></div></th>
<!--                            <th style="width: 5%;border-right: 1px solid black;"><div><?=  'Plot Village' ?></div></th>-->
                            <th class="" style="width: 5%;border-right: 1px solid black;"><div>SNO / Compt No.</div></th>
                            <th class="" style="width: 5%;border-right: 1px solid black;"><div>Area Claimed</div></th>
                            <th class="" style="width: 5%;border-right: 1px solid black;"><div>Area Gramsabha</div></th>
                            <th class="" style="width: 5%;border-right: 1px solid black;"><div>Area SDLC</div></th>
                            <th class="" style="width: 5%;border-right: 1px solid black;"><div>Area DLC</div></th>
                            <th class="" style="width: 5%;border-right: 1px solid black;"><div>Area (GPS) Hectare.</div></th>
                            <th class="" style="width: 5%;"><div>Area (SAT) Hectare.</div></th>
                            <!--<th><div>Total Area.</div></th>-->
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $index = 1;
// $mohallah_id_current_claimant = $claimants[0]['mohallah_id_master'];
                                foreach ($claimants as $key => $value) {
//                                    if($mohallah_id_current_claimant === $value['mohallah_id_master'])
//                                    {
//                                        
//                                    }
                                    $claimant_id = ($value['is_old'] == 1) ? $value['old_id'] : $value['id'];
                                    $claimant_new_id = $value['id'];
                                    //$residence_village_id = $value['claimant_resi_village_id'];
                                    $residence_village_id = $value['claimant_resi_master_village_id'];
                                    //$claim_village_id = $value['village_id'];
                                    $claim_village_id = $value['village_id_master'];
                                    $plots = isset($value['plots']) ? $value['plots'] : '';
                                    $claims = isset($value['claims']) ? $value['claims'] : '';
                                    $plot_arr = [];
                                    $i = 0;

                                    if ($claims) {
                                        foreach ($claims as $claim_key => $claim[0]['number']) {
                                            $plots = $claim[0]['number']['plots'];
                                            if ($plots) {
                                                foreach ($plots as $k => $v) {
                                                    array_push($plot_arr, $v);
                                                }
                                            }
                                        }
                                    }
                                    // t($plot_arr);
                                    if ($plot_arr) {
                                        ?>
                                        <tr><td> <!--claim-table-container-->
                                                <?php
                                                $areaclaimedh = $areaapprovedgsh = $areaapprovedsdlch = $areaapproveddlch = $total_gps_area = $total_sat_area = 0;
                                                $total_plots = count($plot_arr);
                                                foreach ($plot_arr as $key_plot => $plot) {
//                                                            print_r($plot['areaSat']);
                                                    // t($plot['areagpssurveyh']);
                                                    $claim_id = $plot['claim_id'];
                                                    $areaclaimedh += $plot['areaclaimedh'];
                                                    $areaapprovedgsh += $plot['areaapprovedgsh'];
                                                    $areaapprovedsdlch += $plot['areaapprovedsdlch'];
                                                    $areaapproveddlch += $plot['areaapproveddlch'];
                                                    $total_gps_area += $plot['areagpssurveyh'];
                                                    $total_sat_area += $plot['areaSat'];
                                                    $plot_vil_id = $plot['vil_id'];
                                                    ?>
                                            <tr>
                                                <td><?= ($key_plot == 0) ? $index : '' ?></td>
                                                <td><?= ($key_plot == 0) ? $claimant_id : '' ?></td>
                                                <td>
                                                    <?php if (($key_plot == 0)): ?>
                                                        <div class="spouce_div_noborder">
                                                            <?php
                                                            $claimant_name = App\Models\Report\ClaimantModel::where(['id' => $claimant_new_id])->value('name');
                                                            echo ($key_plot == 0) ? $claimant_name : ''
                                                            ?>
                                                        </div>
                                                        <div class="spouce_div">
                                                            <?php
                                                            $spouse_name = App\Models\ManageRecords\FamilyModel::where(['claimant_id' => $claimant_new_id])->value('name');
                                                            echo ($key_plot == 0) ? $spouse_name : ''
                                                            ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </td>
                <!--                                            <td>
                                                <?php
//                                                $village_name = App\Models\Report\VillageModel::where(['vil_id' => "$residence_village_id"])->value('name');
//                                                echo ($key_plot == 0) ? $village_name : ''
                                                ?>
                                                </td>-->
                <!--                                            <td>
                                                <?php
//                                                $village_name = App\Models\Report\VillageModel::where(['vil_id' => "$claim_village_id"])->value('name');
//                                                echo ($key_plot == 0) ? $village_name : ''
                                                ?>
                                                </td>-->
                                                <td> <?php
                                                    $claim = App\Models\Report\ClaimModel::where(['id' => $claim_id])->select('number', 'claim_status')->get()->toArray();
                                                    echo ($key_plot == 0) ? $claim[0]['number'] : '';
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo ($key_plot == 0) ? $claim[0]['claim_status'] : ''; ?>
                                                </td>
                                                <td ><?= $plot['plt_number'] ?></td>
                                                <td >
                                                    <?php
                                                    //$plot_village_id = trim($plot['vil_id']);
                                                    $plot_village_id = trim($plot['village_id_master']);
                                                     $village_name = App\Models\Report\VillageModel::where(['vil_id' => "$plot_village_id"])->value('village_name');
                                                    echo (!$moholla_name) ? $village_name : $moholla_name;
                                                    ?> 
                                                </td>
                                                <td ><?= $plot['compartment_no'] ?></td>
                                                <td ><?= ($plot['areaclaimedh']) ? number_format($plot['areaclaimedh'], 2) : '' ?></td>
                                                <td ><?= ($plot['areaapprovedgsh']) ? number_format($plot['areaapprovedgsh'], 2) : '' ?></td>
                                                <td ><?= ($plot['areaapprovedsdlch']) ? number_format($plot['areaapprovedsdlch'], 2) : '' ?></td>
                                                <td ><?= ($plot['areaapproveddlch']) ? number_format($plot['areaapproveddlch'], 2) : '' ?></td>
                                                <td ><?= number_format($plot['areagpssurveyh'], 2) ?></td>
                                                <td ><?= number_format($plot['areaSat'], 2) ?></td>
                <!--                                            <td><?php
//                                                if ($key_plot == ($total_plots - 1)) {
//                                                    echo ($total_gps_area > 0) ? number_format($total_gps_area,2) : '0.00';
//                                                }
                                                ?></td>-->
                                            </tr>
                                            <?php if ($key_plot == ($total_plots - 1)): ?>
                                                <tr class="bold_td">
                                                    <td colspan="2" class="align-center">Total Area:</td>
                                                    <td colspan="6">&nbsp;</td> 
                                                    <td><?php echo ($areaclaimedh != 0) ? $areaclaimedh : ''; ?></td>
                                                    <td><?php echo ($areaapprovedgsh != 0) ? $areaapprovedgsh : ''; ?></td>
                                                    <td><?php echo ($areaapprovedsdlch != 0) ? $areaapprovedsdlch : ''; ?></td>
                                                    <td><?php echo ($areaapproveddlch != 0) ? number_format($areaapproveddlch, 2) : ''; ?></td>
                                                    <td><?php echo number_format($total_gps_area, 2); ?></td>
                                                    <td><?php echo number_format($total_sat_area, 2); ?></td>
                                                </tr>
                                            <?php endif; ?>
                                            <?php
                                        }
                                        ?>
                                        </td>
                                        </tr> 
                                        <?php
                                    }
                                    $index++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php ?>
                <?php $links ?>
                <?php ?>
            </div>

        <?php } ?>
    </div>
</div>

@endsection

