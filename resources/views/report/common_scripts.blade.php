<script>
    selected_state = "{{ isset($selected_state) ? $selected_state : Request::get('state') }}";
    selected_dist = "{{ isset($selected_district) ? $selected_district :Request::get('district') }}";
    selected_block = "{{ isset($selected_block) ? $selected_block : Request::get('block') }}";
    selected_village = "{{ isset($selected_village) ? $selected_village :Request::get('village') }}";
    selected_claimant = "{{ isset($selected_claimants)? $selected_claimants : '' }}";
    selected_claim = "{{ isset($selected_claims)? $selected_claims : '' }}";
    selected_moholla = "{{ isset($selected_moholla) ? $selected_moholla :Request::get('moholla') }}";
    //alert(selected_claim);
    function populateDistrict(state_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#district').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/districts') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'state_id': state_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
                //alert(selected_dist);

                $.each(data, function (index, value) {
                    var selected = (selected_dist == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#district').append('<option ' + selected + 'value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_block) {
                    populateBlock($('#district').val());
                }

            },
        });
        $('.display_panel_content').css('cursor', '');
    }

    function populateBlock(dist_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#block').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/blocks') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'dist_id': dist_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)

                $.each(data, function (index, value) {
                    var selected = (selected_block == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#block').append('<option ' + selected + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_block) {
                    populateVillage($('#block').val());
                }
                $('.display_panel_content').css('cursor', '');
            },
        });
    }

    function populateVillage(block_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#village').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/villages') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'block_id': block_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)

                $.each(data, function (index, value) {
                    var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#village').append('<option ' + selected + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_village != '' && selected_moholla == '') {
                    populateMoholla($('#village').val());
                    populateClaimant($('#village').val());
                    populateClaims($('#village').val(), $('#claimant').val());
                } else {
                    populateMoholla($('#village').val());
                }


                $('.display_panel_content').css('cursor', '');
            },
        });
    }

    function populateMoholla(village_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#moholla').children(':not(:first-child)').remove();
        $.ajax({
            url: '<?= url('ajax/mohollas') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'village_id': village_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //console.log(data)
$('#moholla').append('<option  value="" >select</option>')
                $.each(data, function (index, value) {
                    // var selected = (selected_moholla == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#moholla').append('<option ' + '' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });

                if (selected_moholla) {
                    $('#moholla').val(selected_moholla.split(','));
                    // populateClaims(village_id, claimant_id)
                    populateClaimantByMoholla($('#moholla').val());
                    //alert($('#claimant').val());
                    
                }


                $('.display_panel_content').css('cursor', '');
            },
        });
    }

    function populateClaimant(village_id) {
        $('#claimant').children(':not(:first-child)').remove();
        $('#claim').children(':not(:first-child)').remove();
        $('.display_panel_content').css('cursor', 'wait');
        $.ajax({
            url: '<?= url('ajax/claimants-by-village') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'village_id': village_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                // console.log(data)
                $.each(data, function (index, value) {
                    //var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#claimant').append('<option ' + '' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_claimant) {
                    $('#claimant').val(selected_claimant.split(','));
                    // populateClaims(village_id, claimant_id)
                }

                $('.display_panel_content').css('cursor', '');
            },
        });
    }

    function populateClaimantByMoholla(moholla_id) {
        $('#claimant').children(':not(:first-child)').remove();
        $('#claim').children(':not(:first-child)').remove();
        $('.display_panel_content').css('cursor', 'wait');
        $.ajax({
            url: '<?= url('ajax/claimants-by-moholla') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'moholla_id': moholla_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                // console.log(data)
                $.each(data, function (index, value) {
                    //var selected = (selected_village == $(this)[0].Id) ? 'selected="selected"' : '';
                    $('#claimant').append('<option ' + '' + ' value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                });
                if (selected_claimant) {
                    $('#claimant').val(selected_claimant.split(','));
                     populateClaims($('#village').val(), $('#claimant').val());
                }

                $('.display_panel_content').css('cursor', '');
            },
        });
    }


    function populateClaims(village_id, claimant_id) {
        $('#claim').children(':not(:first-child)').remove();
        $('.display_panel_content').css('cursor', 'wait');
        var moholla_id = $('#moholla').val();
        if(!claimant_id){
            claimant_id = $('#claimant').val();
        }
        $.ajax({
            url: '<?= url('ajax/claims-by-village') ?>',
            type: 'POST',
            data: {
                '_token': '<?= csrf_token() ?>',
                'village_id': village_id,
                'claimant_id': claimant_id,
                'moholla_id': moholla_id
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            success: function (data) {
                //  console.log(data)
                $.each(data, function (index, value) {
                    $('#claim').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>');
                });
                var selected_claimants = $('#claimant').val();
                //alert(selected_claimants);
                if (selected_claim && selected_claimants) {
                    $('#claim').val(selected_claim.split(','));
                }
                $('.display_panel_content').css('cursor', '');
            },
        });
    }
    $(function () {



        $('select').select2(
                {
                    placeholder: 'Select'
                }
        );
        $('.close').on('click', function () {
            $(this).parents('.alert-success').hide();
            $(this).parents('.alert-danger').hide();
        });

        $('.search').on('click', function () {
            $(".display_panel_content").toggle('slow');
        });
        //------------- Form validation -------------//
        $('.search-form').validate({
            ignore: null,
            ignore: 'input[type="hidden"]',
                    errorPlacement: function (error, element) {
                        var place = element.closest('.input-group');
                        if (!place.get(0)) {
                            place = element;
                        }
                        if (place.get(0).type === 'checkbox') {
                            place = element.parent();
                        }
                        if (error.text() !== '') {
                            place.after(error);
                        }
                    },
            errorClass: 'help-block',
            rules: {
                select2: "required",
            },
            messages: {
                select2: "Please select something"
            },
            highlight: function (label) {
                $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            }
        });

        if (selected_state != 'all') {
            populateDistrict($('#state').val());
        }

    });
    $(window).on('load', function () {
//        $.ajax({
//            url: '<?= url('ajax/states') ?>',
//            type: 'POST',
//            data: {
//                '_token': '<?= csrf_token() ?>'
//            },
//            error: function () {
//                // $('#info').html('<p>An error has occurred</p>');
//            },
//            dataType: 'json',
//            success: function (data) {
//                //console.log(data)
//                $.each(data, function (index, value) {
//                    var selected = (selected_state == $(this)[0].Id) ? 'selected="selected"' : '';
//                    $('#state').append('<option ' + selected + '  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
//                });
//                if (selected_state) {
//                    populateDistrict($('#state').val());
//                }
//            },
//        });
    });
</script>

