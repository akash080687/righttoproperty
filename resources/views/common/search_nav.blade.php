
<div class="container">
    <div class="display_panel_content">
        {!! Form::open(['url' => url('family/list'),'class' => 'form-horizontal','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
        <div class="box-body">
            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputEmail3">State:</label>
                <div class="col-sm-5">                       
                    <select class="form-control" name="state" id="state" onchange="populateDistrict($(this).val());">
                        <option value="">Select State</option>                        
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputEmail3">District:</label>
                <div class="col-sm-5">
                    <select class="form-control" id="district" name="district" onchange="populateBlock($(this).val())">
                        <option value="">Select District</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="inputEmail3">Block:</label>
                <div class="col-sm-5">
                    <select class="form-control" id="block" name="block" onchange="populateVillage($(this).val())">
                        <option value="">Select Block/Tehsil</option>                      
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"  for="inputEmail3">Village:</label>
                <div class="col-sm-5">
                    <select class="form-control" id="village" name="village">
                        <option value="Select Village">Select Village</option>                    
                    </select>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="col-sm-5">
                    <button class="btn btn-info pull-right" type="submit" name="submit" value="yes">Search</button>
                </div>
            </div>
            <!-- /.box-footer -->
            {!! Form::close() !!}
        </div>
    </div>
</div>