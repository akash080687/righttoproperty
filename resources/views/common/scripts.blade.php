<script>
    $(function () {
        $(".login-form").validate({
//            ignore: null,
//            ignore: 'input[type="hidden"]',
            errorPlacement: function (error, element) {
                var place = element.closest('.input-group');
                if (!place.get(0)) {
                    place = element;
                }
                if (place.get(0).type === 'checkbox') {
                    place = element.parent();
                }
                if (error.text() !== '') {
                    place.after(error);
                }
            },
            errorClass: 'help-block',
            rules: {
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: '<?= url('check-valid-user') ?>',
                        type: "get",
                        complete: function (data) {
                            if (data.responseText !== 'false') {
                                $('#login-email-error').html('');
                            }
                        }
                    },
                },
                password: {
                    required: true,
                    //password: true,
                },
            },
            messages: {
                email: {
                    required: "This field is required",
                    email: "Please enter a valid email address",
                    remote: function () {
                        return $.validator.format("{0} does not exist", $("#login-email").val())
                    }
                },
                password: {
                    required: "This field is required",
                },
            },
            highlight: function (label) {
                $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            success: function (label) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function (form) {
                // form.submit();
                //alert('sdcvzc');
                $.ajax({
                    url: '<?= url('validate-user-login') ?>',
                    type: 'POST',
                    data: {
                        '_token': '<?= csrf_token() ?>',
                        'email': $('#login-email').val(),
                        'password': $('#inputPassword').val(),
                    },
                    error: function () {
                        // $('#info').html('<p>An error has occurred</p>');
                    },
                    // dataType: 'json',
                    success: function (data) {
                        // console.log($password)
                        if (data == 1) {
                            window.location.reload();
                        } else {
                            $('.login-form').find('.error-msg').html('Incorrect credentials given');
                        }

                    },
                });
                return false;
            }
        });
          toggleNavbarMethod();
        $(window).resize(toggleNavbarMethod);
    });
    function toggleNavbarMethod() {
        if ($(window).width() > 768) {
            $('.navbar .dropdown').on('mouseover', function () {
                $('.dropdown-toggle', this).trigger('click');
            }).on('mouseout', function () {
                $('.dropdown-toggle', this).trigger('click').blur();
            });
        }
        else {
            $('.navbar .dropdown').off('mouseover').off('mouseout');
        }
    }
</script>
