@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>                                              
                        <li class="active">Support Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <h1>Support Us</h1>
    <h2 class="myhead1">&quot;Change the face of poverty&quot;</h2>
    <h3 class="myhead3">with secure property rights</h3>
    <h4 class="myhead2">Help families climb out of extreme poverty!</h4>
    <br /><br />
    <p>Poverty is not merely absence  of money or assets, but most fundamentally, poverty is a consequence of the  inability of capitalise whatever asset people may have, because of lack of  formal title, and the high transaction cost.</p>
    <p>Recognition and protection of  property rights lie at the foundation of a free society. Property rights allows  the development of market economy. With the spread of ownership of assets,  people develop a stake in their community, and become active citizens. At the  early stages of socio-economic development, land is the principal form of  property. </p>
    <p>For over a century, millions  of Indians, largely indigenous tribes and others living in forest areas, have  not had their land rights recognised by the state. A new law in 2006,  acknowledges this historical injustice, and seeks to recognise the land rights  of these remote and poor communities. </p>
    <!-- <p>ARCH Vahini, a grassroots NGO  based in Gujarat, a think tank based in New Delhi, are  working together to help these remote communities document and map their land  claims. Please visit www.RighttoProperty.org </p>-->
    <p>A short outline of this  initiative may be found here. (<a href="http://righttoproperty.org/images/sample/PR-brochure-v2-2013.pdf">PR-brochure-v2-2013</a>)</p>
    <p>A flier with a request for  support can be found here. <br />
        For contributions in India  rupees. (<a href="http://righttoproperty.org/images/sample/Right%20to%20land%20rupee%20version-v2.pdf">Right to land  rupee version-v2</a>)<br />
        For contributions from  outside of India. (<a href="http://righttoproperty.org/images/sample/Right%20to%20land-v2.pdf">Right  to land-v2</a>)</p>
    <p><span class="style1">Donations* by cheque may be mailed to:</span><br />
        <br />
        <strong>ARCH  Vahini,</strong><span class="style1"> E-702, Samrajya Complex, Fatehgunj,  Vadodara 390002, Gujarat. India<br />
            or  transferred directly to:<br />
            Bank  Account no. 01930100006389, Bank of Baroda, M S University Campus Branch,  Vadodara</span><br />
        <strong>IFSC  Code: BARB0MSUNIV</strong> (for domestic Indian  transfer)</p>
    <p class="style1">Contributions from well wishers abroad can be made by cheque  payable to: &quot;Friends of ARCH&quot;<br />
        Kaushika  Patel, 223 Nassau Blvd., Garden City Park, NY 11040. USA.<br />
        Tel  No. +1-(516) 741-4155 ; Fax No. +1-(516) 873-8930<br />
        Friends  of ARCH, Inc. is a non-profit, tax-exempt organisation. (Tax ID: 13-3903010)</p>
   <!-- <p>Contributions* may be made to:&nbsp; <br />
        <strong>Liberty Institute</strong> account no: 905520100 57517<br />
        Syndicate Bank, South Block Branch, New Delhi 110 011. India<br />
        Swift Code: SYN BIN BB 171  (for international transfer)<br />
        IFSC CODE: SYNB0009055 (for domestic Indian transfer)</p>
    <p>Donations by cheque may be  sent to <br />
        Liberty Institute,<strong> </strong>4/8  Sahyadri, Plot 5, Sector 12, Dwarka, New Delhi  - 110078. India &nbsp;</p>-->
    <p><span class="style1"><em>* Donations to ARCH Vahini <!--and Liberty Institute -->qualify for tax  deduction under Section 80G of the Indian Income Tax Act.</em></span></p>
    <hr align="center" size="2" /><hr align="center" size="2" />
    <h1 align="center">Land rights initiative - A brief outline:<strong>
        </strong>
    </h1>
    <p>ARCH Vahini, a grassroots NGO, has pioneered an initiative  to help the people map their land in their own villages in forest areas of  Gujarat. The villagers are using hand-held GPS devices, as well as using  satellite images, where available, to document their land claims, and getting  it endorsed at the village meetings (gram sabhas).</p>
    <p>From the initial 25-30 villages covered under this  initiative in 2010-11, there has been a sharp increase in demand from villagers  in five other districts of Gujarat. Currently, about 100 villages are being  covered under this initiative.</p>
    <p>To meet the growing demand, with the help of some friends,  additional hand-held GPS devices were purchased. In some instances, satellite  images from the National Remote Sensing Centre, in Hyderabad, were also  procured, to augment the imagery available through Google, and other publicly  accessible sources. Requests are coming in from other states too, where some of  the grassroots NGOs are expressing their interest to learn about this  initiative, and undertake similar work in their own areas.</p>
    <p>Apart from the practical aspects of mapping and  documenting land, this is a truly people's initiative, where the local people  are directly participating in collecting the information, learning to use the  GPS, in a transparent and accoutable manner.</p>
    <p>We would greatly welcome any help or suggestion you may be  able to offer</p>
    <h3>Types of support</h3>
    <ul>
        <li>Grassroots network: We are keen to collaborate  with other grassroots organisations working in tribal villages, who may want to  ensure proper implementation of the FRA in their areas. </li>
        <li>Communication and outreach: This calls for  public advocacy and awareness to make the wider society aware of the  significance of the land titles for tribal villagers. </li>
        <li>Technical support: the capacity to integrate the  data being generated on the ground, with the web based database and  map/satellite images, and producing reports. </li>
        <li>Legal and administrative support: The initiative  requires management of large databases, as well as follow up with various  designated authorities at sub-district, district and state level. Legal  knowledge, particularly about the FRA and other forest related issues are also  very much required, to process and follow up on the claims filed. </li>
        <li>Financial  support: For a project of this nature any monetary support as well as in terms  of skill and technology are most welcome. </li>
    </ul>
    <h3>Background:</h3>
    <p>For over 150 years, the land claims of tribal populations  living in and forest areas were not recognised. Even in Independent India,  tribal villagers were mostly seen as encroachers on state land or forest. Now  the FRA has opened the possibility of land rights of the tribal villages being  recognised. Most of these villages were not mapped because they were not  revenue villages.</p>
    <p>Some state governments are attempting to digitise land  records, but there has been little effort to physically survey and map the  land, and identify the claimants. The government's capacity to undertake such a  massive survey exercise on the ground in remote areas is quite limited. Yet,  the need for mapping and documenting the land in these villages has never been  greater.</p>
    <p>Over the years, land has emerged as the single biggest  source for corruption and conflict. Land records are in very poor state. It is  estimated that 80% of court cases in the lower judiciary is on account of land  and property. The indigenous tribes, the poorest and most marginalized groups,  are among the worst sufferers. Without any legal title over their land, they  are largely seen as encroachers on their own land, even though they may have  lived on that land for generations and even centuries.</p>
    <p>With over 100 million population, it is believed that the  tribal villagers may possess and operate about 40-50 million plots of land  across India. This is a challenge, but also an opportunity to empower the most  marginalised people in the country.</p>
</div>
@endsection

