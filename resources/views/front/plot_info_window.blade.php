<div class="">
    <h2>Region Details</h2>
	
	
    <?php
    $claim_id = isset($plot_details['claim_id']) ? $plot_details['claim_id'] : '';
    $is_old = isset($plot_details['is_old']) ? $plot_details['is_old'] : '';
    $claimant_id = \App\Models\Report\ClaimModel::where(['id' => $claim_id])->value('claimant_id');
    $claimant_old = \App\Models\Report\ClaimantModel::where(['id' => $claimant_id])->value('old_id');
    $claimant_resi_village_id = \App\Models\Report\ClaimantModel::where(['id' => $claimant_id])->value('claimant_resi_village_id');
    ?>
    <table class="table">
        <tbody>
            <tr class="success">
                <td><strong>State:</strong> <?= \App\Models\Report\StateModel::where(['stt_state_id' => $posted_data['state_id']])->value('stt_state_name') ?></td>
                <td><strong>District:</strong> <?= \App\Models\Report\DistrictModel::where(['ogc_fid' => $posted_data['district_id']])->value('district_name') ?></td>
            </tr>
            <tr class="info">
                <td><strong>Tehsil:</strong> <?= \App\Models\Report\BlockModel::where(['ogc_fid' => $posted_data['block_id']])->value('block_name') ?></td>
                <td><strong>Resident Village:</strong> <?= ($claimant_resi_village_id) ?  \App\Models\Report\VillageModel::where(['vil_id' => $claimant_resi_village_id])->value('block_name') : '' ?></td>
            </tr>
            <tr class="success">
                <td><strong>Plot No:</strong> <?= isset($plot_details['plt_number']) ? $plot_details['plt_number'] : '' ?></td>
                <td><strong>Plot Village:</strong> <?= \App\Models\Report\VillageModel::where(['vil_id' => trim($plot_details['village_id_master'])])->value('village_name') ?></td>

            </tr>
            <tr class="info">
                <td><strong>Claim No: 
                        <?php
                        echo $claim_number = ($claim_id) ? \App\Models\Report\ClaimModel::where(['id' => $claim_id])->value('number') : '';
                        ?>
                    </strong></td>
                <td><strong>Claim Village:</strong> <?= \App\Models\Report\VillageModel::where(['vil_id' => $posted_data['village_id']])->value('village_name') ?></td>
            </tr>
            <tr class="success">
                <td><strong>Claimant ID:</strong>
                    <?php
                    echo (!$is_old) ? $claimant_id : $claimant_old;
                    ?>
                </td>
                <td><strong>GPS Area in Hectares:</strong> <?= isset($plot_details['areagpssurveyh']) ? number_format($plot_details['areagpssurveyh'], 2) : ''; ?></td>
            </tr>
            <tr class="info">
                <td><strong>Claimant Name:</strong> <?php
                    echo $claimant_name = \App\Models\Report\ClaimantModel::where(['id' => $claimant_id])->value('name');
                    ?></td>
                <td><strong>Spouse Name:</strong> <?php
                    echo $spouse_name = \App\Models\ManageRecords\FamilyModel::where(['claimant_id' => $claimant_id])->value('name');
                    ?></td>
            </tr>

        </tbody>
    </table>
</div>