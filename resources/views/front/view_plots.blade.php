@extends('layouts.common')
@section('content')
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.11&sensor=false" type="text/javascript"></script>-->
<div class="breadcrumb_container">
    <div class="container">
	
        <div class="row">
            <div class="col-sm-12">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>                                              
                        <li class="active">View Plot</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="display_panel_content">
        <h2 class="print_hide">Search</h2>
        {!! Form::open(['url' => 'view-plots','class' => 'form-horizontal search-form','method' => 'GET', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
        <div class="box-body print_hide">
            <div class="row">
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">State:</label>
                            <div class="col-sm-9">                       
                                {{Form::select('state', isset($states) ?$states : '' , isset($logged_in_user_boudary_id) && $logged_in_user_boudary_id != 'all' ? $logged_in_user_boudary_id : $selected_state,array('class'=>'form-control select2-minimum required','id' => 'state','onchange' => 'populateDistrict($(this).val())'))}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">District:</label>
                            <div class="col-sm-9">
                                <select class="form-control required" id="district" name="district" onchange="populateBlock($(this).val())">
                                    <option value="">Select District</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="inputEmail3">Block:</label>
                            <div class="col-sm-9">
                                <select class="form-control required" id="block" name="block" onchange="populateVillage($(this).val())">
                                    <option value="">Select Block/Tehsil</option>                      
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"  for="inputEmail3">Village:</label>
                            <div class="col-sm-9">
                                <select class="form-control required" id="village" name="village" onchange="populateMoholla($(this).val())">
                                    <option value="">Select Village</option>                    
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"  for="inputEmail3">Mohalla:</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="moholla" style="width: 200px;" name="moholla"  onchange="">
                                    <option value="">Select Mohalla</option>                    
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <!--                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"  for="inputEmail3">Claimant ID:</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="claimant" name="claimant[]"  onchange="populateClaims($('#village').val(), $(this).val())" multiple="">
                                                    <option value="">Select Claimant</option>                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->

                <!--                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"  for="inputEmail3">Claim No:</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="claim" name="claim[]" multiple="">
                                                    <option value="">Select Claim</option>                    
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>-->

            </div> 
            <div class="box-footer">
                <div class="row">
                    <div style="width:30%; margin:0 auto">
                        <div class="col-sm-6">
                            <button class="btn btn-info pull-right search_plot" type="submit" name="search" value="1">Search</button>
                            &nbsp; <i class="fa fa-spinner fa-spin" id="loading_search_plot" style="font-size:24px;visibility: hidden"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <br>
    <div id="map" style="width: 100%; height:650px;"></div>
</div>

<script type="text/javascript">
    // check DOM Ready
    $(document).ready(function () {
        $('.search-form').on('submit', function (e) {
            e.preventDefault();
            var state_id = $('#state').val();
            var district_id = $('#district').val();
            var block_id = $('#block').val();
            var village_id = $('#village').val();
            var claimants = $('#claimant').val();
            var claims = $('#claim').val();
            var moholla_id = $('#moholla').val();
            if (village_id) {
                $('#loading_search_plot').css('visibility', 'visible');
                $.ajax({
                    url: '<?= url('draw-plot-map') ?>',
                    type: 'POST',
                    data: {
                        '_token': '<?= csrf_token() ?>',
                        'state_id': state_id,
                        'district_id': district_id,
                        'block_id': block_id,
                        'village_id': village_id,
                        'claimant': claimants,
                        'claim': claims,
                        'moholla_id': moholla_id
                    },
                    async: false,
                    dataType: 'json',
                    error: function () {
                        // $('#info').html('<p>An error has occurred</p>');
                    },
                    success: function (data) {
                        var coords = [];
                        // var plot_ids = [];
                        var k = 0;
                        coords['plot_id'] = [];
                        coords['coords'] = [];
                        $.each(data, function (key, item)
                        {
                            //  console.log(item.ogc_fid);
                            var plot_id = item.ogc_fid;
                            var lat_lon_str = item.lat_lon_str;
                            var lat_lon_arr = (lat_lon_str) ? lat_lon_str.split(",") : '';
                            var polyCoords = [];


                            if (lat_lon_arr) {
                                $.each(lat_lon_arr, function (i, v) {
                                    var lat_lon = (v) ? v.split(" ") : '';
                                    polyCoords[i] = {lat: parseFloat(lat_lon[1]), lng: parseFloat(lat_lon[0])};

                                });
                                coords.plot_id.push(plot_id);
                                coords.coords.push(polyCoords);
                            }
                        });
                        initMap(coords)
                        console.log(coords);
                        //console.log(plot_ids);
                        $('#loading_search_plot').css('visibility', 'hidden');

                    },
                });
            }
        });



    });

</script>
<script>

    // This example creates a simple polygon representing the Bermuda Triangle.
    // When the user clicks on the polygon an info window opens, showing
    // information about the polygon's coordinates.

    var map;
    var infoWindow;
    var infowindows = [];
    function initMap(coords, plot_ids) {

        infoWindow = new google.maps.InfoWindow;
        var state_id = $('#state').val();
        var district_id = $('#district').val();
        var block_id = $('#block').val();
        var village_id = $('#village').val();
        var moholla_id = $('#moholla').val();

        if (coords) {
            var polyCoords = coords['coords'];
            var plot_ids = coords['plot_id'];
            console.log(plot_ids);
            if (typeof polyCoords[0][0]['lat'] != "undefined" && typeof polyCoords[0][1]['lng'] != "undefined") {

                var center_lat = polyCoords[0][0]['lat'];
                var center_long = polyCoords[0][1]['lng'];
                map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 14,
                    //  center: {lat: 24.886, lng: -70.268},
                    center: {lat: center_lat, lng: center_long},
                    // mapTypeId: 'terrain'
                    mapTypeId: google.maps.MapTypeId.HYBRID
                });

                var polygons = [];
                for (var i = 0; i < polyCoords.length; i++) {

                    polygons.push(new google.maps.Polygon({
                        paths: polyCoords[i],
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.10,
                        name: plot_ids[i]
                    }));
                    polygons[polygons.length - 1].setMap(map);
                    polygons[i].addListener('click', function (event) {
                        //alert(this.name)
                        //infoWindow = new google.maps.InfoWindow;

                        //infoWindow.setContent('');
                        var plot_id = this.name;
                        $.ajax({
                            url: '<?= url('plot-info-window') ?>',
                            type: 'POST',
                            data: {
                                '_token': '<?= csrf_token() ?>',
                                'plot_id': plot_id,
                                'state_id': state_id,
                                'district_id': district_id,
                                'block_id': block_id,
                                'village_id': village_id,
                                'moholla_id': moholla_id
                            },
                            async: false,
                            dataType: 'html',
                            error: function () {
                                // $('#info').html('<p>An error has occurred</p>');
                            },
                            success: function (data) {
                                // Replace the info window's content and position.


                                infoWindow.setContent(data);
                                infoWindow.setPosition(event.latLng);
                                //infowindows.push(infowindow);
                                // infowindow.close();

                                infoWindow.open(map);
                            },
                        });
                    });
                }


            }

        } else {
            // alert('sadsa');
            //document.getElementById("map").innerHTML = "Loading";
            var myLatLng = new google.maps.LatLng(21.66694, 73.65355);
            var myOptions = {
                zoom: 10,
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.HYBRID
            };
            map = new google.maps.Map(document.getElementById("map"), myOptions);
        }
    }

    function closeInfoWindows() {
        for (var i = 0; i < infowindows.length; i++) {
            infowindows[i].close();
        }
    }

    /** @this {google.maps.Polygon} */
    function showArrays(event) {
        // Since this polygon has only one path, we can call getPath() to return the
        // MVCArray of LatLngs.
        var vertices = this.getPath();

        var contentString = '<b>Bermuda Triangle polygon</b><br>' +
                'Clicked location: <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
                '<br>';

        // Iterate over the vertices.
        for (var i = 0; i < vertices.getLength(); i++) {
            var xy = vertices.getAt(i);
            contentString += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' +
                    xy.lng();
        }

        // Replace the info window's content and position.
        infoWindow.setContent(contentString);
        infoWindow.setPosition(event.latLng);

        infoWindow.open(map);
        // callback=initMap
    }
</script>

<script async defer
        src="https://maps.googleapis.com/maps/api/js?key= AIzaSyDexZcTrnbaYIRewY_nnEHelc-I-4kU044 &callback=initMap">
</script>

@endsection

