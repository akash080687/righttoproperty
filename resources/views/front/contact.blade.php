@extends('layouts.common')
@section('content')
<div class="container">
    <h1>Contact Us</h1>
    <p>
        If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
    </p>
    <p>
    <h4>You can also contact us directly with:</h4>
 <!--   <p>Barun Mitra, <strong>Liberty Institute</strong>,  New Delhi.<br />
        Email: barun@libertyinstitute.org.in</p>

    <p>www.InDefenceofLiberty.org<br /> 
        www.ProtectingPropertyRights.org</p>-->

    <p>Ambrish Mehta, <strong>ARCH Vahini</strong>, Baroda, Gujarat.<br /> 
        Email: m_ambrish@hotmail.com</p>
    <br>
    {!! Form::open(['url' => url('claim/submit-claim-data'),'class' => 'form-horizontal add-edit-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}
    <div class="box-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Name<span>*</span>:</label>
                        <div class="col-sm-9">                       
                            {{ Form::text('contact[name]', '', array('class'=>'form-control col-xs-5','placeholder' => 'Name')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Email<span>*</span>:</label>
                        <div class="col-sm-9">
                            {{ Form::email('contact[email]', '', array('class'=>'form-control col-xs-5','placeholder' => 'Email')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Subject<span>*</span>:</label>
                        <div class="col-sm-9">
                            {{ Form::text('contact[subject]', '', array('class'=>'form-control col-xs-5','placeholder' => 'Subject')) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Message<span>*</span>:</label>
                        <div class="col-sm-9">                       
                            {{ Form::textarea('contact[message]', '', array('class'=>'form-control col-xs-5','placeholder' => 'Message','size' => '30x3')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Address:</label>
                        <div class="col-sm-9">
                            {{ Form::textarea('contact[address]', '', array('class'=>'form-control col-xs-5','placeholder' => 'Address','size' => '30x3')) }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Contact Number:</label>
                        <div class="col-sm-9">
                            {{ Form::text('contact[contact_no]', '', array('class'=>'form-control col-xs-5','placeholder' => 'Contact Number')) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--        <div class="row">
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputEmail3">Captcha<span>*</span>:</label>
                        <div class="col-sm-9">                       
                            {--!! captcha_img() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="row">
                    <div class="form-group">                    
                        <div class="col-sm-12">                       
                            {{ Form::text('captcha', '', array('class'=>'form-control col-xs-5','placeholder' => 'Please enter the letters as they are shown in the image. ')) }}
                        </div>
                    </div>
                </div>
            </div>

        </div>-->
        <div class="box-footer">
            <div class="col-sm-5">
                <button class="btn btn-info pull-right" type="button">Submit</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

