@extends('layouts.common')
@section('content')
<div class="breadcrumb_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>                                              
                        <li class="active">About Us</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <h2>About Us</h2>
    <p><strong>Action Research in Community Health and Development</strong> (ARCH Vahini) is an NGO active in some of the rural and forest areas of Gujarat. Over the past thirty years, it has provided health care services to the poor villagers, particularly the tribal communities. It has been at the forefront of the effort to resettle and rehabilitate the tribal villagers who were displaced because of the Narmada dam. It has also been very active in securing rights of the tribal villagers to various forest resources.</p>

   <!-- <p><strong>Liberty Institute</strong> is an indepedent public policy research and advocacy organisation based in Delhi. One of the key areas of institute has been land and property rights. The institue seeks to improve understanding of the constitutional and legal dimension of property rights. The institute believes that under a proper framework, property rights, including land rights, greatly empowers the people, particularly the poor and the marginalised sections of society, both politically as citizens, and economically as producers and consumers.</p>

    <p><strong>Liberty Institute</strong><br />
        4/8 Sahyadri, Plot 5, Sector 12, Dwarka,<br />
        New Delhi - 110078. India<br />
        Email: info@libertyinstitute.org.in<br />
        Tel: +91-11-28031309, +91-11-45665193<br />

        www.InDefenceofLiberty.org<br />
        www.EmpoweringIndia.org<br />
        www.ProtectingPropertyRights.org
    </p>
	-->
    <p><strong>ARCH Vahini,</strong><br />
        E-702, Samrajya Complex, <br />
        Fatehgunj, Vadodara 390002, <br />
        Gujarat. India.<br />
        Email: m_ambrish@hotmail.com<br />
    </p>
</div>
@endsection

