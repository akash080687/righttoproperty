@extends('layouts.common')
@section('content')
<div class="container minbody_height">
    <h2>Land rights of tribal villagers</h2>
    <p>A people's initiative to facilitate mapping of land in tribal villages in forest areas under the Forest Rights Act (FRA) 2006.</p>

    <p>This is an initiative to document and submit claims regarding private land holding, as well as village common land, to the concerned authorities under the Forest Rights Act 2006. This law, for the first time, allows tribal populations in forest villages to claim land title.</p>

    <p>The use of GPS devices and satellite images provides an opportunity to the people to document the claims, democratise the process thro' wider participation and transparency, and help to implement the FRA law, both in letter and spirit. The aim here is to map the agriculture and common land of villagers, by collecting the lat/long coordinates for the plots, information about their ownership claims and land usage.</p>

    <p>Most importantly, the aim is to attract others in similar situation to join hands, and make this a truly people's initiative to know, document and claim their own assets, and be empowered as free citizens of India. This also makes it critical to join hands with motivated people at the grassroots, attract the entrepreneurial talent, and skilled people to help us realise the true potential of this initiative.</p>

    <p>Such a platform will be very useful in demonstrating the viability of such an initiative, demystify the technology and make it a practical tool to solve one of the most critical challenges facing the country.</p>
</div>
@endsection

