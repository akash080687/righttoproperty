@include('layouts.header')
</head>
<body onload="">
    <div class="top_bar_container print_hide">
        <div class="container"> 
            <div class="row"> 
                <div class="clearfix headerCvr">
                <div class="logoPart">
                    <a href="http://righttoproperty.org/">
                        <img src="{{ URL::asset('includes/images/logo.png') }}" alt="" />
                        <span>Right to property</span>
                    </a>
                </div>
                <div class="hdrRight"> 
                    <p>Mapping land, documenting, evidence, and claiming title under the Forest Rights Act
                        <span>An initiative of ARCH Vahini, Gujarat<!-- and Liberty Institute, New Delhi.--> </span> </p>
                </div>

            </div>
            </div>
        </div>
    </div>
    <div>@include('layouts.nav')</div>
    @yield('content')
    @include('layouts.footer')
    <!--added for google -->