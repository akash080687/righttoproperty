<!doctype html>
<html lang="en-gb" class="no-js">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 		
        <!--[if lt IE 9]> 
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <![endif]-->
        <title><?= isset($appData['metaTitle']) && $appData['metaTitle'] ? 'Right To Property::' . $appData['metaTitle'] : 'Right To Property' ?></title>
        <meta name="description" content="">
        <meta name="author" content="WebThemez">

        <!-- fav icon -->
        <link rel="shortcut icon" type="image/x-icon" href="">
        <!-- fav icon -->
        <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

        <link rel="stylesheet" href="{{ URL::asset('includes/css/bootstrap.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('includes/css/font-awesome.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('includes/css/custom/custom.css') }}" />

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

        @if(isset($appData['cssArr']))
        @foreach($appData['cssArr'] as  $css)
        <link rel="stylesheet" href="{{ URL::asset($css) }}" />
        @endforeach
        @endif

        <script src="http://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript"></script>
        <script src="{{ URL::asset('includes/js/jQuery.print.js') }}"></script>
        <!--<script src="{{ URL::asset('includes/js/jQuery.print.js') }}"></script>-->
        <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

