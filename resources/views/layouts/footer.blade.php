

<script src="{{ URL::asset('includes/js/bootstrap.js') }}" type="text/javascript"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
@if(isset($appData['jsArr']))
@foreach($appData['jsArr'] as  $js)
<script src="{{ URL::asset($js) }}" type="text/javascript"></script>
@endforeach
@endif
<?php
if (isset($include_script_view) && $include_script_view) {
    foreach ($include_script_view as $key => $value) {
        ?>
        @include($value)
    <?php
    }
}
?>
<?php if (isset($common_script_view) && $common_script_view) { ?>
    @include($common_script_view)
<?php } ?>
<?php if (isset($additional_script_view) && $additional_script_view) { ?>
    @include($additional_script_view)
<?php } ?>

<!-- footer starts  -->

<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-6">
                <div class="para">Copyright &copy; <?php echo date('Y'); ?> Right To Property, All Rights Reserved.</div>
            </div>   
            <div class="col-sm-6 col-lg-6">
                <div class="socialPart clearfix">
                    <a class="fb" href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
                    <a class="tw" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>

                </div>
            </div>
        </div> 
    </div>
</footer>
</body>
</html>