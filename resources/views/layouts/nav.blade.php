<nav class="navbar navbar-default menubar">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?= (isset($page) && $page == 'home') ? 'active' : '' ?>"><a href="<?= url('/') ?>" >Home</a></li>
                    <li class="<?= (isset($page) && $page == 'about_us') ? 'active' : '' ?>"><a href="<?= url('about-us') ?>" >About Us</a></li>
                    <li class="<?= (isset($page) && $page == 'support_us') ? 'active' : '' ?>"><a href="<?= url('support-us') ?>" >Support Us</a></li>
                    <li class="<?= (isset($page) && $page == 'contact') ? 'active' : '' ?>"><a href="<?= url('contact') ?>" >Contact</a></li>
                    <li class="<?= (isset($page) && $page == 'view_plots') ? 'active' : '' ?>"><a href="<?= url('view-plots') ?>" >View Plot</a></li>

    <!--                <li class=""><a href="#/viewPlot">View Plot</a></li>-->
                    <?php if (isset($user_id) && $user_id) { ?>
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Reports <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class=""><a href="<?= url('report/villge-claims-plot') ?>"> Village Claims Report</a></li>
                                <li class=""><a href="<?= url('http://rtp.local/report-village-plot') ?>"> Village Plots</a></li>
                              <!--<li class=""><a  href="http://59.90.163.72/RTP/report-village-plot">Village Plots</a></li>-->
                                <!-- <li class=""><a  href="http://righttoproperty.org:8090/RTP/report-village-plot">Village Plots</a></li> -->
                               <!-- <li class=""><a  href="http://righttoproperty.org:8090/righttoproperty/RTP/report-village-plot">Village Plots</a></li>-->

                            </ul>

                        </li> 
                    <?php } else { ?>
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Sample Reports <span class="caret"></span></a>
                            <!--                        <ul class="dropdown-menu">
                                                        <li class=""><a href="<?= url('report/villge-claims-plot') ?>"> Village Claims Report</a></li>
                                                        <li class=""><a href="<?= url('report/villge-plots') ?>">Village Plots</a></li>

                                                    </ul>-->

                        </li> 
                    <?php } if (isset($user_id) && $user_id) { ?>
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle <?= (isset($page) && $page == 'manage_records') ? 'active' : '' ?>" data-toggle="dropdown">Manage Records <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?= (isset($section) && $section == 'family') ? 'active' : '' ?>"><a href="<?= url('family/list') ?>">Family</a></li>
                                <li class="<?= (isset($section) && $section == 'village') ? 'active' : '' ?>"><a href="<?= url('village/list') ?>">Village</a></li>
                                <li class="<?= (isset($section) && $section == 'moholla') ? 'active' : '' ?>"><a href="<?= url('moholla/list') ?>">Mohalla</a></li>
                                <li class="<?= (isset($section) && $section == 'claimant') ? 'active' : '' ?>"><a href="<?= url('claimant/list') ?>">Claimant</a></li>
                                <li class="<?= (isset($section) && $section == 'claim') ? 'active' : '' ?>"><a href="<?= url('claim/list') ?>">Claim</a></li>
                                <li class="<?= (isset($section) && $section == 'plot') ? 'active' : '' ?>"><a href="<?= url('plot/list') ?>">Plot</a></li>
                            </ul>

                        </li>

                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Import/Export<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?= (isset($section) && $section == 'import-excel') ? 'active' : '' ?>"><a href="<?= url('import/survey-data') ?>">Import Survey Data</a></li>                            
                            </ul>
                        </li>
                    <?php } ?>
                    <li><a href="javascript:void(0);">Photo Gallery</a></li>
                    <li><a target="_blank" href="http://www.righttoproperty.org/blog">Blog</a></li>
                    <li><a target="_blank" href="http://www.righttoproperty.org/map/">News Map</a></li>
                </ul>
                <!--            <a class="btn btn-primary" data-toggle="modal" href="#myModal" >Login</a>-->
                <ul class="nav navbar-nav navbar-right">
                    <?php if (isset($user_id) && $user_id) { ?>
                        <li class="right_bar"><a href="<?= url('logout') ?>">Logout</a></li>    
                    <?php } else { ?>
                        <li class="right_bar"><a href="#" data-toggle="modal" data-target="#login-modal">Login</a></li>               
                        <li><a href="<?= url('registration') ?>">Register</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</nav>

<style>
    .loginmodal-container {
        background-color: #f7f7f7;
        border-radius: 2px;
        box-shadow: 0 2px 2px rgba(0, 0, 0, 0.3);
        font-family: roboto;
        margin: 0 auto;
        max-width: 350px;
        overflow: hidden;
        padding: 30px;
        width: 100% !important;
    }
</style>

<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <h3>Login to Your Account</h3><br>
            {!! Form::open(['url' => '','class' => 'form-horizontal login-form','method' => 'POST', 'enctype' => 'multipart/form-data','role' => 'form']) !!}

            <div class="form-group">             
                <div class="col-sm-12">
                    {{ Form::email('email', '', array('class'=>'form-control required','id' => 'login-email','placeholder' => 'Email')) }}
                </div>
            </div>
            <div class="form-group">            
                <div class="col-sm-12">
                    {{ Form::text('password', '', array( 'class'=>'form-control required','id' => 'inputPassword','placeholder' => 'Password')) }}
                    <!-- <input type="password" name="password" class="form-control required" id="inputPassword" placeholder="Password"> -->
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12 error-msg">

                </div>

            </div>
            <div class="form-group">
                <div class="col-sm-12 sucess-msg">

                </div>

            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-default">Sign in</button>
                </div>

            </div>
            {!! Form::close() !!}


        </div>
    </div>
</div>
