<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */


Route::get('/registration', ['as' => 'home1', 'uses' => 'UserRegistrationController@registration']);
Route::post('/registration', ['as' => 'home1', 'uses' => 'UserRegistrationController@registration']);
Route::get('/', ['as' => 'home', 'uses' => 'FrontController@home']);
Route::get('about-us', ['as' => 'about-us', 'uses' => 'FrontController@aboutUs']);
Route::get('support-us', ['as' => 'support-us', 'uses' => 'FrontController@supportUs']);
Route::get('contact', ['as' => 'contact', 'uses' => 'FrontController@contact']);
Route::get('view-plots', ['as' => 'view-plots', 'uses' => 'ViewPlotController@viewPlots']);
Route::post('draw-plot-map', ['as' => 'draw-plot-map', 'uses' => 'ViewPlotController@drawPlotView']);
Route::post('plot-info-window', ['as' => 'plot-info-window', 'uses' => 'ViewPlotController@plotInfoWindow']);
Route::any('plot-id-for-report', ['as' => 'plot-id-for-report', 'uses' => 'Report\AjaxController@getPlotOgcFidForVillagePlotReport']);
Route::get('404', function () {
    return view('404');
});
//Route::get('family/update/{id}', ['as' => 'edit_family', 'uses' => 'ManageRecords\RecordController@family']);
////////////family///////////////////////////////////
Route::group(['prefix' => 'family', 'namespace' => 'ManageRecords'], function() {
    Route::get('update/{id}', ['as' => 'edit_family', 'uses' => 'FamilyController@edit']);
    Route::get('add', ['as' => 'add_family', 'uses' => 'FamilyController@add']);
    Route::any('list', ['as' => 'list_family', 'uses' => 'FamilyController@familyList']);
    Route::post('submit-family-data', ['as' => 'submit-family-data', 'uses' => 'FamilyController@processData']);
});


////////////claimant///////////////////////////////////
Route::group(['prefix' => 'claimant', 'namespace' => 'ManageRecords'], function() {
    Route::get('update/{id}', ['as' => 'edit_claimant', 'uses' => 'ClaimantController@edit']);
    Route::get('add', ['as' => 'add_claimant', 'uses' => 'ClaimantController@add']);
    Route::any('list', ['as' => 'list_claimant', 'uses' => 'ClaimantController@claimantList']);
    Route::post('submit-claimant-data', ['as' => 'submit-claimant-data', 'uses' => 'ClaimantController@processData']);
});

////////////village///////////////////////////////////
Route::group(['prefix' => 'village', 'namespace' => 'ManageRecords'], function() {
    Route::get('update/{id}', ['as' => 'edit_village', 'uses' => 'VillageController@edit']);
    Route::get('add', ['as' => 'add_village', 'uses' => 'VillageController@add']);
    Route::any('list', ['as' => 'list_village', 'uses' => 'VillageController@villageList']);
    Route::post('submit-village-data', ['as' => 'submit-village-data', 'uses' => 'VillageController@processData']);
});

////////////moholla///////////////////////////////////
Route::group(['prefix' => 'moholla', 'namespace' => 'ManageRecords'], function() {
    Route::get('update/{id}', ['as' => 'edit_moholla', 'uses' => 'MohollaController@edit']);
    Route::get('add', ['as' => 'add_moholla', 'uses' => 'MohollaController@add']);
    Route::any('list', ['as' => 'list_moholla', 'uses' => 'MohollaController@mohollaList']);
    Route::post('submit-moholla-data', ['as' => 'submit-moholla-data', 'uses' => 'MohollaController@processData']);
});

////////////plot///////////////////////////////////
Route::group(['prefix' => 'plot', 'namespace' => 'ManageRecords'], function() {
    Route::get('update/{id}', ['as' => 'edit_plot', 'uses' => 'PlotController@edit']);
    Route::get('add', ['as' => 'add_plot', 'uses' => 'PlotController@add']);
    Route::any('list', ['as' => 'list_plot', 'uses' => 'PlotController@plotList']);
    Route::post('submit-plot-data', ['as' => 'submit-plot-data', 'uses' => 'PlotController@processData']);
    Route::any('edit-geometry/{id}', ['uses' => 'PlotController@editGeometry']);
});

////////////claim///////////////////////////////////
Route::group(['prefix' => 'claim', 'namespace' => 'ManageRecords'], function() {
    Route::get('update/{id}', ['as' => 'edit_claim', 'uses' => 'ClaimController@edit']);
    Route::get('add', ['as' => 'add_claim', 'uses' => 'ClaimController@add']);
    Route::any('list', ['as' => 'list_claimt', 'uses' => 'ClaimController@claimList']);
    Route::post('submit-claim-data', ['as' => 'submit-claim-data', 'uses' => 'ClaimController@processData']);
});

Route::group(['prefix' => 'report', 'namespace' => 'Report'], function() {
    Route::any('villge-claims-plot', ['as' => 'villge-claims-plot', 'uses' => 'ReportController@villageClaimsPlot']);
    Route::get('villge-plots', ['as' => 'villge-plots', 'uses' => 'ReportController@villagePlots']);
});
Route::group(['prefix' => 'kml', 'namespace' => 'KML'], function() {
    Route::get('plot/{id}/{cond}', ['as' => '', 'uses' => 'KmlController@plot']);
    //Route::get('list', ['as' => 'list_family', 'uses' => 'FamilyController@list']);
    //Route::post('submit-family-data', ['as' => 'submit-family-data', 'uses' => 'FamilyController@processData']);
});

Route::group(['prefix' => 'ajax', 'namespace' => 'Report'], function() {
    Route::post('states', ['as' => 'states', 'uses' => 'AjaxController@state']);
    Route::post('districts', ['as' => 'districts', 'uses' => 'AjaxController@district']);
    Route::post('districts-master', ['as' => 'districts-master', 'uses' => 'AjaxController@districtMaster']);
    Route::post('blocks', ['as' => 'blocks', 'uses' => 'AjaxController@block']);
    Route::post('blocks-master', ['as' => 'blocks-master', 'uses' => 'AjaxController@blockMaster']);
    Route::post('villages', ['as' => 'villages', 'uses' => 'AjaxController@village']);
    Route::post('villages-master', ['as' => 'villages-master', 'uses' => 'AjaxController@villageMaster']);
    Route::post('mohollas', ['as' => 'mohollas', 'uses' => 'AjaxController@getMohollaByVillageId']);
    Route::post('plots-by-village', ['as' => 'plots-by-village', 'uses' => 'AjaxController@getPlotsByVillageId']);
    Route::post('claimants-by-village', ['as' => 'claimants-by-village', 'uses' => 'AjaxController@getClaimantsByVillageId']);
    Route::post('claimants-by-moholla', ['as' => 'claimants-by-moholla', 'uses' => 'AjaxController@getClaimantsByMoholla']);
    Route::post('claims-by-village', ['as' => 'claims-by-village', 'uses' => 'AjaxController@getClaimsByVillageId']);
    Route::any('plotGpsGeoJSON/{id?}', ['as' => '', 'uses' => 'AjaxController@PlotGPSGeoJSON']);
    Route::any('plotSatGeoJSON/{id?}', ['as' => '', 'uses' => 'AjaxController@PlotSATGeoJSON']);
    Route::any('GeoJSONplots/{vilid?}/{plotid?}', ['as' => '', 'uses' => 'AjaxController@getGeoJSONofClaimedPlots']);
    Route::any('get-csrf', ['as' => 'get-csrf', 'uses' => 'AjaxController@getCSRF']);
});

Route::get('check-valid-user', ['as' => 'check-valid-user', 'uses' => 'LoginController@checkValidUser']);
Route::post('validate-user-login', ['as' => 'validate-user-login', 'uses' => 'LoginController@authUserByCred']);
Route::get('logout', ['as' => 'logout', 'uses' => 'LoginController@logout']);
//Route::get('import-excel', ['as' => '', 'uses' => 'ImportController@importExport']);
//Route::post('submit-excel', ['as' => '', 'uses' => 'ImportController@importExcel']);

Route::group(['prefix' => 'import'], function() {
    Route::get('survey-data/{vil_id?}', ['as' => 'survey-data', 'uses' => 'ImportController@importExcel']);
    Route::get('auto-cal-update/{village_id}', ['as' => 'auto-cal-update', 'uses' => 'ImportController@autoCalculateAndUpdate']);
    Route::post('submit-survey-data', ['as' => 'submit-survey-data', 'uses' => 'ImportController@processData']);
    Route::any('sat-gpx-data-upload', ['as' => 'sat-survey-data', 'uses' => 'ImportController@surveySAT_GPX']);
    Route::any('gps-gpx-data-upload', ['as' => 'gps-survey-data', 'uses' => 'ImportController@surveyGPS_GPX']);
});

Route::get('delete-survey-data/{village_id}', ['as' => '', 'uses' => 'FrontController@deleteSurveyData']);



