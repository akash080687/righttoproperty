<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User\UserModel;
use App\Models\Report\ClaimantModel;
use App\Models\Report\ClaimModel;
use App\Models\Report\VillageModel;
use App\Models\Report\BlockModel;
use App\Models\Report\DistrictModel;
use App\Models\ManageRecords\FamilyModel;
use App\Models\Report\PlotModel;
use App\Models\Report\MohollaModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use DB;
use Hash;
use Excel;
use XmlParser;

class ImportController extends Controller {

    public function __construct() {
        parent:: __construct();
        $this->appData['section'] = 'import-excel';
        $this->appData['include_script_view'][] = 'ImportExport.import_export_scripts';
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        $this->middleware('auth');
    }

    public function importExcel(Request $request, $village_id = null) {
        $this->appData['appData']['metaTitle'] = 'Import Survey Data';
        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];

        $state = $request->old('state');
        $district = $request->old('district');
        $block = $request->old('block');
        $village = $request->old('village');
        $this->appData['selected_village'] = $village;
        $this->appData['selected_block'] = $block;
        $this->appData['selected_district'] = $district;
        $this->appData['selected_state'] = $state;
        $this->appData['selected_moholla'] = $request->old('moholla');

        if ($village_id) {
            $this->appData['selected_village'] = $selected_village = ($village_id) ? $village_id : '';
            $this->appData['selected_block'] = $selected_block = VillageModel::where(['vil_id' => $selected_village])->value('block_id');
            // $this->appData['selected_district'] = $selected_district = BlockModel::where(['id' => "$selected_block"])->value('dist_id');
            $this->appData['selected_district'] = $selected_district = BlockModel::where(['ogc_fid' => "$selected_block"])->value('dist_id');
            // $this->appData['selected_state'] = $selected_state = DistrictModel::where(['id' => $selected_district])->value('state_id');
            $this->appData['selected_state'] = $selected_state = DistrictModel::where(['ogc_fid' => $selected_district])->value('state_id');
        }
        return view('ImportExport.import_excel', $this->appData);
    }

    public function processData(Request $request) {
		 set_time_limit(0);
        $this->importExlTmplate = array(
            "claimants" => ['claim_village_id', 'claim_no', 'claimant_id', 'claimant_resi_village_id', 'mohalla_system_id', 'claimant_name_full', 'spouse_name_full', 'claimant_voter_id', 'spouse_voter_id', 'st_otfd'],
            "claims" => ['claimant_id', 'claim_no', 'mohalla_system_id', 'claim_status', 't_area_claimed_ha', 't_area_gs_ha',
                't_area_sdlc_ha', 't_area_dlc_ha', 't_area_gps_ha', 't_area_sat_ha', 't_area_gs2_ha', 't_area_sdlc2_ha',
                't_area_dlc2_ha', 'final_status', 'adhikar_patra_recd', 'adhikar_patra_date', 'status_sdlc2', 'appeal'],
            "plots" => ['claim_village_id', 'claim_no', 'plot_no', 'plot_village_id', 'mohalla_system_id', 'survey_no', 'compt_no', 'area_claimed_ha', 'area_approved_gs_ha', 'area_approved_sdlc_ha'
                , 'area_approved_dlc_ha', 'area_gps_survey_ha', 'area_satellite_ha', 'area_approved_gs_2_ha', 'area_approved_sdlc_2_ha', 'area_approved_dlc_2_ha']
        );
        $this->required_sheets = ['Claimants', 'Claims', 'Plots'];

        $errors = [];
        $posted = $request->all();
        $this->state_id = isset($posted['state']) ? $posted['state'] : '';
        $this->village_id = isset($posted['village']) ? $posted['village'] : '';
        //$this->village_old_id = VillageModel::where(['vil_id' => $this->village_id])->value('old_id');
        $this->village_old_id = VillageModel::where(['vil_id' => $this->village_id])->value('census2011');
        $this->village_ogc_id = VillageModel::where(['vil_id' => $this->village_id])->value('ogc_fid');
        $this->appData['errors'] = '';
        // $this->appData['state_id'] = $this->state_id;
        // t($posted,1);
        if (isset($posted['import_file']) && !empty($posted['import_file'])) {
            $path = $request->file('import_file')->getRealPath();
            $extension = $request->file('import_file')->getClientOriginalExtension();
            $submit_type = isset($posted['submit_excel']) ? 'excel' : '';
            if ($submit_type == 'excel') {
                if ($extension != 'xlsx') {
                    $msg = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Not a valid excel file.Try with .xlsx extension!</strong></div>';
                    return redirect('import/survey-data')->with('message', $msg);
                } else {
                    Excel::load($path, function($reader) {
                        $sheetNames = $reader->getSheetNames();
                        //t($sheetNames);
                        $results = $reader->get();
                        $results = $reader->all()->toArray();
						//print_r($results);
                        $claimant_error = '';
                        $claim_error = '';
                        $plot_error = '';
                        $sheet_error = '';
                        $state_error = '';
                        $state_not_found_error = '';
                        $village_not_found_error = '';

                        $required_sheets = $this->required_sheets;
                        $logged_in_user_boudary_id = isset($this->appData['logged_in_user_boudary_id']) && $this->appData['logged_in_user_boudary_id'] !== 'all' ? $this->appData['logged_in_user_boudary_id'] : $this->state_id;
                        $logged_in_user_boudary_type = isset($this->appData['logged_in_user_boudary_type']) ? $this->appData['logged_in_user_boudary_type'] : '';
                        foreach ($required_sheets as $key => $value) {
                            if (!in_array($value, $sheetNames)) {
                                $sheet_error.= $value . ',';
                            }
                        }
                        ($sheet_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' . $sheet_error . ' sheet missing</strong></div>' : '';
                        foreach ($sheetNames as $key => $value) {
                            $sheet_name = $sheetNames[$key];
                            if ($sheet_name == 'Claimants') {
                                $moholla_error = '';
                                $claim_village_id_arr = $reader->select(array('claim_village_id'))->toArray();
                                $claim_village_id_arr = isset($claim_village_id_arr[0]) ? array_unique(array_values(array_column($claim_village_id_arr[0], 'claim_village_id'))) : '';
                                // t($claim_village_id_arr);
                                //exit;
                                $moholla_id_arr = $reader->select(array('mohalla_system_id'))->toArray();
                                $moholla_id_arr = isset($moholla_id_arr[0]) ? array_unique(array_values(array_column($moholla_id_arr[0], 'mohalla_system_id'))) : '';

                                $claimant_keys = isset($results[$key][0])? array_keys($results[$key][0]):[];
                                $claimant_required_keys = $this->importExlTmplate['claimants'];
                                foreach ($claimant_required_keys as $c_key => $c_val) {
                                    if (!in_array($c_val, $claimant_keys)) {
                                        $claimant_error.= $c_val . ',';
                                    }
                                }

                                ($claimant_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' . $claimant_error . ' missing in Claimants sheet</strong></div>' : '';


                                if ($claim_village_id_arr) {
                                    //$claim_village_id_arr = $claim_village_id_arr[0];
                                    foreach ($claim_village_id_arr as $c_key => $c_val) {
                                        //   $claim_village_id = '';
                                        // $c_val;
                                        if ($c_val) {
                                            //  $village_ogc_id = VillageModel::where(['census2011' => $c_val])->value('ogc_fid');
                                            $village_ogc_id = VillageModel::where(['census2011' => $c_val])->value('ogc_fid');
                                            // t($village_ogc_id);
                                            if ($village_ogc_id) {
                                                $village_state_id = VillageModel::find($village_ogc_id)->getBlock->getDistrict->state_id;
                                                if (!$village_state_id) {
                                                    $state_not_found_error .= $c_val . ',';
                                                } else if ($village_state_id != $logged_in_user_boudary_id) {
                                                    $state_error .= $c_val . ',';
                                                }
                                            } else {
                                                $village_not_found_error .= $c_val . ',';
                                            }
                                        }
                                    }
                                    ($state_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> claim_village_id: ' . $state_error . ' in Claimants sheet belongs to others states.Need state lavel permission to upload this village.</strong></div>' : '';
                                    ($state_not_found_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> claim_village_id: ' . $state_not_found_error . ' in Claimants sheet not found in database.Selected village mismatches with excel.</strong></div>' : '';
                                    ($village_not_found_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> claim_village_id: ' . $village_not_found_error . ' in Claimants sheet not found in database.Selected village mismatches with excel.</strong></div>' : '';
                                    //exit;
                                    // t($errors,1);
                                }

                                if ($moholla_id_arr) {
                                    foreach ($moholla_id_arr as $m_key => $m_value) {
                                        if ($m_value) {
                                            $moholla_master_village_id = MohollaModel::where(['census2011' => $m_value])->value('village_master_id');
                                            if ($moholla_master_village_id !== $this->village_id) {
                                                $moholla_error.= $m_value . ',';
                                            }
                                        }
                                    }
                                }

                                ($moholla_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> mohalla_system_id: ' . $moholla_error . ' in Claimants sheet are not within the selected village.</strong></div>' : '';
                                // exit;
                                // t($errors);exit;
                            } else if ($sheet_name == 'Claims') {
                                $moholla_error = '';
                                $claim_keys = isset($results[$key][0])? array_keys($results[$key][0]):[];

                                $claim_required_keys = $this->importExlTmplate['claims'];
                                foreach ($claim_required_keys as $c_key => $c_val) {
                                    if (!in_array($c_val, $claim_keys)) {
                                        $claim_error.= $c_val . ',';
                                    }
                                }
                                // exit;
                                //  $errors[] = ($claim_error) ? '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' . $claim_error . ' missing in Claims sheet</strong></div>' : '';
                                ($claim_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' . $claim_error . ' missing in Claims sheet</strong></div>' : '';

                                //$moholla_id_arr = $reader->select(array('mohalla_system_id'))->toArray();
                                $moholla_id_arr = isset($results[$key]) ? array_unique(array_values(array_column($results[$key], 'mohalla_system_id'))) : '';
                                // t($moholla_id_arr);exit;
                                if ($moholla_id_arr) {
                                    foreach ($moholla_id_arr as $m_key => $m_value) {
                                        if ($m_value) {
                                            $moholla_master_village_id = MohollaModel::where(['census2011' => $m_value])->value('village_master_id');
                                            if ($moholla_master_village_id !== $this->village_id) {
                                                $moholla_error.= $m_value . ',';
                                            }
                                        }
                                    }
                                }

                                ($moholla_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> mohalla_system_id: ' . $moholla_error . ' in Claims sheet are not within the selected village.</strong></div>' : '';
                                // exit;
                                // t($errors);exit;
                            } else if ($sheet_name == 'Plots') {
                                $moholla_error = '';
                                $plot_keys = isset($results[$key][0])? array_keys($results[$key][0]):[];
//echo $key;
                                  //t($plot_keys,1);exit;
                                $plot_required_keys = $this->importExlTmplate['plots'];
                                foreach ($plot_required_keys as $c_key => $c_val) {
                                    if (!in_array($c_val, $plot_keys)) {
                                        $plot_error.= $c_val . ',';
                                    }
                                }
                                // echo 'dfgdfg'.$plot_error;
                                ($plot_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>' . $plot_error . ' missing in PLots sheet</strong></div>' : '';
                                $moholla_id_arr = isset($results[$key]) ? array_unique(array_values(array_column($results[$key], 'mohalla_system_id'))) : '';
                                if ($moholla_id_arr) {
                                    foreach ($moholla_id_arr as $m_key => $m_value) {
                                        if ($m_value) {
                                            $moholla_master_village_id = MohollaModel::where(['census2011' => $m_value])->value('village_master_id');
                                            if ($moholla_master_village_id !== $this->village_id) {
                                                $moholla_error.= $m_value . ',';
                                            }
                                        }
                                    }
                                }

                                ($moholla_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> mohalla_system_id: ' . $moholla_error . ' in Plots sheet are not within the selected village.</strong></div>' : '';
                            }
                        }

                        // t($errors);exit;

                        if (isset($errors[0]) && $errors[0] != '') {
                            $this->appData['errors'] = $errors;
                        } else {
                            if ($results) {

                                foreach ($results as $key => $value) {
                                    $sheet_name = $sheetNames[$key];
                                    if ($sheet_name == 'Claimants') {
                                        // break;
                                        $claimant_info = $results[$key];
//                                        t($claimant_info);
//                                        exit;
                                        if ($claimant_info) {
                                            $data_to_insert = '';
                                            foreach ($claimant_info as $claimant_key => $claimant_value) {
                                                $claimant_id = isset($claimant_value['claimant_id']) ? $claimant_value['claimant_id'] : '';
                                                $mohalla_system_id = isset($claimant_value['mohalla_system_id']) ? $claimant_value['mohalla_system_id'] : '';
                                                $mohalla_ogc_id = ($mohalla_system_id) ? MohollaModel::where(['census2011' => (int) $mohalla_system_id])->value('ogc_fid') : '';
                                                if ($claimant_id) {
                                                    $claimant_res = ClaimantModel::where(['old_id' => $claimant_id, 'village_id' => $this->village_id])->get()->toArray();
                                                    $family_info = FamilyModel::where(['claimant_old' => $claimant_value['claimant_id']])->get()->toArray();
                                                    $data_to_insert['old_id'] = $claimant_value['claimant_id'];
                                                    $data_to_insert['name'] = $claimant_value['claimant_name_full'];
                                                    $data_to_insert['age'] = 0;
                                                    $data_to_insert['voterid'] = $claimant_value['claimant_voter_id'];
                                                    $data_to_insert['stofd'] = $claimant_value['st_otfd'];
                                                    // $data_to_insert['village'] = $this->village_old_id;
//                                                    $data_to_insert['village_id'] = $this->village_id;
//                                                    $data_to_insert['village_ogc_id'] = $this->village_ogc_id;
                                                    $data_to_insert['village_id_master'] = $this->village_id;
                                                    $data_to_insert['village_ogc_fid_master'] = $this->village_ogc_id;
                                                    $data_to_insert['is_old'] = true;
                                                    $data_to_insert['created_at'] = Carbon::now();
                                                    $data_to_insert['updated_at'] = Carbon::now();
                                                    $data_to_insert['adharid'] = '';
                                                    $data_to_insert['publish'] = true;
                                                    $data_to_insert['claimant_resi_village_old_id'] = $claimant_value['claimant_resi_village_id'];
                                                    $claimant_resi_village_id = VillageModel::where(['census2011' => $claimant_value['claimant_resi_village_id']])->value('vil_id');
                                                    //$data_to_insert['claimant_resi_village_id'] = $claimant_resi_village_id;
                                                    $data_to_insert['claimant_resi_master_village_id'] = $claimant_resi_village_id;
                                                    $data_to_insert['excel_uploaded'] = true;
                                                    $data_to_insert['mohallah_id_master'] = $mohalla_ogc_id;
                                                    // t($data_to_insert);exit;
                                                    $family_data_to_insert['old_id'] = '';
                                                    $family_data_to_insert['name'] = $claimant_value['spouse_name_full'];
                                                    $family_data_to_insert['age'] = 0;
                                                    $family_data_to_insert['relation'] = 'SPOUSE';
                                                    $family_data_to_insert['claimant_old'] = $claimant_value['claimant_id'];

                                                    $family_data_to_insert['is_old'] = false;
                                                    $family_data_to_insert['created_at'] = Carbon::now();
                                                    $family_data_to_insert['updated_at'] = Carbon::now();
                                                    $family_data_to_insert['publish'] = true;
                                                    $family_data_to_insert['excel_uploaded'] = true;
                                                    if (empty($claimant_res)) {
                                                        $insert_id = ClaimantModel::insertGetId($data_to_insert);
                                                        $family_data_to_insert['claimant_id'] = $insert_id;

                                                        if (empty($family_info)) {
                                                            $family_insert_id = FamilyModel::insertGetId($family_data_to_insert);
                                                        } else {
                                                            $update_family = FamilyModel::where(['claimant_old' => $claimant_value['claimant_id']])->update($family_data_to_insert);
                                                        }
                                                    } else {
                                                        //$new_claimant_id = ClaimantModel::where(['old_id' => $claimant_id, 'village_id' => $this->village_id])->value('id');
                                                        $new_claimant_id = ClaimantModel::where(['old_id' => $claimant_id, 'village_id_master' => $this->village_id])->value('id');
                                                        $family_data_to_insert['claimant_id'] = $new_claimant_id;
                                                        if (empty($family_info)) {
                                                            $family_insert_id = FamilyModel::insertGetId($family_data_to_insert);
                                                        } else {
                                                            $update_family = FamilyModel::where(['claimant_old' => $claimant_value['claimant_id']])->update($family_data_to_insert);
                                                        }
                                                        //$update_claimant = ClaimantModel::where(['old_id' => $claimant_id, 'village_id' => $this->village_id])->update($data_to_insert);
                                                        $update_claimant = ClaimantModel::where(['old_id' => $claimant_id, 'village_id_master' => $this->village_id])->update($data_to_insert);
                                                    }
                                                    //   exit;
                                                }
                                            }
                                            echo 'insertted...';
                                            // exit;
                                        }
                                    } else if ($sheet_name == 'Claims') {
                                        $claim_info = $results[$key];
//                                        t($claim_info);
//                                        exit;
                                        if ($claim_info) {
                                            $data_to_insert = '';
                                            foreach ($claim_info as $claim_key => $claim_value) {
                                                $claim_res = ClaimModel::where(['number' => $claim_value['claim_no'], 'claimant' => $claim_value['claimant_id'], 'village_id' => $this->village_id])->get()->toArray();
                                                // t($claim_res);
                                                $claimant_new_id = ClaimantModel::where(['old_id' => trim($claim_value['claimant_id'])])->value('id');
                                                $mohalla_system_id = isset($claim_value['mohalla_system_id']) ? $claim_value['mohalla_system_id'] : '';
                                                $mohalla_ogc_id = ($mohalla_system_id) ? MohollaModel::where(['census2011' => (int) $mohalla_system_id])->value('ogc_fid') : '';
                                                // exit;
                                                $data_to_insert['claimant'] = isset($claim_value['claimant_id']) ? $claim_value['claimant_id'] : NULL;
                                                //$data_to_insert['village'] = $this->village_old_id;
                                                //$data_to_insert['tareaclaimed'] = 0;  //will be auto calculated
                                                $data_to_insert['gsdecision'] = isset($claim_value['gs_decision']) && !empty(trim($claim_value['gs_decision'])) ? trim($claim_value['gs_decision']) : '';
                                                //$data_to_insert['areaapprovedgsha'] = 0; //will be auto calculated
                                                $data_to_insert['sdlcdecision'] = isset($claim_value['sdlc_decision']) && !empty(trim($claim_value['sdlc_decision'])) ? trim($claim_value['sdlc_decision']) : '';
                                                //$data_to_insert['areaapprovedsdlcha'] = 0; //will be auto calculated
                                                $data_to_insert['dlcdecision'] = isset($claim_value['dlc_decision']) && !empty(trim($claim_value['dlc_decision'])) ? trim($claim_value['dlc_decision']) : '';
                                                //$data_to_insert['areaapproveddlcha'] = 0; //will be auto calculated
                                                $data_to_insert['appealfield'] = '';
                                                //$data_to_insert['village_id'] = $this->village_id;
                                                $data_to_insert['village_id_master'] = $this->village_id;
                                                $data_to_insert['claimant_id'] = ($claimant_new_id) ? $claimant_new_id : 0;
                                                $data_to_insert['number'] = isset($claim_value['claim_no']) ? $claim_value['claim_no'] : '';
                                                $data_to_insert['is_old'] = false;
                                                $data_to_insert['created_at'] = Carbon::now();
                                                $data_to_insert['updated_at'] = Carbon::now();
                                                $data_to_insert['publish'] = true;
                                                $data_to_insert['excel_uploaded'] = true;
                                                $data_to_insert['mohallah_id_master'] = $mohalla_ogc_id;
                                                //t($data_to_insert);
                                                if (empty($claim_res)) {
                                                    if (!$data_to_insert['claimant_id'] == 0) {
                                                        $claim_insert_id = ClaimModel::insertGetId($data_to_insert);
                                                    }
                                                } else {
                                                    if ($claimant_new_id) {
                                                        //$update_claimant = ClaimModel::where(['number' => $claim_value['claim_no'], 'claimant_id' => $claimant_new_id, 'village_id' => $this->village_id])->update($data_to_insert);
                                                        $update_claimant = ClaimModel::where(['number' => $claim_value['claim_no'], 'claimant_id' => $claimant_new_id, 'village_id_master' => $this->village_id])->update($data_to_insert);
                                                    }
                                                }
                                            }
                                        }
                                    } else if ($sheet_name == 'Plots') {
                                        $plot_info = $results[$key];
                                        // t($plot_info,1);exit;
                                        if ($plot_info) {
                                            $data_to_insert = '';
                                            $i = 0;
                                            foreach ($plot_info as $plot_key => $plot_value) {

                                                //$old_claim_id = ClaimModel::where(['village_id' => $this->village_id, 'number' => $plot_value['claim_no']])->value('old_id');
                                                $mohalla_system_id = isset($plot_value['mohalla_system_id']) ? $plot_value['mohalla_system_id'] : '';
                                                $mohalla_ogc_id = ($mohalla_system_id) ? MohollaModel::where(['census2011' => (int) $mohalla_system_id])->value('ogc_fid') : '';
                                                if ($mohalla_ogc_id) {
                                                    $old_claim_id = ClaimModel::where(['village_id_master' => $this->village_id, 'mohallah_id_master' => $mohalla_ogc_id, 'number' => $plot_value['claim_no']])->value('old_id');
                                                } else {
                                                    $old_claim_id = ClaimModel::where(['village_id_master' => $this->village_id, 'number' => $plot_value['claim_no']])->value('old_id');
                                                }
                                                //$new_claim_id = ClaimModel::where(['village_id' => $this->village_id, 'number' => $plot_value['claim_no']])->value('id');
                                                if ($mohalla_ogc_id) {
                                                    $new_claim_id = ClaimModel::where(['village_id_master' => $this->village_id, 'mohallah_id_master' => $mohalla_ogc_id, 'number' => $plot_value['claim_no']])->value('id');
                                                } else {
                                                    $new_claim_id = ClaimModel::where(['village_id_master' => $this->village_id, 'number' => $plot_value['claim_no']])->value('id');
                                                }
                                                $plot_village_id = $plot_value['plot_village_id'];
                                                //$plot_village_new_id = VillageModel::where(['census2011' => $plot_village_id])->value('vil_id');
                                                $plot_village_new_id = VillageModel::where(['census2011' => $plot_village_id])->value('vil_id');
                                                // $data_to_insert['claim'] = 0;


                                                ($old_claim_id) ? $data_to_insert['claim'] = $old_claim_id : '';
                                                $data_to_insert['village'] = $plot_village_id;
                                                $data_to_insert['plt_number'] = $plot_value['plot_no'];
                                                $data_to_insert['surveyno'] = $plot_value['survey_no'];
                                                $data_to_insert['compartment_no'] = $plot_value['compt_no'];
                                                $data_to_insert['areaclaimedh'] = isset($plot_value['area_claimed_ha']) ? $plot_value['area_claimed_ha'] : 0;
                                                $data_to_insert['areaapprovedgsh'] = isset($plot_value['area_approved_gs_ha']) && !empty(trim($plot_value['area_approved_gs_ha'])) ? trim($plot_value['area_approved_gs_ha']) : 0;
                                                $data_to_insert['areaapprovedsdlch'] = isset($plot_value['area_approved_sdlc_ha']) && !empty(trim($plot_value['area_approved_sdlc_ha'])) ? trim($plot_value['area_approved_sdlc_ha']) : null;
                                                $data_to_insert['areaapproveddlch'] = isset($plot_value['area_approved_dlc_ha']) && !empty(trim($plot_value['area_approved_dlc_ha'])) ? trim($plot_value['area_approved_dlc_ha']) : null;
//                                                $data_to_insert['areaapprovedsdlch'] = null;
//                                                $data_to_insert['areaapproveddlch'] = null;
                                                // $data_to_insert['areagpssurveyh'] = isset($plot_value['area_gps_survey_ha']) && !empty(trim($plot_value['area_gps_survey_ha'])) ? trim($plot_value['area_gps_survey_ha']) : 0;
                                                //$data_to_insert['vil_id'] = $plot_village_new_id;
                                                $data_to_insert['village_id_master'] = $plot_village_new_id;
                                                $data_to_insert['mohallah_id_master'] = $mohalla_ogc_id;
                                                // $data_to_insert['areaSat'] = isset($plot_value['area_satellite_ha']) && !empty(trim($plot_value['area_satellite_ha'])) ? trim($plot_value['area_satellite_ha']) : 0;
                                                $data_to_insert['claim_id'] = $new_claim_id;
                                                $data_to_insert['is_old'] = false;
                                                $data_to_insert['created_at'] = Carbon::now();
                                                $data_to_insert['updated_at'] = Carbon::now();
                                                $data_to_insert['publish'] = true;
                                                $data_to_insert['excel_uploaded'] = true;
                                                $data_to_insert['area_approved_gs_2_ha'] = isset($plot_value['area_approved_gs_2_ha']) && !empty(trim($plot_value['area_approved_gs_2_ha'])) ? trim($plot_value['area_approved_gs_2_ha']) : 0;
                                                $data_to_insert['area_approved_sdlc_2_ha'] = isset($plot_value['area_approved_sdlc_2_ha']) && !empty(trim($plot_value['area_approved_sdlc_2_ha'])) ? trim($plot_value['area_approved_sdlc_2_ha']) : 0;
                                                $data_to_insert['area_approved_dlc_2_ha'] = isset($plot_value['area_approved_dlc_2_ha']) && !empty(trim($plot_value['area_approved_dlc_2_ha'])) ? trim($plot_value['area_approved_dlc_2_ha']) : 0;
//                                                t($data_to_insert);
//                                                exit;
                                                if ($new_claim_id) {
//                                                    
                                                    $plot_exist_info = PlotModel::where(['claim_id' => $new_claim_id, 'plt_number' => $plot_value['plot_no']])->get()->toArray();
                                                    //  t($plot_exist_info);
                                                    if (empty($plot_exist_info)) {

                                                        $insert = PlotModel::insert($data_to_insert);
                                                    } else {

                                                        $update = PlotModel::where(['claim_id' => $new_claim_id, 'plt_number' => $plot_value['plot_no']])->update($data_to_insert);
                                                    }
                                                }
                                                //   if ($plot_key == 2)break;
                                            }
                                            //  echo 'inserted...';
                                            // exit;
                                        }
                                    }
                                }
                            }
                        }
                    });
                    //    exit;
                    if (!empty($this->appData['errors'])) {
                        return redirect('import/survey-data')->with('errors', isset($this->appData['errors']) ? $this->appData['errors'] : '')->withInput();
                    } else {
                        // $this->autoCalculateAndUpdate($this->village_id);
                        $update_village_rtp_status = VillageModel::where(['vil_id' => $this->village_id])->update(['rtp_status' => TRUE]);
                        $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Data uploaded successfully!</strong></div>';
                        return redirect('import/survey-data')->with('message', $msg)->withInput();
                    }
                }
            }
        }
//        return back();
    }

    public function autoCalculateAndUpdate($village_id) {
        $sql_sat = "(st_area(ST_Transform(st_setsrid(wkb_geometry_sat,4326),utmzone(ST_Centroid(st_setsrid(wkb_geometry_sat,4326)))))/10000) ";
        $sql_sat = 'UPDATE plot SET "areaSat"=' . $sql_sat;
        DB::select(DB::raw($sql_sat));
        $sql_gps = "(st_area(ST_Transform(st_setsrid(wkb_geometry,4326),utmzone(ST_Centroid(st_setsrid(wkb_geometry,4326)))))/10000) ";
        $sql_gps = 'UPDATE plot SET areagpssurveyh=' . $sql_gps;
        DB::select(DB::raw($sql_gps));
        // $vill_id = VillageModel::where(['ogc_fid' => $village_id])->value('vil_id');
        //$claims = ClaimModel::where(['village_id' => "$village_id"])->orderBy('number', 'asc')->with('plots')->get()->toArray();
        $claims = ClaimModel::where(['village_id_master' => "$village_id"])->orderBy('number', 'asc')->with('plots')->get()->toArray();
        // t($claims);
        $data_to_update = [];
        foreach ($claims as $key => $value) {
            $plots = isset($value['plots']) ? $value['plots'] : '';
            if ($plots) {
                $total_area_claimed = 0;
                $t_area_gs = 0;
                $t_area_sdlc = 0;
                $t_area_dlc = 0;
                $t_area_gps = 0;
                $t_area_sat = 0;
                $t_area_gs2 = 0;
                $t_area_sdlc2_ha = 0;
                $t_area_dlc2_ha = 0;
//                echo '<pre>';


                foreach ($plots as $p_key => $p_val) {
//                    if ($value['id'] == 19261)
//                        print_r($plots);
                    $total_area_claimed+=$p_val['areaclaimedh'];
                    $t_area_gs+=$p_val['areaapprovedgsh'];
                    $t_area_sdlc+=$p_val['areaapprovedsdlch'];
                    $t_area_dlc+=$p_val['areaapproveddlch'];
                    $t_area_gps+=$p_val['areagpssurveyh'];
                    $t_area_sat+=$p_val['areaSat'];
                    $t_area_gs2+=$p_val['area_approved_gs_2_ha'];
                    $t_area_sdlc2_ha+=$p_val['area_approved_sdlc_2_ha'];
                    $t_area_dlc2_ha+=$p_val['area_approved_dlc_2_ha'];
                }
//                if ($value['id'] == 19261) {
//
//                    echo $t_area_dlc . '--';
//                    echo ($t_area_sat - $t_area_dlc);
//                    die;
//                }
                $data_to_update['tareaclaimed'] = $total_area_claimed;
                $data_to_update['areaapprovedgsha'] = $t_area_gs;
                $data_to_update['areaapprovedsdlcha'] = $t_area_sdlc;
                $data_to_update['areaapproveddlcha'] = $t_area_dlc;
                $data_to_update['t_area_gps_ha'] = $t_area_gps;
                $data_to_update['t_area_sat_ha'] = $t_area_sat;
                $data_to_update['t_area_gs2_ha'] = $t_area_gs2;
                $data_to_update['t_area_sdlc2_ha'] = ($t_area_sdlc2_ha == null) ? null : 0;
                $data_to_update['t_area_dlc2_ha'] = ($t_area_dlc2_ha == null) ? null : 0;
                if ($t_area_dlc == 0 || $t_area_dlc == null) {
                    $claim_status = 'P';
                } else if ($t_area_dlc > 0 && (($t_area_sat - $t_area_dlc) <= 0.1)) {
                    $claim_status = 'A1';
//                    echo 'first a1';
                } else if ($t_area_dlc > 0 && (($t_area_gs - $t_area_dlc) <= 0.1)) {
                    $claim_status = 'A1';
//                    echo 'second a1';
                } else {
                    $claim_status = 'A2';
                }
//                echo 'Claim status' . $claim_status;                die;
                if ($t_area_dlc2_ha == null) {
                    $final_status = 'P';
                } else if ($t_area_dlc2_ha == 0) {
                    $final_status = 'R';
                } else if (($t_area_gs2 - $t_area_dlc2_ha) <= 1) {
                    $final_status = 'A1';
                } else {
                    $final_status = 'A2';
                }

                if ($t_area_sdlc2_ha == null) {
                    $status_sdlc2 = 'P';
                } else if ($t_area_sdlc2_ha == 0) {
                    $status_sdlc2 = 'R';
                } else if (($t_area_sdlc2_ha - $t_area_dlc2_ha) <= 1) {
                    $status_sdlc2 = 'A1';
                } else {
                    $status_sdlc2 = 'A2';
                }
                $data_to_update['claim_status'] = $claim_status;
                $data_to_update['final_status'] = $final_status;
                $data_to_update['status_sdlc2'] = $status_sdlc2;
//                t($data_to_update);
//                exit;
                ClaimModel::where(['id' => $value['id']])->update($data_to_update);
            }
        }
    }

    public function surveySAT_GPX(Request $request) {


        $posted_data = $request->all();
//        t($posted_data);
//        exit;
        $state_error = '';
        $state_not_found_error = '';
        $plot_error = '';
        $plotno_arr = [];
        $plot_no_mohola_arr = [];
        $errors = [];

        $extension = (isset($posted_data['sat_gpx_file']) && !empty($posted_data['sat_gpx_file'])) ? $request->file('sat_gpx_file')->getClientOriginalExtension() : '';
        $village_id = isset($posted_data['village']) ? $posted_data['village'] : '';
        $moholla_id = isset($posted_data['moholla']) ? $posted_data['moholla'] : '';
        $village_ogc_id = VillageModel::where(['vil_id' => $village_id])->value('ogc_fid');
        $village_state_id = VillageModel::find($village_ogc_id)->getBlock->getDistrict->state_id;
        $posted_state_id = isset($posted_data['state']) ? $posted_data['state'] : '';
        $logged_in_user_boudary_id = isset($this->appData['logged_in_user_boudary_id']) ? $this->appData['logged_in_user_boudary_id'] : $posted_state_id;
        if (($village_state_id != $logged_in_user_boudary_id) && ($logged_in_user_boudary_id !== 'all')) {
            $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> You do not have permission to upload gpx info for this village</strong></div>';
        }

        if ($village_id) {
            $res = VillageModel::where(['vil_id' => $village_id])->with('claims.plots')->get()->toArray();
            foreach ($res as $key => $value) {
                $claims = $value['claims'];
                if ($claims) {
                    foreach ($claims as $c_key => $c_value) {
                        $plots = $c_value['plots'];
                        if ($plots) {
                            foreach ($plots as $p_key => $p_value) {
                                if ($p_value['mohallah_id_master'] == $moholla_id) {
                                    $plot_no_mohola_arr[$p_value['plt_number']] = $p_value['ogc_fid'];
                                } else {
                                    $plotno_arr[$p_value['plt_number']] = $p_value['ogc_fid'];
                                }
                            }
                        }
                    }
                }
            }
        }
        $plotno_arr = ($plot_no_mohola_arr) ? $plot_no_mohola_arr : $plotno_arr;
        //  t($plotno_arr);exit;
        if (!empty($errors)) {
            return redirect('import/survey-data')->with('errors', $errors)->withInput();
        }
        if (isset($posted_data['sat_gpx_file']) && !empty($posted_data['sat_gpx_file'])) {
            // t($posted_data, 1);exit;
            if ($extension != 'gpx') {
                $msg = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please try with .gpx extension!</strong></div>';
                return redirect('import/survey-data')->with('message', $msg)->withInput();
            } else {
                $path = $request->file('sat_gpx_file')->getRealPath();
                $dom = new \DOMDocument();
                $dom->load($path);
                $dom->preserveWhiteSpace = false;
                $trks = $dom->getElementsByTagName("trk");
//                $village_code_2011 = $dom->getElementsByTagName("village_code_2011")->item(0)->nodeValue;
//                $gpx_village_old_id = VillageModel::where(['census2011' => $village_code_2011])->value('old_id');
//                $gpx_village_new_id = VillageModel::where(['old_id' => $gpx_village_old_id])->value('vil_id');
//                if (!$gpx_village_new_id) {
//                    $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> Village id mentioned in gpx file not found in database</strong></div>';
//                } else if ($gpx_village_new_id != $village_id) {
//                    $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> Selected village id and gpx file village id does not match</strong></div>';
//                }

                if (empty($errors)) {
                    foreach ($trks as $trk) {
                        $plotno = $trk->getElementsByTagName('name')->item(0)->nodeValue;
                        $plotno = ltrim($plotno, '0');
                        $plot_id = isset($plotno_arr[$plotno]) ? $plotno_arr[$plotno] : '';
                        if ($plot_id != '') {

                            $plotno = $trk->getElementsByTagName('name')->item(0)->nodeValue;
                            $plotno = ltrim($plotno, '0');
                            $plot_id = isset($plotno_arr[$plotno]) ? $plotno_arr[$plotno] : '';
                            $coordinates = $trk->getElementsByTagName('trkpt');
                            $latlonarr = '';
                            foreach ($coordinates as $coordinateEle) {
                                $latVal = $coordinateEle->getAttribute('lat');
                                $lngVal = $coordinateEle->getAttribute('lon');

                                $coord = $lngVal . ' ' . $latVal;
                                $latlonarr[] = $coord;
                            }
                            if ($latlonarr[0] != end($latlonarr)) {
                                $latlonarr[] = $latlonarr[0];
                            }
                            $latlonstr = implode(',', $latlonarr);
                            $update = PlotModel::where(['ogc_fid' => $plot_id])->update(['wkb_geometry_sat' => DB::raw("st_geomfromtext('MULTIPOLYGON(((" . $latlonstr . ")))',4326)")]);
                        } else {
                            $plot_error.= $plotno . ',';
                        }
                    }
                }
                //echo $plot_error ;exit;
                ($plot_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> Plot number : ' . $plot_error . ' not found in database</strong></div>' : '';

                if (!empty($errors)) {
                    return redirect('import/survey-data')->with('errors', $errors)->withInput();
                } else {

                    $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gpx data uploaded successfully!</strong></div>';
                    $this->autoCalculateAndUpdate($village_id);
                    return redirect('import/survey-data')->with('message', $msg)->withInput();
                }
            }
        } else {
            return redirect('import/survey-data')->with('errors', $errors)->withInput();
        }
    }

    public function surveyGPS_GPX(Request $request) {


        $posted_data = $request->all();
        // t($posted_data);exit;
        $state_error = '';
        $state_not_found_error = '';
        $plot_error = '';
        $plotno_arr = [];
        $errors = [];
          $plot_no_mohola_arr = [];
        $extension = (isset($posted_data['gps_gpx_file']) && !empty($posted_data['gps_gpx_file'])) ? $request->file('gps_gpx_file')->getClientOriginalExtension() : '';
        $village_id = isset($posted_data['village']) ? $posted_data['village'] : '';
        $moholla_id = isset($posted_data['moholla']) ? $posted_data['moholla'] : '';
        $village_ogc_id = VillageModel::where(['vil_id' => $village_id])->value('ogc_fid');
        $village_state_id = VillageModel::find($village_ogc_id)->getBlock->getDistrict->state_id;
        $posted_state_id = isset($posted_data['state']) ? $posted_data['state'] : '';
        $logged_in_user_boudary_id = isset($this->appData['logged_in_user_boudary_id']) ? $this->appData['logged_in_user_boudary_id'] : $posted_state_id;

        if (($village_state_id != $logged_in_user_boudary_id) && ($logged_in_user_boudary_id !== 'all')) {
            $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> You do not have permission to upload gpx info for this village</strong></div>';
        }
        if ($village_id) {
            $res = VillageModel::where(['vil_id' => $village_id])->with('claims.plots')->get()->toArray();
            foreach ($res as $key => $value) {
                $claims = $value['claims'];
                if ($claims) {
                    foreach ($claims as $c_key => $c_value) {
                        $plots = $c_value['plots'];
                        if ($plots) {
                            foreach ($plots as $p_key => $p_value) {
                                if ($p_value['mohallah_id_master'] == $moholla_id) {
                                    $plot_no_mohola_arr[$p_value['plt_number']] = $p_value['ogc_fid'];
                                } else {
                                    $plotno_arr[$p_value['plt_number']] = $p_value['ogc_fid'];
                                }
                            }
                        }
                    }
                }
            }
        }
        $plotno_arr = ($plot_no_mohola_arr) ? $plot_no_mohola_arr : $plotno_arr;
        //    t($plotno_arr,1);
        if (!empty($errors)) {
            return redirect('import/survey-data')->with('errors', $errors)->withInput();
        }
        if (isset($posted_data['gps_gpx_file']) && !empty($posted_data['gps_gpx_file'])) {
            // t($posted_data, 1);exit;
            if ($extension != 'gpx') {
                $msg = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please try with .gpx extension!</strong></div>';
                return redirect('import/survey-data')->with('message', $msg)->withInput();
            } else {
                $path = $request->file('gps_gpx_file')->getRealPath();
                $dom = new \DOMDocument();
                $dom->load($path);
                $dom->preserveWhiteSpace = false;
                $trks = $dom->getElementsByTagName("trk");
//                $village_code_2011 = $dom->getElementsByTagName("village_code_2011")->item(0)->nodeValue;
//                $gpx_village_old_id = VillageModel::where(['census2011' => $village_code_2011])->value('old_id');
//                $gpx_village_new_id = VillageModel::where(['old_id' => $gpx_village_old_id])->value('vil_id');
//                if (!$gpx_village_new_id) {
//                    $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> Village id mentioned in gpx file not found in database</strong></div>';
//                } else if ($gpx_village_new_id != $village_id) {
//                    $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> Selected village id and gpx file village id does not match</strong></div>';
//                }

                if (empty($errors)) {
                    foreach ($trks as $trk) {
                        $plotno = $trk->getElementsByTagName('name')->item(0)->nodeValue;
                        $plotno = ltrim($plotno, '0');
                        $plot_id = isset($plotno_arr[$plotno]) ? $plotno_arr[$plotno] : '';
                        if ($plot_id != '') {

                            $plotno = $trk->getElementsByTagName('name')->item(0)->nodeValue;
                            $plotno = ltrim($plotno, '0');
                            $plot_id = isset($plotno_arr[$plotno]) ? $plotno_arr[$plotno] : '';
                            $coordinates = $trk->getElementsByTagName('trkpt');
                            $latlonarr = '';
                            foreach ($coordinates as $coordinateEle) {
                                $latVal = $coordinateEle->getAttribute('lat');
                                $lngVal = $coordinateEle->getAttribute('lon');

                                $coord = $lngVal . ' ' . $latVal;
                                $latlonarr[] = $coord;
                            }
                            if ($latlonarr[0] != end($latlonarr)) {
                                $latlonarr[] = $latlonarr[0];
                            }
                            $latlonstr = implode(',', $latlonarr);
                            $update = PlotModel::where(['ogc_fid' => $plot_id])->update(['wkb_geometry' => DB::raw("st_geomfromtext('MULTIPOLYGON(((" . $latlonstr . ")))',4326)")]);
                        } else {
                            $plot_error.= $plotno . ',';
                        }
                    }
                }
                //echo $plot_error ;exit;
                ($plot_error) ? $errors[] = '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong> Plot number : ' . $plot_error . ' not found in database</strong></div>' : '';

                if (!empty($errors)) {
                    return redirect('import/survey-data')->with('errors', $errors)->withInput();
                } else {

                    $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Gpx data uploaded successfully!</strong></div>';
                    $this->autoCalculateAndUpdate($village_id);
                    echo 'auto calculation done';
                    return redirect('import/survey-data')->with('message', $msg)->withInput();
                }
            }
        } else {
            return redirect('import/survey-data')->with('errors', $errors)->withInput();
        }
    }

}
