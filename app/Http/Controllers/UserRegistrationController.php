<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Models\User\RtpUserModel;

//use App\User;
//use App\Http\Controllers\Controller;

class UserRegistrationController extends Controller {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function registration(Request $request) {
        $messages = [
            'firstname.required' => 'First Name is required',
            'firstname.max' => 'First name should be maximum 75 charecters',
            'lastname.required' => 'Last Name is required',
            'lastname.max' => 'Last name should be maximum 75 charecters',
            'email.required' => 'Email is required',
            'contactno.required' => 'Contact No is required',
            'password.required' => 'Password is required',
            'confirmPass.required' => 'Confirm Password is required',
            'confirmPass.same' => 'Confirm Password must match with password',
            'way_to_use.required' => 'Please share how you want to access the website',
            'way_to_use.max' => 'Way to use should be less than 500 characters.',
            'g-recaptcha-response.required' => 'Please provide captcha response'
        ];
        if ($request->all()) {
            $validator = Validator::make($request->all(), [
                        'firstname' => 'required|max:75',
                        'lastname' => 'required|max:75',
                        'email' => 'required',
                        // 'contactno' => 'required',
                        'password' => 'required',
                        'confirmPass' => 'required|same:password',
                        // 'way_to_use' => 'required|max:500',
                        // 'g-recaptcha-response' => 'required'
                            ], $messages);
//        
            if ($validator->fails()) {

                return redirect('registration')
                                ->withErrors($validator, 'registration')
                                ->withInput($request->all());
            } else {
                $data = $request->all();
                $data_to_insert = array();
                $data_to_insert['first_name'] = $data['firstname'];
                $data_to_insert['last_name'] = $data['lastname'];
                $data_to_insert['email'] = $data['email'];
                $data_to_insert['user_name'] = $data['email'];
                $data_to_insert['contact_no'] = $data['contactno'];
                $data_to_insert['password'] = md5($data['password']);
                $data_to_insert['site_acess_pattern'] = $data['way_to_use'];

                $new_user = RtpUserModel::insertGetId($data_to_insert);

                //print_r($new_user);
                //die();
                $msg = '<div class="alert alert-success"><a href="javascript:void(0);" onclick="$(this).parent().remove();" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>User added successfully!</strong></div>';
                return redirect('registration')->with('message', $msg);
            }
        }

        return view('registration.view');
    }

}
