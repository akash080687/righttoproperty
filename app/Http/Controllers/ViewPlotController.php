<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Report\VillageModel;
use App\Models\Report\ClaimantModel;
use App\Models\ManageRecords\FamilyModel;
use App\Models\Report\ClaimModel;
use App\Models\Report\PlotModel;
use Illuminate\Http\Request;

class ViewPlotController extends Controller {

    public function __construct() {
        parent:: __construct();
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
    }

    public function viewPlots(Request $request) {

        $this->appData['include_script_view'][] = 'report.common_scripts';
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        $this->appData['page'] = 'view_plots';

        $posted_data = $request->all();
        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : '';
        return view('front.view_plots', $this->appData);
    }

    public function drawPlotView(Request $request) {
        $posted_data = $request->all();
        $state_id = isset($posted_data['state_id']) ? $posted_data['state_id'] : '';
        $district_id = isset($posted_data['district_id']) ? $posted_data['district_id'] : '';
        $block_id = isset($posted_data['block_id']) ? $posted_data['block_id'] : '';
        $village_id = isset($posted_data['village_id']) ? $posted_data['village_id'] : '';
        $moholla_id = isset($posted_data['moholla_id']) ? $posted_data['moholla_id'] : '';
        $selected_claimants = isset($posted_data['claimant']) && !empty($posted_data['claimant']) ? implode(',', array_filter($posted_data['claimant'])) : '';
        $selected_claims = isset($posted_data['claim']) && !empty($posted_data['claim']) ? implode(',', $posted_data['claim']) : '';
        $cond = ' 1 = 1';
        if ($village_id) {
            if (!$moholla_id) {
                $cond.= " and village_id_master = '$village_id' ";
            } else {
                $cond.= " and village_id_master = '$village_id' and mohallah_id_master= '$moholla_id'";
            }
           // echo $cond;exit;
            if ($selected_claimants) {
                //echo $selected_claimants;exit;
                $res = ClaimModel::whereRaw($cond)->whereRaw(" claimant_id in ($selected_claimants)")->with(['plots' => function($query) {
                                $query->selectRaw(" ogc_fid, replace(replace (ST_AsText(wkb_geometry),'MULTIPOLYGON(((',''),')))','')  as lat_lon_str,claim_id");
                            }])->get()->toArray();
            } else if ($selected_claims) {
                $res = ClaimModel::whereRaw($cond)->whereRaw(" id in ($selected_claims)")->with(['plots' => function($query) {
                                $query->selectRaw(" ogc_fid, replace(replace (ST_AsText(wkb_geometry),'MULTIPOLYGON(((',''),')))','')  as lat_lon_str,claim_id");
                            }])->get()->toArray();
            } else {
                $res = ClaimModel::whereRaw($cond)->with(['plots' => function($query) {
                                $query->selectRaw(" ogc_fid, replace(replace (ST_AsText(wkb_geometry),'MULTIPOLYGON(((',''),')))','')  as lat_lon_str,claim_id");
                            }])->get()->toArray();
            }
           // t($res);exit;
            $all_plots = [];
            if ($res) {
                foreach ($res as $key => $value) {
                    $plots = $value['plots'];
                    if ($plots) {
                        foreach ($plots as $p_key => $p_value) {
                            $all_plots[] = $p_value;
                        }
                    }
                }
            }
            //t($all_plots);

            echo json_encode($all_plots);
        }
    }
    public function plotInfoWindow(Request $request) {
        $posted_data = $request->all();
        $plot_id = isset($posted_data['plot_id']) ? $posted_data['plot_id'] : '';
        $plot_details = PlotModel::where(['ogc_fid' => $plot_id])->get()->toArray();
        //t($plot_details);exit;
        $this->appData['plot_details'] = isset($plot_details[0]) ? $plot_details[0] : '';
        $this->appData['posted_data'] = $posted_data;
        echo view('front.plot_info_window', $this->appData)->render();
    }

}
