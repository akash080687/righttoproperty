<?php

namespace App\Http\Controllers\ManageRecords;
use App\Http\Controllers\Controller;
use App\Models\Report\ClaimantModel;
use App\Models\Report\BlockModel;
use App\Models\Report\VillageModel;
use App\Models\Report\DistrictModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use DB;

class ClaimantController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
        $this->appData['section'] = 'claimant';
        $this->appData['include_script_view'][] = 'managerecords.common_scripts';
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        $this->middleware('auth');
    }

    public function add(Request $request) {
        //echo 'ssfsaf';
//        $this->appData['appData']['metaTitle'] = $this->metaTitle;
//        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];
        $request->old('state');
        $state = $request->old('state');
        $district = $request->old('district');
        $block = $request->old('block');
        $village = $request->old('claimant.village_id_master');
        $mahalla = $request->old('claimant.mohallah_id_master');
        //  $claimnts = $request->old('family.claimant_id');
        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $this->appData['selected_state'] = isset($state) ? $state : $this->appData['logged_in_user_boudary_id'];
        $this->appData['selected_district'] = $district;
        $this->appData['selected_block'] = $block;
        $this->appData['selected_villages'] = $village;
        $this->appData['selected_moholla'] = $mahalla;
        return view('managerecords.claimant.add', $this->appData);
    }

    public function edit($id) {
        if (!$id) {
            return 'claimant id missing';
        }
        $this->appData['appData']['metaTitle'] = $this->metaTitle;

        $claimant_data = ClaimantModel::where(['id' => $id])->get()->toArray();
        // t($claimant_data);exit;
        $this->appData['claimant_data'] = $claimant_data = isset($claimant_data[0]) ? $claimant_data[0] : '';
        // t($claimant_data);
        //   $this->appData['selected_villages'] = $selected_village = isset($claimant_data['village_id']) ? $claimant_data['village_id'] : '';
        $this->appData['selected_villages'] = $selected_village = isset($claimant_data['village_id_master']) ? $claimant_data['village_id_master'] : '';
        $this->appData['selected_moholla'] = $selected_moholla = isset($claimant_data['mohallah_id_master']) ? $claimant_data['mohallah_id_master'] : '';
        //$this->appData['selected_resi_village'] = $selected_resi_village = isset($claimant_data['claimant_resi_village_id']) ? $claimant_data['claimant_resi_village_id'] : '';
        $this->appData['selected_resi_village'] = $selected_resi_village = isset($claimant_data['claimant_resi_master_village_id']) ? $claimant_data['claimant_resi_master_village_id'] : '';
        $this->appData['selected_block'] = $selected_block = ($selected_village) ? VillageModel::where(['vil_id' => "$selected_village"])->value('block_id') : '';
        $this->appData['selected_district'] = $selected_district = ($selected_block) ? BlockModel::where(['ogc_fid' => "$selected_block"])->value('dist_id') : '';
        $this->appData['selected_state'] = $selected_state = ($selected_district) ? DistrictModel::where(['ogc_fid' => $selected_district])->value('state_id') : '';
        $this->appData['id'] = $id;
        return view('managerecords.claimant.edit', $this->appData);
    }

    public function claimantList(Request $request) {

        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $posted_data = $request->all();
        //$this->pre($posted_data);
        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];
        $selected_claimants = isset($posted_data['claimant']) && !empty($posted_data['claimant']) ? implode(',', $posted_data['claimant']) : '';
        $selected_moholla = isset($posted_data['moholla']) && !empty($posted_data['moholla']) ? implode(',', $posted_data['moholla']) : '';
        $this->appData['selected_claimants'] = $selected_claimants;
        $this->appData['selected_moholla'] = $selected_moholla;
        $moholla_id_str = isset($posted_data['moholla']) && !empty($posted_data['moholla']) ? "'" . implode("','", $posted_data['moholla']) . "'" : '';
        $cond = ' 1 = 1';
        if (isset($selected_claimants) && !empty($selected_claimants)) {
            $claimant_id_str = $selected_claimants;
        } else if (isset($posted_data['village']) && !empty($posted_data['village'])) {
            $village_id = $posted_data['village'];
            if (!$moholla_id_str) {
                $cond.= " and claimant.village_id_master = '$village_id' ";
            } else {
                $cond.= " and claimant.village_id_master = '$village_id' and claimant.mohallah_id_master in ($moholla_id_str)";
            }
            // $claimants = ClaimantModel::selectRaw("array_to_string(array_agg(id), ',') as id_str")->where(['village_id' => "$village_id"])->get()->toArray();
            $claimants = ClaimantModel::selectRaw("array_to_string(array_agg(id), ',') as id_str")->whereRaw($cond)->get()->toArray();

            $claimant_id_str = isset($claimants[0]['id_str']) ? $claimants[0]['id_str'] : '';
        }
        if (isset($claimant_id_str) && $claimant_id_str) {
            $results = ClaimantModel::join('t_village_master', 't_village_master.vil_id', '=', 'claimant.village_id_master')
			->whereRaw(" t_village_master.rtp_status is true ")->whereRaw("id in ($claimant_id_str)")->orderBy('id', 'desc')->paginate(25);
        } else {
           // $results = ClaimantModel::orderBy('id', 'desc')->paginate(25);
			$results = ClaimantModel::join('t_village_master', 't_village_master.vil_id', '=', 'claimant.village_id_master')
			->whereRaw(" t_village_master.rtp_status is true ")->orderBy('id', 'desc')->paginate(25);
        }
        // $results = ClaimantModel::orderBy('id', 'desc')->paginate(25);
	//	t($results);exit;
        $results->appends($posted_data);
        $links = $results->render();
        $results = $results->toArray();
        $claimants = isset($results['data']) ? $results['data'] : '';
        // $this->pre($claimants);
        $this->appData['claimants'] = $claimants;
        $this->appData['links'] = $links;
        $this->appData['search'] = isset($posted_data['search']) && $posted_data['search'] == 1 ? $posted_data['search'] : '';
        return view('managerecords.claimant.list', $this->appData);
    }

    public function processData(Request $request) {
        $posted_data = $request->all();
        $posted_claimant_data = isset($posted_data['claimant']) ? $posted_data['claimant'] : '';
        $posted_claimant_data['age'] = isset($posted_claimant_data['age']) && !empty($posted_claimant_data['age']) ? $posted_claimant_data['age'] : 0;
        // t($posted_claimant_data);exit;
        $id = $request->get('id');
        if ($id) {
            $res = ClaimantModel::select('is_old')->where(['id' => $id])->get()->toArray();
            $mode = 'edit';
            $old_id = ClaimantModel::where(['id' => $id])->value('old_id');
        }else {
            $mode = 'add';
            $old_id = '';
        }
        if (isset($res[0]['is_old']) && $res[0]['is_old']) {
            $is_old_value = true;
        } else {
            $is_old_value = false;
        }
        $messages = [
            'claimant.name.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The name field is required!</strong></div>',
            'claimant.old_id.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The Claimant ID field is required!</strong></div>',
            'claimant.old_id.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The Claimant ID field should be only numbers!</strong></div>',
            'claimant.old_id.validate_old_claimant' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The Claimant ID already exists in database!</strong></div>',
            'claimant.stofd.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The ST/OFD field is required!</strong></div>',
            'claimant.age.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Age should be numeric value!</strong></div>',
            'state.validate_state' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>You are not authorized to edit claimants for this state</strong></div>',
        ];
        $rules = [
            'claimant.name' => 'required',
            'claimant.stofd' => 'required',
            'claimant.old_id' => 'required|numeric|validate_old_claimant:'.$mode.','.$old_id,
            'claimant.age' => 'integer',
            'state' => 'validate_state'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        // $messages = $validator->errors();
        // $village_id = isset($posted_claimant_data['village_id']) ? $posted_claimant_data['village_id'] : '';
        $village_id = isset($posted_claimant_data['village_id_master']) ? $posted_claimant_data['village_id_master'] : '';
        $village_ogc_fid = ($village_id) ? VillageModel::where(['vil_id' => "$village_id"])->value('ogc_fid') : 0;

        //  $residence_village_id = isset($posted_claimant_data['claimant_resi_village_id']) ? $posted_claimant_data['claimant_resi_village_id'] : '';
        $residence_village_id = isset($posted_claimant_data['claimant_resi_master_village_id']) ? $posted_claimant_data['claimant_resi_master_village_id'] : '';
        // $resi_village_old_id = ($residence_village_id) ? VillageModel::where(['vil_id' => "$village_id"])->value('code2011') : '';
        $resi_village_old_id = ($residence_village_id) ? VillageModel::where(['vil_id' => "$village_id"])->value('census2011') : '';
        if ($id) {
            if ($validator->fails()) {
                return redirect('claimant/update/' . $id)
                                ->withErrors($validator, 'claimant')
                                ->withInput();
            }
            $data_to_update = $posted_claimant_data;
            $data_to_update['village_ogc_id'] = $village_ogc_fid;
            $data_to_update['claimant_resi_village_old_id'] = $resi_village_old_id;
            $data_to_update['is_old'] = $is_old_value;
            $data_to_update['updated_at'] = Carbon::now();
            $upadte_data = ClaimantModel::where(['id' => $id])->update($data_to_update);
            $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Claimant updated successfully!</strong></div>';
            return redirect('claimant/update/' . $id)->with('message', $msg);
        } else {
            if ($validator->fails()) {
                return redirect('claimant/add')
                                ->withErrors($validator, 'claimant')
                                ->withInput();
            }
            $data_to_insert = $posted_claimant_data;
            $data_to_insert['village_ogc_id'] = $village_ogc_fid;
            $data_to_insert['claimant_resi_village_old_id'] = $resi_village_old_id;
            $data_to_insert['is_old'] = $is_old_value;
            $data_to_insert['created_at'] = Carbon::now();
            //t($data_to_insert);exit;
            // $data_to_insert['updated_at'] = Carbon::now();
            $insert_id = ClaimantModel::insertGetId($data_to_insert);
            if ($insert_id) {
                $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Claimant added successfully!</strong></div>';
                return redirect('claimant/list')->with('message', $msg);
            }
        }
    }

}
