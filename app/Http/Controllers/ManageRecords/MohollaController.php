<?php

namespace App\Http\Controllers\ManageRecords;

use App\Http\Controllers\Controller;
use App\Models\Report\ClaimantModel;
use App\Models\Report\BlockModel;
use App\Models\Report\VillageModel;
use App\Models\Report\DistrictModel;
use App\Models\Report\MohollaModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use DB;

class MohollaController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
        $this->appData['section'] = 'moholla';
        $this->appData['include_script_view'][] = 'managerecords.common_scripts';
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        $this->middleware('auth');
    }

    public function add(Request $request) {

        $state = $request->old('moholla.state_id');
        $district = $request->old('moholla.dist_id');
        $block = $request->old('moholla.block_id');
        $village_id = $request->old('moholla.village_master_id');
        //  $claimnts = $request->old('family.claimant_id');
        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $this->appData['selected_state'] = isset($state) ? $state : $this->appData['logged_in_user_boudary_id'];
        $this->appData['selected_district'] = $district;
        $this->appData['selected_block'] = $block;
        $this->appData['selected_villages'] = $village_id;
        //$this->appData['selected_claimants'] = $claimnts;
        return view('managerecords.moholla.add', $this->appData);
    }

    public function edit($id) {
        if (!$id) {
            return 'moholla id missing';
        }
        $moholla_data = MohollaModel::where(['ogc_fid' => $id])->get()->toArray();
        $this->appData['moholla_data'] = $moholla_data = isset($moholla_data[0]) ? $moholla_data[0] : '';
        $this->appData['selected_block'] = $selected_block = isset($moholla_data['block_id']) ? $moholla_data['block_id'] : '';
        $this->appData['selected_district'] = $selected_district = BlockModel::where(['ogc_fid' => "$selected_block"])->value('dist_id');
        $this->appData['selected_state'] = $selected_state = DistrictModel::where(['ogc_fid' => $selected_district])->value('state_id');
        $this->appData['selected_villages'] = $selected_villages = isset($moholla_data['village_master_id']) ? $moholla_data['village_master_id'] : '';
        $this->appData['id'] = $id;
        return view('managerecords.moholla.edit', $this->appData);
    }

    public function mohollaList(Request $request) {

        $this->appData['appData']['metaTitle'] = $this->metaTitle;

        $posted_data = $request->all();
//        if ($posted_data) {
//            t($posted_data);
//            exit;
//        }
        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];
        $selected_villages = isset($posted_data['village']) ? $posted_data['village'] : '';
        $this->appData['selected_villages'] = $selected_villages ? implode(',', array_filter($selected_villages)) : '';
        if ($selected_villages) {
            foreach ($selected_villages as &$value) {
                $value = "'" . trim($value) . "'";
            }
            $selected_villages = implode(', ', $selected_villages);
        }
        if (isset($selected_villages) && !empty($selected_villages)) {
            $village_id_str = $selected_villages;
        }
        $block_id = isset($posted_data['block']) ? $posted_data['block'] : '';
        if ($block_id) {
            if (isset($village_id_str) && $village_id_str) {
                $results = MohollaModel::where(['block_id' => $block_id, 'is_rtp' => true])->whereRaw("village_master_id in ($village_id_str)")->orderBy('ogc_fid', 'desc')->paginate(25);
            } else {
                $results = MohollaModel::where(['block_id' => $block_id, 'is_rtp' => true])->orderBy('ogc_fid', 'desc')->paginate(25);
            }
        } else {
            $results = MohollaModel::where(['is_rtp' => true])->orderBy('ogc_fid', 'desc')->paginate(25);
        }
        // $results = ClaimantModel::orderBy('id', 'desc')->paginate(25);

        $results->appends($posted_data);
        $links = $results->render();
        $results = $results->toArray();
        $mohollas = isset($results['data']) ? $results['data'] : '';
        //$this->pre($mohollas);exit;
        $this->appData['mohollas'] = $mohollas;
        $this->appData['links'] = $links;
        $this->appData['search'] = isset($posted_data['search']) && $posted_data['search'] == 1 ? $posted_data['search'] : '';
        return view('managerecords.moholla.list', $this->appData);
    }

    public function processData(Request $request) {
        $posted_data = $request->all();
//        t($posted_data, 1);
//        exit;
        $posted_moholla_data = isset($posted_data['moholla']) ? $posted_data['moholla'] : '';
        $id = $request->get('id');

        $messages = [
//            'moholla.code.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The code field is required!</strong></div>',
            //'moholla.census2011.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Mahalla code must be number value!</strong></div>',
            'moholla.pincode.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Pincode must be number value!</strong></div>',
            //'moholla.status.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Status must be number value!</strong></div>',
            //'moholla.status.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Enable should be checked!</strong></div>',
            'moholla.moholla_name.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The name field is required!</strong></div>',
            'moholla.moholla_name.string' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The name field must contain only letters!</strong></div>',
            'moholla.state_id.validate_state' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>You are not authorized to edit mohollas for this state</strong></div>',
        ];
        $rules = [
            // 'moholla.census2011' => 'required|integer',
            'moholla.pincode' => 'integer',
            //'moholla.status' => 'required',
            'moholla.moholla_name' => 'required|string',
            'moholla.state_id' => 'validate_state'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        // $messages = $validator->errors();
        //$this->pre($res);
        $block_id = isset($posted_moholla_data['block_id']) ? $posted_moholla_data['block_id'] : '';
        $village_id = isset($posted_moholla_data['village_master_id']) ? $posted_moholla_data['village_master_id'] : '';
        $village_ogc_id = VillageModel::where(['vil_id' => $village_id])->value('ogc_fid');

        //$old_block_id = ($block_id) ? BlockModel::where(['id' => $block_id])->value('old_id') : '';
        if ($id) {
            $rtp_status = MohollaModel::where(['ogc_fid' => $id])->value('rtp_status');
            $posted_status = isset($posted_moholla_data['rtp_status']) ? $posted_moholla_data['rtp_status'] : '';
            if ($validator->fails()) {
                return redirect('moholla/update/' . $id)
                                ->withErrors($validator, 'moholla')
                                ->withInput();
            }
            $data_to_update = $posted_moholla_data;
            if ($rtp_status == true && !$posted_status) {
                $data_to_update['rtp_status'] = FALSE;
            }
            if (isset($posted_moholla_data['rtp_status'])) {
                $data_to_update['rtp_status'] = TRUE;
            } else {
                $data_to_update['rtp_status'] = FALSE;
            }
            $data_to_update['village_ogc_fid'] = $village_ogc_id;
            $data_to_update['updated_at'] = Carbon::now();
            $upadte_data = MohollaModel::where(['ogc_fid' => $id])->update($data_to_update);
            $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Mahalla updated successfully!</strong></div>';
            return redirect('moholla/update/' . $id)->with('message', $msg);
        } else {
            if ($validator->fails()) {
                return redirect('moholla/add')
                                ->withErrors($validator, 'moholla')
                                ->withInput($posted_data);
            }
            $data_to_insert = $posted_moholla_data;
            if (isset($posted_moholla_data['rtp_status'])) {
                $data_to_insert['rtp_status'] = TRUE;
            } else {
                $data_to_insert['rtp_status'] = FALSE;
            }
            $data_to_insert['village_ogc_fid'] = $village_ogc_id;
            $data_to_insert['created_at'] = Carbon::now();

            $census_2011 = VillageModel::where(['vil_id' => $village_id])->value('census2011');
            $census_2011_max = MohollaModel::selectRaw(" max(census2011) as max_census")->where(['village_master_id' => $village_id])->get()->toArray();

            $census_2011_max = isset($census_2011_max[0]['max_census']) ? $census_2011_max[0]['max_census'] : '';
            if ($census_2011_max) {
                $data_to_insert['census2011'] = (int) $census_2011_max + 1;
            } else {
                $data_to_insert['census2011'] = (int) $census_2011 . '1';
            }

            $insert_id = MohollaModel::insert($data_to_insert);
            if ($insert_id) {
                $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Mahalla added successfully!</strong></div>';
                return redirect('moholla/list')->with('message', $msg);
            }
        }
    }

}
