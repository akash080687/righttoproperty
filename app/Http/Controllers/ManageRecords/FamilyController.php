<?php

namespace App\Http\Controllers\ManageRecords;

use App\Http\Controllers\Controller;
use App\Models\ManageRecords\FamilyModel;
use App\Models\Report\ClaimantModel;
use App\Models\Report\BlockModel;
use App\Models\Report\VillageModel;
use App\Models\Report\DistrictModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use DB;

class FamilyController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
        $this->appData['section'] = 'family';
        $this->appData['include_script_view'][] = 'managerecords.common_scripts';
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        $this->middleware('auth');
    }

    public function add(Request $request) {
        //echo 'ssfsaf';
        // t($request->flash());
        $request->old('state');
        $state = $request->old('state');
        $district = $request->old('district');
        $block = $request->old('block');
        $village = $request->old('village');
        $claimnts = $request->old('family.claimant_id');
        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $this->appData['selected_state'] = isset($state) ? $state : $this->appData['logged_in_user_boudary_id'];
        $this->appData['selected_district'] = $district;
        $this->appData['selected_block'] = $block;
        $this->appData['selected_villages'] = $village;
        $this->appData['selected_claimants'] = $claimnts;

        return view('managerecords.family.add', $this->appData);
    }

    public function edit($id) {
        if (!$id) {
            return 'family id missing';
        }
        $this->appData['appData']['metaTitle'] = $this->metaTitle;

        $family_data = FamilyModel::where(['id' => $id])->get()->toArray();
        //$this->pre($family_data);
        if (empty($family_data)) {
            return redirect('family/add');
        }
        $claimant_id = isset($family_data[0]['claimant_id']) ? $family_data[0]['claimant_id'] : '';
        $this->appData['family_data'] = isset($family_data[0]) ? $family_data[0] : '';
        $this->appData['selected_villages'] = $selected_village = ClaimantModel::where(['id' => $claimant_id])->value('village_id_master');
        $this->appData['selected_block'] = $selected_block = VillageModel::where(['vil_id' => "$selected_village"])->value('block_id');
        $this->appData['selected_district'] = $selected_district = BlockModel::where(['ogc_fid' => $selected_block])->value('dist_id');
        $this->appData['selected_state'] = $selected_state = DistrictModel::where(['ogc_fid' => $selected_district])->value('state_id');
        $this->appData['selected_claimants'] = $selected_claims = isset($family_data[0]['claimant_id']) ? $family_data[0]['claimant_id'] : '';
        $this->appData['id'] = $id;
        return view('managerecords.family.edit', $this->appData);
    }

    public function familyList(Request $request) {

        $this->appData['appData']['metaTitle'] = $this->metaTitle;

        $posted_data = $request->all();
        //$this->pre($posted_data);
        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];
        $selected_claimants = isset($posted_data['claimant']) ? implode(',', array_filter($posted_data['claimant'])) : '';
        $this->appData['selected_claimants'] = $selected_claimants;
        if (isset($selected_claimants) && !empty($selected_claimants)) {
            $claimant_id_str = $selected_claimants;
        } else if (isset($posted_data['village']) && !empty($posted_data['village'])) {
            $village_id = $posted_data['village'];
            $claimants = ClaimantModel::selectRaw("array_to_string(array_agg(id), ',') as id_str")->where(['village_id' => "$village_id"])->get()->toArray();
            $claimant_id_str = isset($claimants[0]['id_str']) ? $claimants[0]['id_str'] : '';
        }
        if (isset($claimant_id_str) && $claimant_id_str) {
            $results = FamilyModel::whereRaw("claimant_id in ($claimant_id_str)")->orderBy('id', 'desc')->paginate(25);
        } else {
            $results = FamilyModel::orderBy('id', 'desc')->paginate(25);
        }
        // $results = FamilyModel::orderBy('id', 'desc')->paginate(25);
        $results->appends($posted_data);
        $links = $results->render();
        $results = $results->toArray();
        $family = isset($results['data']) ? $results['data'] : '';
        //$this->pre($family);
        $this->appData['family'] = $family;
        $this->appData['links'] = $links;
        $this->appData['search'] = isset($posted_data['search']) && $posted_data['search'] == 1 ? $posted_data['search'] : '';
        return view('managerecords.family.list', $this->appData);
    }

    public function processData(Request $request) {
        $posted_data = $request->all();
        $posted_family_data = isset($posted_data['family']) ? $posted_data['family'] : '';
        $id = $request->get('id');
        if ($id) {
            $res = FamilyModel::select('is_old')->where(['id' => $id])->get()->toArray();
        }
        if (isset($res[0]['is_old']) && $res[0]['is_old']) {
            $claimant_field = 'claimant_old';
            $is_old_value = true;
        } else {
            $claimant_field = 'claimant_id';
            $is_old_value = false;
        }
        $messages = [
            'family.name.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The name field is required!</strong></div>',
            'family.relation.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The relation field is required!</strong></div>',
            'family.age.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Age should be number value!(e.g 12,13,23 etc.)</strong></div>',
            'state.validate_state' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>You are not authorized to edit family for this state</strong></div>',
        ];

        $rules = [
            'family.name' => 'required',
            'family.relation' => 'required',
            'family.age' => 'integer',
            'state' => 'validate_state'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $claimant_id = isset($posted_family_data['claimant_id']) ? $posted_family_data['claimant_id'] : '';
        $old_claimant_id = ($claimant_id) ? ClaimantModel::where(['id' => $claimant_id])->value('old_id') : '';
        isset($posted_family_data['age']) && $posted_family_data['age'] == '' ? $posted_family_data['age'] = null : '';
        if ($id) {
            if ($validator->fails()) {
                return redirect('family/update/' . $id)
                                ->withErrors($validator, 'family')
                                ->withInput();
            }
            $data_to_update = $posted_family_data;
            $data_to_update['claimant_old'] = "$old_claimant_id";
            $data_to_update['is_old'] = $is_old_value;
            $data_to_update['updated_at'] = Carbon::now();
            $upadte_data = FamilyModel::where(['id' => $id])->update($data_to_update);
            $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Family udated successfully!</strong></div>';
            return redirect('family/update/' . $id)->with('message', $msg);
        } else {
            if ($validator->fails()) {
                return redirect('family/add')
                                ->withErrors($validator, 'family')
                                ->withInput();
            }

            $data_to_insert = $posted_family_data;
            $data_to_update['claimant_old'] = "$old_claimant_id";
            $data_to_insert['is_old'] = $is_old_value;
            $data_to_insert['created_at'] = Carbon::now();
            $data_to_insert['updated_at'] = Carbon::now();
            $insert_id = FamilyModel::insertGetId($data_to_insert);
            if ($insert_id) {
                $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Family added successfully!</strong></div>';
                return redirect('family/list')->with('message', $msg);
            }
        }
    }

}
