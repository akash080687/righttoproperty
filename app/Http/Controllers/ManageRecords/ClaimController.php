<?php

namespace App\Http\Controllers\ManageRecords;

use App\Http\Controllers\Controller;
use App\Models\Report\ClaimantModel;
use App\Models\Report\ClaimModel;
use App\Models\Report\BlockModel;
use App\Models\Report\VillageModel;
use App\Models\Report\DistrictModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use DB;

class ClaimController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
        $this->appData['section'] = 'claim';
        $this->appData['include_script_view'][] = 'managerecords.common_scripts';
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        // $this->appData['appData']['jsArr'][] = 'includes/js/bootstrap-datepicker/bootstrap-datepicker.js';
        $this->middleware('auth');
    }

    public function add(Request $request) {
        //echo 'ssfsaf';

        $request->old('state');
        $state = $request->old('state');
        $district = $request->old('district');
        $block = $request->old('block');
        $village = $request->old('claim.village_id_master');
        $mahalla = $request->old('claim.mohallah_id_master');
        //  $claimnts = $request->old('family.claimant_id');
        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $this->appData['selected_state'] = isset($state) ? $state : $this->appData['logged_in_user_boudary_id'];
        $this->appData['selected_district'] = $district;
        $this->appData['selected_block'] = $block;
        $this->appData['selected_villages'] = $village;
        $this->appData['selected_moholla'] = $mahalla;
        $this->appData['is_old'] = FALSE;
        return view('managerecords.claim.add', $this->appData);
    }

    public function edit($id) {
        if (!$id) {
            return 'claim id missing';
        }

        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $claim_data = ClaimModel::where(['id' => $id])->get()->toArray();
        $this->appData['claim_data'] = $claim_data = isset($claim_data[0]) ? $claim_data[0] : '';
        $this->appData['selected_claimants'] = $selected_claimants = isset($claim_data['claimant_id']) ? $claim_data['claimant_id'] : '';
        $this->appData['selected_villages'] = $selected_village = isset($claim_data['village_id_master']) ? $claim_data['village_id_master'] : '';
        $this->appData['selected_moholla'] = $selected_moholla = isset($claim_data['mohallah_id_master']) ? $claim_data['mohallah_id_master'] : '';
        $this->appData['selected_block'] = $selected_block = VillageModel::where(['vil_id' => $selected_village])->value('block_id');
        $this->appData['selected_district'] = $selected_district = BlockModel::where(['ogc_fid' => "$selected_block"])->value('dist_id');
        $this->appData['selected_state'] = $selected_state = DistrictModel::where(['ogc_fid' => $selected_district])->value('state_id');
        $this->appData['is_old'] = isset($claim_data['is_old']) ? boolval($claim_data['is_old']) : FALSE;
        //  echo $id;exit;
        $this->appData['id'] = $id;
        return view('managerecords.claim.edit', $this->appData);
    }

    public function claimList(Request $request) {

        $this->appData['appData']['metaTitle'] = $this->metaTitle;

        $posted_data = $request->all();
        //$this->pre($posted_data);exit;
        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];
        $selected_claimants = isset($posted_data['claimant']) ? implode(',', array_filter($posted_data['claimant'])) : '';
        $this->appData['selected_claimants'] = $selected_claimants;
        $claimant_id_str = isset($selected_claimants) && !empty($selected_claimants) ? $selected_claimants : '';
        $village_id = isset($posted_data['village']) ? $posted_data['village'] : '';

        $selected_moholla = isset($posted_data['moholla']) && !empty($posted_data['moholla']) ? implode(',', $posted_data['moholla']) : '';
        $this->appData['selected_moholla'] = $selected_moholla;
        $moholla_id_str = isset($posted_data['moholla']) && !empty($posted_data['moholla']) ? "'" . implode("','", $posted_data['moholla']) . "'" : '';
        $cond = ' 1 = 1';
        if ($village_id) {
            if (!$moholla_id_str) {
                $cond.= " and claim.village_id_master = '$village_id' ";
            } else {
                $cond.= " and claim.village_id_master = '$village_id' and claim.mohallah_id_master in ($moholla_id_str)";
            }
        }
        if (isset($claimant_id_str) && $claimant_id_str && $village_id) {
            // $results = ClaimModel::whereRaw("claimant_id in ($claimant_id_str)")->where(['village_id' => $village_id])->orderBy('number', 'asc')->paginate(50);
            $results = ClaimModel::join('t_village_master', 't_village_master.vil_id', '=', 'claim.village_id_master')
			->whereRaw(" t_village_master.rtp_status is true ")->whereRaw("claimant_id in ($claimant_id_str)")->whereRaw($cond)->orderBy('claim.id', 'desc')->paginate(50);
        } else if (!$claimant_id_str && $village_id) {
            // $results = ClaimModel::where(['village_id' => $village_id])->orderBy('number', 'asc')->paginate(50);
            $results = ClaimModel::join('t_village_master', 't_village_master.vil_id', '=', 'claim.village_id_master')
			->whereRaw(" t_village_master.rtp_status is true ")->whereRaw($cond)->orderBy('claim.id', 'desc')->paginate(50);
        } else {
            $results = ClaimModel::join('t_village_master', 't_village_master.vil_id', '=', 'claim.village_id_master')
			->whereRaw(" t_village_master.rtp_status is true ")->orderBy('claim.id', 'desc')->paginate(50);
        }
        // $results = ClaimantModel::orderBy('id', 'desc')->paginate(25);
        $results->appends($posted_data);
        $links = $results->render();
        $results = $results->toArray();
        $claims = isset($results['data']) ? $results['data'] : '';
        //$this->pre($claims);exit;
        $this->appData['claims'] = $claims;
        $this->appData['links'] = $links;
        $this->appData['search'] = isset($posted_data['search']) && $posted_data['search'] == 1 ? $posted_data['search'] : '';
        return view('managerecords.claim.list', $this->appData);
    }

    public function processData(Request $request) {
        $posted_data = $request->all();
        // $this->pre($posted_data);exit;
        $posted_claim_data = isset($posted_data['claim']) ? $posted_data['claim'] : '';
        $id = $request->get('id');
        if ($id) {
            $res = ClaimModel::select('is_old')->where(['id' => $id])->get()->toArray();
        }
        if (isset($res[0]['is_old']) && $res[0]['is_old']) {
            $claimant_field = 'claimant';
            $is_old_value = true;
        } else {
            $claimant_field = 'claimant_id';
            $is_old_value = false;
        }

        $rules = [
            'claim.number' => 'required|integer',
            'claim.' . $claimant_field => 'required|valid_claimant',
            'claim.tareaclaimed' => 'numeric',
            'claim.gsdecision' => 'alpha',
            'claim.areaapprovedgsha' => 'numeric',
            'claim.sdlcdecision' => 'alpha',
            'claim.areaapprovedsdlcha' => 'numeric',
            'claim.dlcdecision' => 'alpha',
            'claim.areaapproveddlcha' => 'numeric',
            'claim.appealfield' => 'alpha',
            'state' => 'validate_state'
        ];
        $messages = [
            'claim.number.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The Claim No. field is required!</strong></div>',
            'claim.number.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The Claim No. field should be number value!(e.g 1,2,3 etc)</strong></div>',
            'claim.' . $claimant_field . '.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The Claimant Id field is required!</strong></div>',
            'claim.' . $claimant_field . '.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The  Claimant Id should be number value!(e.g 1,2,3 etc)</strong></div>',
            'claim.' . $claimant_field . '.valid_claimant' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The Claimant Id is invalid!</strong></div>',
            'claim.tareaclaimed.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Total Area Claimed should be numeric value(e.g 0.1,1,1.2 etc)!</strong></div>',
            'claim.gsdecision.alpha' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>GS Decision should contain only letters!</strong></div>',
            'claim.areaapprovedgsha.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Approved GS should be numeric value(e.g 0.1,1,1.2 etc)</strong></div>',
            'claim.sdlcdecision.alpha' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>SDLC Decision should contain only letters!</strong></div>',
            'claim.areaapprovedsdlcha.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Approved GS  should be numeric value(e.g 0.1,1,1.2 etc)</strong></div>',
            'claim.dlcdecision.alpha' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>DLC Decision should contain only letters!</strong></div>',
            'claim.areaapproveddlcha.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strongArea Approved DLC  should be numeric value(e.g 0.1,1,1.2 etc)</strong></div>',
            'claim.dlcdecision.alpha' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>DLC Decision should contain only letters!</strong></div>',
            'state.validate_state' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>You are not authorized to edit claims for this state</strong></div>',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        // $messages = $validator->errors();
        // $village_id = $posted_claim_data['village_id'];
        if (isset($posted_claim_data['adhikar_patra_date']) && !empty($posted_claim_data['adhikar_patra_date'])) {
            $adhikar_patra_date = str_replace("/", "-", $posted_claim_data['adhikar_patra_date']);
            $adhikar_patra_date = new \DateTime($adhikar_patra_date);
            $adhikar_patra_date = $adhikar_patra_date->format('Y-m-d');
            $posted_claim_data['adhikar_patra_date'] = $adhikar_patra_date;
        }

        if ($id) {
            if ($validator->fails()) {
                return redirect('claim/update/' . $id)
                                ->withErrors($validator, 'claim')
                                ->withInput();
            }
            $data_to_update = $posted_claim_data;
            if (!isset($data_to_update['adhikar_patra_date']) || empty($data_to_insert['adhikar_patra_date'])) {
                //unset($data_to_update['adhikar_patra_date']); 
                $data_to_update['adhikar_patra_date'] = '0001-01-01 00:00:00';
            }
            $data_to_update['adhikar_patra_recd'] = isset($posted_claim_data['adhikar_patra_recd']) && $posted_claim_data['adhikar_patra_recd'] == 1 ? true : false;
            $data_to_update['is_old'] = $is_old_value;
            $data_to_update['updated_at'] = Carbon::now();

            $upadte_data = ClaimModel::where(['id' => $id])->update($data_to_update);
            $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Claim updated successfully!</strong></div>';
            return redirect('claim/update/' . $id)->with('message', $msg);
        } else {
            if ($validator->fails()) {
                return redirect('claim/add')
                                ->withErrors($validator, 'claim')
                                ->withInput();
            }
            $data_to_insert = $posted_claim_data;

            if (!isset($data_to_insert['adhikar_patra_date']) || empty($data_to_insert['adhikar_patra_date'])) {
                // unset($data_to_insert['adhikar_patra_date']);
                $data_to_insert['adhikar_patra_date'] = '0001-01-01 00:00:00';
            }
            $data_to_insert['adhikar_patra_recd'] = isset($posted_claim_data['adhikar_patra_recd']) && $posted_claim_data['adhikar_patra_recd'] == 1 ? true : false;
            $data_to_insert['is_old'] = $is_old_value;
            $data_to_insert['created_at'] = Carbon::now();
            $data_to_insert['updated_at'] = Carbon::now();
            // t($data_to_insert);exit;
            $insert_id = ClaimModel::insertGetId($data_to_insert);
            if ($insert_id) {
                $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Claim added successfully!</strong></div>';
                return redirect('claim/list')->with('message', $msg);
            }
        }
    }

}
