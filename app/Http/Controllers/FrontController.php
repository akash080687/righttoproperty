<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Report\VillageModel;
use App\Models\Report\ClaimantModel;
use App\Models\ManageRecords\FamilyModel;
use App\Models\Report\ClaimModel;
use App\Models\Report\PlotModel;
use Illuminate\Http\Request;
use Session;

class FrontController extends Controller {

    public function __construct() {
        parent:: __construct();
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
    }

    public function home(Request $request) {
        $this->appData['page'] = 'home';
        return view('front.welcome', $this->appData);
    }

    public function aboutUs() {
        $this->appData['page'] = 'about_us';
        return view('front.about_us', $this->appData);
    }

    public function supportUs() {
        $this->appData['page'] = 'support_us';
        return view('front.support_us', $this->appData);
    }

    public function contact() {
        $this->appData['page'] = 'contact';
        return view('front.contact', $this->appData);
    }

    public function viewPlots(Request $request)
    {
        $this->appData['include_script_view'][] = 'report.common_scripts';
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        $this->appData['page'] = 'view_plots';
        $posted_data = $request->all();
        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : '';
        return view('front.view_plots', $this->appData);
    }

    public function deleteSurveyData($village_id) {
        $this->appData['page'] = 'Test family';
        $claimants = ClaimantModel::selectRaw("array_to_string(array_agg(id), ',') as id_str")->where(['village_id_master' => "$village_id"])->get()->toArray();
        $claims = ClaimModel::selectRaw("array_to_string(array_agg(id), ',') as id_str")->where(['village_id_master' => "$village_id"])->get()->toArray();

        $claimant_id_str = isset($claimants[0]['id_str']) ? $claimants[0]['id_str'] : '';
        $claim_id_str = isset($claims[0]['id_str']) ? $claims[0]['id_str'] : '';
        if (!empty($claimant_id_str))
            $deletefamily = FamilyModel::whereRaw("claimant_id in ($claimant_id_str)")->delete();

        $deleteclaimant = ClaimantModel::where(['village_id_master' => "$village_id"])->delete();

        if (!empty($claim_id_str))
            $deleteplot = PlotModel::whereRaw("claim_id in ($claim_id_str)")->delete();
        $deleteclaim = ClaimModel::where(['village_id_master' => "$village_id"])->delete();
        $update_village_rtp_status = VillageModel::where(['vil_id' => $village_id])->update(['rtp_status' => FALSE]);
        $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Survey Data Deleted Successfully!</strong></div>';
        return redirect('village/list')->with('message', $msg);
//        $family = FamilyModel::whereRaw("claimant_id in ($claimant_id_str)")->get()->toArray();
//        $this->pre($family);
    }

}
