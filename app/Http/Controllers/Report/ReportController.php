<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\ManageRecords\FamilyModel;
use App\Models\Report\ClaimantModel;
use App\Models\Report\ClaimModel;
use App\Models\Report\PlotModel;
use App\Models\Report\BlockModel;
use App\Models\Report\VillageModel;
use App\Models\Report\MohollaModel;
use App\Models\Report\DistrictModel;
use App\Models\Report\StateModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Hash;

class ReportController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
        $this->appData['section'] = 'villge-claims-plot';
        $this->appData['include_script_view'][] = 'report.common_scripts';
        $this->appData['appData']['jsArr'][] = 'includes/js/jquery.validate.js';
        $this->middleware('auth');
    }

    public function villageClaimsPlot(Request $request) {

        //echo Hash::make('admin@123');
        $this->appData['appData']['metaTitle'] = 'Village Claims Plot';
        $this->appData['appData']['cssArr'][] = 'includes/css/custom/custom.css';
        $posted_data = $request->all();

        $this->appData['selected_state'] = isset($posted_data['state']) ? $posted_data['state'] : $this->appData['logged_in_user_boudary_id'];
        $selected_claimants = isset($posted_data['claimant']) ? implode(',', array_filter($posted_data['claimant'])) : '';
        $selected_claims = isset($posted_data['claim']) ? implode(',', $posted_data['claim']) : '';

        $this->appData['selected_claimants'] = $selected_claimants;
        $this->appData['selected_claims'] = $selected_claims;
        $village_id = isset($posted_data['village']) ? $posted_data['village'] : '';
        $block_id = isset($posted_data['block']) ? $posted_data['block'] : '';
        $district_id = isset($posted_data['district']) ? $posted_data['district'] : '';
        $state_id = isset($posted_data['state']) ? $posted_data['state'] : '';
//        $moholla_id_str = isset($posted_data['moholla']) && !empty($posted_data['moholla']) ? "'" . implode("','", $posted_data['moholla']) . "'" : '';
//        $this->appData['selected_moholla'] = isset($posted_data['moholla']) && !empty($posted_data['moholla']) ? implode(",", $posted_data['moholla']) : '';
        $moholla_id_str = isset($posted_data['moholla']) && !empty($posted_data['moholla']) ? "'".$posted_data['moholla']."'" : '';
        $this->appData['selected_moholla'] = $selected_moholla = isset($posted_data['moholla']) && !empty($posted_data['moholla']) ? $posted_data['moholla'] : '';
        $this->appData['claimstat'] = $claim_status = isset($posted_data['claimstat']) && $posted_data['claimstat'] != -1 ? $posted_data['claimstat'] : '';
        //$this->$claim_status = $claim_status;
        $this->appData['village_name'] = ($village_id) ? VillageModel::where(['vil_id' => $village_id])->value('village_name') : '';
        $this->appData['moholla_name'] = ($selected_moholla) ? MohollaModel::where(['ogc_fid' => $selected_moholla])->value('moholla_name') : ''; 
        $this->appData['block_name'] = ($block_id) ? BlockModel::where(['ogc_fid' => $block_id])->value('block_name') : '';
        $this->appData['district_name'] = ($district_id) ? DistrictModel::where(['ogc_fid' => $district_id])->value('district_name') : '';
        $this->appData['state_name'] = ($state_id) ? StateModel::where(['stt_state_id' => $state_id])->value('stt_state_name') : '';
//        $this->appData['claim_status'] = $claim_status; //($claim_status) ? StateModel::where(['stt_state_id' => $claim_status])->value('stt_state_name') : '';
        $this->appData['claim_status'] =  $this->claim_status = $claim_status;
        $cond = ' 1 = 1';
        if ($village_id) {
            if (!$moholla_id_str) {
                $cond.= " and village_id_master = '$village_id' ";
            } else {
               // $cond.= " and village_id_master = '$village_id' and mohallah_id_master in ($moholla_id_str)";
                $cond.= " and village_id_master = '$village_id' and mohallah_id_master in ($moholla_id_str)";
            }
            //$total_claims = ClaimModel::selectRaw('count(*) as total_claims')->where(['village_id_master' => "$village_id"])->get()->toArray();
            $total_claims = ClaimModel::selectRaw('count(*) as total_claims')->whereRaw($cond)->get()->toArray();
            $this->appData['total_claims'] = isset($total_claims[0]['total_claims']) ? $total_claims[0]['total_claims'] : '';
            $all_results = ClaimantModel::whereRaw($cond)->orderBy('id', 'ASC')->with(['claims' => function($query) {
                            //t($query,1);exit;
                            if ($this->claim_status) {
                                $query->where(['claim_status' => $this->claim_status])->with('plots');
                            } else {
                                $query->with('plots');
                            }
                        }])->get()->toArray();
                   // t($all_results,1);exit;
                    if ($selected_claimants) {
                        $results = ClaimantModel::whereRaw($cond)->whereRaw("id in ($selected_claimants)")->orderBy('mohallah_id_master', 'DESC')->orderBy('id', 'ASC')->with(['claims' => function($query) {

                                        if ($this->claim_status) {
                                            $query->where(['claim_status' => $this->claim_status])->with('plots');
                                        } else {
                                            $query->with('plots');
                                        }
                                    }])->get()->toArray();
                            } else {
                                $results = ClaimantModel::whereRaw($cond)->orderBy('mohallah_id_master', 'DESC')->orderBy('id', 'ASC')->with(['claims' => function($query) {
                                                //t($query,1);exit;
                                                //$query->where(['claim_status' => 'P'])->with('plots');
                                                if ($this->claim_status) {
                                                    $query->where(['claim_status' => $this->claim_status])->with('plots');
                                                } else {
                                                    $query->with('plots');
                                                }
                                            }])->get()->toArray();
                                    }
//                                    t($results);
//                                    exit;
                                    $claimnt_id_str = '';
                                   // t($all_results);exit;
                                    if ($all_results) {
                                        $total_area_claimed = 0;
                                        $total_claims_from_same_village = 0;
                                        $total_claims_from_other_village = 0;
                                        $total_plots = 0;
                                        foreach ($all_results as $key => $value) {
                                            $id = $value['id'];
                                            $residence_village_id = $value['claimant_resi_master_village_id'];
                                            $claim_village_id = $value['village_id_master'];
                                            $claims = isset($value['claims']) ? $value['claims'] : '';
                                            // $claimnt_id_str .= "'$id'".',';
                                            if ($claims) {
                                                foreach ($claims as $claim_key => $claim) {
                                                    $plots = $claim['plots'];
                                                    if ($plots) {
                                                        foreach ($plots as $k => $v) {
                                                            $total_area_claimed+=$v['areagpssurveyh'];
                                                            $total_plots++;
                                                        }
                                                    }
                                                    ($claim['village_id_master'] == $residence_village_id) ? $total_claims_from_same_village++ : $total_claims_from_other_village++;
                                                }
                                            }
                                        }
                                    }
                                    $this->appData['total_area_claimed'] = isset($total_area_claimed) ? $total_area_claimed : 0;
                                    $this->appData['total_plots'] = isset($total_plots) ? $total_plots : 0;
                                    $this->appData['total_claims_from_same_village'] = isset($total_claims_from_same_village) ? $total_claims_from_same_village : 0;
                                    $this->appData['total_claims_from_other_village'] = isset($total_claims_from_other_village) ? $total_claims_from_other_village : 0;
                                    //$results->appends($posted_data);
                                    //$links = $results->render();
                                    // $results = $results->toArray();
                                    $claimants = isset($results) ? $results : '';

                                    if ($selected_claims) {
                                        foreach ($claimants as $key => $value) {
                                            $claim_arr = [];
                                            $claims = $value['claims'];
                                            foreach ($claims as $claim_key => $claim_val) {
                                                $claim_id = $claim_val['id'];
                                                if (in_array($claim_id, $posted_data['claim'])) {
                                                    array_push($claim_arr, $claim_val);
                                                }
                                            }
                                            $claimants[$key]['claims'] = $claim_arr;
                                        }
                                    }
                                    //   echo $claimnt_id_str;
                                   //  t($claimants);exit;
                                    $this->appData['claimants'] = $claimants;
                                    $this->appData['links'] = '';
                                    $this->appData['search'] = isset($posted_data['search']) && $posted_data['search'] == 1 ? $posted_data['search'] : '';
                                }
                                return view('report.village_cliams_plot', $this->appData);
                            }

                            public function claims() {

                                $this->appData['appData']['metaTitle'] = $this->metaTitle;
                                $this->appData['appData']['jsArr'] = array(
                                );
                                //return view('managerecords.family.add', $this->appData);
                            }

                            public function plot() {

                                $this->appData['appData']['metaTitle'] = $this->metaTitle;
                                $this->appData['appData']['jsArr'] = array(
                                );
                                //return view('managerecords.family.add', $this->appData);
                            }

                            public function villagePlots() {

                                $this->appData['appData']['metaTitle'] = $this->metaTitle;
                                $this->appData['appData']['jsArr'] = array(
                                );
                                return view('report.village_plot', $this->appData);
                            }

                        }
                        