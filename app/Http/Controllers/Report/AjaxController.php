<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\Report\StateModel;
use App\Models\Report\DistrictModel;
use App\Models\Report\BlockModel;
use App\Models\Report\VillageModel;
use App\Models\Report\PlotModel;
use App\Models\Report\ClaimantModel;
use App\Models\Report\ClaimModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use DB;


class AjaxController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
    }

    public function state() {
        $states = StateModel::select('stt_state_id as Id', 'stt_state_name as Name')->where(['stt_fl_archive' => 'N', 'is_rtp' => TRUE])->orderBy('stt_state_name', 'ASC')->get()->toArray();
        echo json_encode($states);
    }

    public function district(Request $request) {
        $state_id = $request->get('state_id');
        if ($state_id) {
            // $districts = DistrictModel::select('id as Id', 'name as Name')->where(['state_id' => "$state_id"])->orderBy('name', 'ASC')->get()->toArray();
            $districts = DistrictModel::select('ogc_fid as Id', 'district_name as Name')->where(['state_id' => "$state_id", 'is_rtp' => TRUE])
                            ->whereRaw(" rtp_status is not FALSE ")->orderBy('district_name', 'ASC')->get()->toArray();
            echo json_encode($districts);
        }
    }

    public function districtMaster(Request $request) {
        $state_id = $request->get('state_id');
        if ($state_id) {
            // $districts = DistrictModel::select('id as Id', 'name as Name')->where(['state_id' => "$state_id"])->orderBy('name', 'ASC')->get()->toArray();
            $districts = DistrictModel::select('ogc_fid as Id', 'district_name as Name')->where(['state_id' => "$state_id"])->whereRaw("status is not false ")
                            ->whereRaw(" rtp_status is not FALSE ")->orderBy('district_name', 'ASC')->get()->toArray();
            echo json_encode($districts);
        }
    }

    public function block(Request $request) {
        $dist_id = $request->get('dist_id');
        //t($dist);
        if ($dist_id) {
            //$blocks = DistrictModel::find($dist_id)->blocks()->select('ogc_fid as Id', 'block_name as Name')->orderBy('block_name', 'ASC')->get()->toArray();
            $blocks = DistrictModel::find($dist_id)->blocks()->select('ogc_fid as Id', 'block_name as Name')->orderBy('block_name', 'ASC')->get()->toArray();
            // t($blocks);
            echo json_encode($blocks);
        }
    }

    public function blockMaster(Request $request) {
        $dist_id = $request->get('dist_id');
        //t($dist);
        if ($dist_id) {
            //$blocks = DistrictModel::find($dist_id)->blocks()->select('ogc_fid as Id', 'block_name as Name')->orderBy('block_name', 'ASC')->get()->toArray();
            $blocks = DistrictModel::find($dist_id)->blocksMaster()->select('ogc_fid as Id', 'block_name as Name')->orderBy('block_name', 'ASC')->get()->toArray();
            // t($blocks);
            echo json_encode($blocks);
        }
    }

    public function village(Request $request) {
        $block_id = $request->get('block_id');
        $filter = $request->get('filter');
        $from = $request->get('from');
        $villages_for_rtp = $request->get('villages_for_rtp');
        //  $res = DB::select(DB::raw("select distinct village_ogc_id from claimant"));
        $res = ClaimantModel::selectRaw("array_to_string(array_agg( distinct QUOTE_LITERAL(village_id_master)), ',') as id_str")->get()->toArray();

        $id_str = isset($res[0]['id_str']) && $res[0]['id_str'] ? $res[0]['id_str'] : '1';


        if ($block_id) {

            if ($villages_for_rtp) {
                $villages = BlockModel::find($block_id)->villagesForRtp()->select('vil_id as Id', 'village_name as Name', 'census2011')
                                ->orderBy('village_name', 'ASC')->get()->toArray();
            } else {
                if (!$filter) {

//                $villages = BlockModel::find($block_id)->villages()->select('vil_id as Id', 'village_name as Name')
//                                ->whereRaw('status=1 and t_village_master.ogc_fid in( select distinct village_ogc_id from claimant)')
//                                ->orderBy('village_name', 'ASC')->get()->toArray();
//                t($villages);exit;

                    $villages = BlockModel::find($block_id)->villages()->select('vil_id as Id', 'village_name as Name', 'census2011')
                                    ->whereRaw("t_village_master.vil_id in($id_str)")
                                    ->orderBy('t_village_master.village_name', 'ASC')->get()->toArray();
                } else if ($filter == 'all') {
                    if ($from == 'import_page') {
                        $villages = BlockModel::find($block_id)->villagesForRtp()->select('vil_id as Id', 'village_name as Name', 'census2011')
                                        ->orderBy('village_name', 'ASC')->get()->toArray();
                    } else {
                        $villages = BlockModel::find($block_id)->villages()->select('vil_id as Id', 'village_name as Name', 'census2011')
                                        ->orderBy('village_name', 'ASC')->get()->toArray();
                    }
                }
            }
            echo json_encode($villages);
        }
    }

    public function villageMaster(Request $request) {
        $block_id = $request->get('block_id');
        if ($block_id) {
            $villages = BlockModel::find($block_id)->villagesMaster()->select('vil_id as Id', 'village_name as Name', 'census2011')
                            ->orderBy('t_village_master.village_name', 'ASC')->get()->toArray();
            echo json_encode($villages);
        }
    }

    public function getMohollaByVillageId(Request $request) {
        $village_id = $request->get('village_id');
        if ($village_id) {
            $village_ogc_id = VillageModel::where(['vil_id' => $village_id])->value('ogc_fid');
            $mohollas = VillageModel::find($village_ogc_id)->mohollas()->select('ogc_fid as Id', 'moholla_name as Name')
                            ->orderBy('moholla_name', 'ASC')->get()->toArray();
            echo json_encode($mohollas);
        }
    }

    public function getPlotsByVillageId(Request $request) {
        $village_id = $request->get('village_id');
        if ($village_id) {
            $plots = PlotModel::select('ogc_fid as Id')->where(['vil_id' => "$village_id"])->get()->toArray();
            echo json_encode($plots);
        }
    }

    public function getClaimantsByVillageId(Request $request) {
        $village_id = $request->get('village_id');
        if ($village_id) {
            $village_ogc_id = VillageModel::where(['vil_id' => $village_id])->value('ogc_fid');
            $claimants = VillageModel::find($village_ogc_id)->claimants()->select('id as Id', 'old_id as Name')
                            ->where(['village_id_master' => "$village_id"])->get()->toArray();
            echo json_encode($claimants);
        }
    }

    public function getClaimantsByMoholla(Request $request) {
        $moholla_id = $request->get('moholla_id');
        // t($moholla_id);
        if (is_array($moholla_id)) {
            $moholla_id_str = ($moholla_id) ? "'" . implode("','", $moholla_id) . "'" : '';
        } else {
            $moholla_id_str = "'" . $moholla_id . "'";
        }
        if ($moholla_id_str) {
            $claimants = ClaimantModel::whereRaw(" mohallah_id_master in ($moholla_id_str)")->select('id as Id', 'old_id as Name')->orderBy('id', 'ASC')->get()->toArray();
            echo json_encode($claimants);
        }
    }

    public function getClaimsByVillageId(Request $request) {
        $village_id = $request->get('village_id');
        $claimants = $request->get('claimant_id');
        //$this->pre($claimants);
        $moholla_id = $request->get('moholla_id');
        if (is_array($moholla_id)) {
            $moholla_id_str = ($moholla_id) ? "'" . implode("','", $moholla_id) . "'" : '';
        } else {
            $moholla_id_str = ($moholla_id) ? "'" . $moholla_id . "'" : '';
        }
        if (is_array($claimants)) {
            $claimant_str = ($claimants) ? implode(',', $claimants) : '';
        } else {
            $claimant_str = $claimants;
        }

        if (!$moholla_id_str) {
            if ($village_id && $claimant_str) {
//            $claims = ClaimModel::select('id as Id', 'number as Name')->where(['village_id' => "$village_id"])
//                            ->whereRaw("claimant_id in ($claimant_str)")->orderBy('id', 'asc')->get()->toArray();

                $claims = ClaimModel::select('id as Id', 'number as Name')->where(['village_id_master' => "$village_id"])
                                ->whereRaw("claimant_id in ($claimant_str)")->orderBy('id', 'asc')->get()->toArray();
            } else {
                //$claims = ClaimModel::select('id as Id', 'number as Name')->where(['village_id' => "$village_id"])->orderBy('id', 'asc')->get()->toArray();
                $claims = ClaimModel::select('id as Id', 'number as Name')->where(['village_id_master' => "$village_id"])->orderBy('id', 'asc')->get()->toArray();
            }
        } else {
            //echo $moholla_id_str;exit;
            if ($moholla_id_str && $claimant_str) {
                $claims = ClaimModel::select('id as Id', 'number as Name')->whereRaw("mohallah_id_master in ($moholla_id_str) ")->where(['village_id_master' => "$village_id"])
                                ->whereRaw("claimant_id in ($claimant_str)")->orderBy('id', 'asc')->get()->toArray();
            } else {
                $claims = ClaimModel::select('id as Id', 'number as Name')->whereRaw("mohallah_id_master in ($moholla_id_str) ")->where(['village_id_master' => "$village_id"])->orderBy('id', 'asc')->get()->toArray();
            }
        }
        echo json_encode($claims);
    }

    public function PlotGPSGeoJSON(Request $request, $id = NULL) {
        $ogc_fid = $id; //$request->get('id');
        $feature = 'plot';
        $attribute = 'gps'; //$request->get('feature');

        switch ($feature):
            case 'plot':
                if ($attribute == 'sat'):
                    PlotModel::select('ogc_fid as Id')->where(['vil_id' => "$village_id"])->get()->toArray();
                elseif ($attribute == 'gps'): {
                        $sql = "SELECT row_to_json(fc) as geojson FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
                    FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry)::json As geometry
                    , row_to_json((ogc_fid, plt_number)) As properties,areagpssurveyh as area_gps FROM plot As lg WHERE ogc_fid = $ogc_fid  ) As f )  As fc;";

                        $response = DB::select(DB::raw($sql));
//                    print_r($response[0]->geojson);
                        echo ($response[0]->geojson);
                    }
                endif;
                break;
        endswitch;

        //echo json_encode($claims);
    }

    public function PlotSATGeoJSON(Request $request, $id = NULL) {
        $ogc_fid = $id; //$request->get('id');
        $feature = 'plot';
        $attribute = 'gps'; //$request->get('feature');

        switch ($feature):
            case 'plot':
                if ($attribute == 'sat'):
                    PlotModel::select('ogc_fid as Id')->where(['vil_id' => "$village_id"])->get()->toArray();
                elseif ($attribute == 'gps'): {
                        $sql = "SELECT row_to_json(fc) as geojson FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
                    FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry_sat)::json As geometry
                    , row_to_json((ogc_fid, plt_number)) As properties,areagpssurveyh as area_gps FROM plot As lg WHERE ogc_fid = $ogc_fid  ) As f )  As fc;";

                        $response = DB::select(DB::raw($sql));
//                    print_r($response[0]->geojson);
                        echo ($response[0]->geojson);
                    }
                endif;
                break;
        endswitch;

        //echo json_encode($claims);
    }

    public function getGeoJSONofClaimedPlots($vil_id = NULL, $plot_id = NULL) {

        $claims_of_the_village = array();
        $plots_under_claim = $claims_of_the_village;


//        $sql = "SELECT row_to_json(fc) as geojson
//  FROM ( 
//  SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
//  FROM ( SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry)::json As geometry 
//  FROM plot As lg  where claim=6258  ) As f )  As fc";
        $sql = "SELECT row_to_json(fc) as geojson  FROM ( 
  SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
  FROM ( SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry)::json As geometry 
  FROM plot As lg  where claim_id = ANY((SELECT array_agg(id) FROM claim where village_id = '$vil_id')::int[]) AND lg.ogc_fid <> $plot_id   ) As f )  As fc";
        $response = DB::select(DB::raw($sql));
        echo ($response[0]->geojson);

        //echo $result[0]['geojson'];
    }

    public function getPlotOgcFidForVillagePlotReport(Request $request) {
        //echo 1213;exit;
        $request_data = $request->all();
        $response_plot_ogc_fids = [];
        $selected_claimants = isset($request_data['claimant']) && !empty($request_data['claimant']) ? implode(',', array_filter($request_data['claimant'])) : '';
        $selected_claims = isset($request_data['claim']) && !empty($request_data['claim']) ? implode(',', $request_data['claim']) : '';
        $village_id = isset($request_data['village']) && $request_data['village'] ? $request_data['village'] : '';
        $block_id = isset($request_data['block']) && $request_data['block'] ? $request_data['block'] : '';
        $district_id = isset($request_data['district']) && $request_data['district'] ? $request_data['district'] : '';
        $state_id = isset($request_data['state']) && $request_data['state'] ? $request_data['state'] : '';
        $moholla_id_str = isset($request_data['moholla']) && !empty($request_data['moholla']) ? "'" . $request_data['moholla'] . "'" : '';
        $selected_moholla = isset($request_data['moholla']) && !empty($request_data['moholla']) ? $request_data['moholla'] : '';
        $claim_status = isset($request_data['claimstat']) && $request_data['claimstat'] != -1 ? $request_data['claimstat'] : '';
        $this->claim_status = $claim_status;
        $cond = ' 1 = 1';
        if ($village_id) {
            if (!$moholla_id_str) {
                $cond.= " and village_id_master = '$village_id' ";
            } else {
                $cond.= " and village_id_master = '$village_id' and mohallah_id_master in ($moholla_id_str)";
            }
            if ($selected_claimants) {
                $results = ClaimantModel::whereRaw($cond)->whereRaw("id in ($selected_claimants)")->orderBy('mohallah_id_master', 'DESC')->orderBy('id', 'ASC')->with(['claims' => function($query) {
                                if ($this->claim_status) {
                                    $query->where(['claim_status' => $this->claim_status])->with('plots');
                                } else {
                                    $query->with('plots');
                                }
                            }])->get()->toArray();
                    } else {
                        $results = ClaimantModel::whereRaw($cond)->orderBy('mohallah_id_master', 'DESC')->orderBy('id', 'ASC')->with(['claims' => function($query) {
                                        //t($query,1);exit;
                                        //$query->where(['claim_status' => 'P'])->with('plots');
                                        if ($this->claim_status) {
                                            $query->where(['claim_status' => $this->claim_status])->with('plots');
                                        } else {
                                            $query->with('plots');
                                        }
                                    }])->get()->toArray();
                            }
                            // t($results);exit;
                            $claimants = isset($results) ? $results : '';
                            if ($selected_claims) { // total data filter by claims,
                                foreach ($claimants as $key => $value) {
                                    $claim_arr = [];
                                    $claims = $value['claims'];
                                    foreach ($claims as $claim_key => $claim_val) {
                                        $claim_id = $claim_val['id'];
                                        if (in_array($claim_id, $request_data['claim'])) {
                                            array_push($claim_arr, $claim_val);
                                        }
                                    }
                                    $claimants[$key]['claims'] = $claim_arr;
                                }
                            }
                            //   echo $claimnt_id_str;

                            if ($claimants) {
                                foreach ($claimants as $key => $value) {
                                    $claims = $value['claims'];
                                    foreach ($claims as $claim_key => $claim_val) {
                                        $plots = $claim_val['plots'];
                                        foreach ($plots as $plot_key => $plot_value) {
                                            // array_push($response_plot_ogc_fids, $plot_value['ogc_fid']);
                                            $response_plot_ogc_fids[] = $plot_value['ogc_fid'];
                                        }
                                    }
                                }
                            }
                            $plot_id_str = json_encode($response_plot_ogc_fids);
                            $plot_id_str = str_replace("[", "", $plot_id_str);
                            $plot_id_str = str_replace("]", "", $plot_id_str);
                            $ret_arr['plotIdList'] = $plot_id_str;
                            echo json_encode($ret_arr);
                            //  t($response_plot_ogc_fids);
                            //echo json_encode($response_plot_ogc_fids);
                        }
                    }

                    public function getCSRF() {
                        echo csrf_token();
                    }

                }
                