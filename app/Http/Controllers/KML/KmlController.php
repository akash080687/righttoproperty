<?php

namespace App\Http\Controllers\KML;

use App\Http\Controllers\Controller;
use App\Models\Report\VillageModel;
use App\Models\Report\DistrictModel;
use App\Models\Report\PlotModel;
use Validator;
use Illuminate\Http\Request;
use Auth;
use Session;
use Carbon\Carbon;
use DB;

class KmlController extends Controller {

    public $metaTitle = '';

    public function __construct() {
        parent:: __construct();
        $this->metaTitle = '';
        $this->appData['section'] = 'plot';
        $this->appData['include_script_view'] = 'managerecords.common_scripts';
    }

    public function plot($id, $download_file = true) {
//        echo $download_file;
//        $label = 'test';
        $plot = PlotModel::selectRaw('plt_number,ST_AsText(wkb_geometry) as wkb_geometry,ST_AsText(wkb_geometry_sat) as wkb_geometry_sat,ST_AsText(ST_Centroid(wkb_geometry)) as centroid')->where(['ogc_fid' => $id])->get()->toArray();
        $gps = ltrim($plot[0]['wkb_geometry'], 'MULTIPOLYGON(((');
        $gps = rtrim($gps, ')))');
        $gps = str_replace(',', '#', $gps);
        $gps = str_replace(' ', ',', $gps);
        $gps = str_replace('#', ' ', $gps);
       // print_r( $gps);
        
//        $sat = PlotModel::selectRaw('ST_AsText(wkb_geometry_sat) as wkb_geometry_sat')->where(['ogc_fid' => $id])->get()->toArray();
        $sat = ltrim($plot[0]['wkb_geometry_sat'], 'MULTIPOLYGON(((');
        $sat = rtrim($sat, ')))');
        
        $sat = str_replace(',', '#', $sat);
        $sat = str_replace(' ', ',', $sat);
        $sat = str_replace('#', ' ', $sat);
        
        $centre = ltrim($plot[0]['centroid'],'POINT(');
        $centre = rtrim($centre,')');
        $centre = str_replace(' ',',',$centre);
//        print_r($centre);        die;
        header('Content-type: text/plain; charset=UTF-8');
        header('Content-Disposition: attachment; filename="' . $id . '.kml"');        
        echo '<?xml version="1.0" encoding="UTF-8"?><kml xmlns="http://earth.google.com/kml/2.1">
    <Document>
        <Style id="gpsStyle">
            <PolyStyle>
                <outline>1</outline>
                
            </PolyStyle>
            <LineStyle>
                <color>ff0080ff</color>
            </LineStyle>
            <IconStyle>
                <scale>0</scale>
                <Icon/> 
            </IconStyle>
            <geomScale>2</geomScale>
        </Style>
        <Style id="satStyle">
            <PolyStyle>
                <outline>2</outline>
                <color>5014F0DC</color>
            </PolyStyle>
            <LineStyle>
                <color>5014F0DC</color>
            </LineStyle>
            <IconStyle>
                <scale>0</scale>
                <Icon/> 
            </IconStyle>
            <geomScale>2</geomScale>
        </Style>
        <Folder>
            <name>PLOTINFO</name>
            <Placemark>
                <name>'.$plot[0]['plt_number'].'</name>
                <styleUrl>#gpsStyle</styleUrl>
                <MultiGeometry>
                    <Polygon>
                        <extrude>0</extrude>
                        <altitudeMode>relativeToGround</altitudeMode>
                        <outerBoundaryIs>
                            <LinearRing>
                                <coordinates>
                                '.$gps.'
                                </coordinates>
                            </LinearRing>
                        </outerBoundaryIs>
                    </Polygon>
                    <Point>
                        <coordinates>'.$centre.',0</coordinates>
                    </Point>
                </MultiGeometry>
            </Placemark>
            <Placemark>                
                <styleUrl>#parcelStyle</styleUrl>
                <MultiGeometry>
                    <Polygon>
                        <extrude>0</extrude>
                        <altitudeMode>relativeToGround</altitudeMode>
                        <outerBoundaryIs>
                            <LinearRing>
                                <coordinates>'.$sat.'</coordinates>
                            </LinearRing>
                        </outerBoundaryIs>
                    </Polygon>                    
                </MultiGeometry>
            </Placemark>
        </Folder>
    </Document>
</kml>';
//        if ($download_file == 'true') {
//            $plot_details = PlotModel::selectRaw('ST_AsText(wkb_geometry) as wkb_geometry')->where(['ogc_fid' => $id])->get()->toArray();
//            $this->pre($plot_details);
//        } else {
//            echo 'response';
//        }
    }

    public function edit($id) {
        if (!$id) {
            return 'plot id missing';
        }
        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $this->appData['appData']['jsArr'] = array(
        );
        $plot_data = PlotModel::where(['ogc_fid' => $id])->get()->toArray();
        //$this->pre($plot_data);
        $this->appData['plot_data'] = $plot_data = isset($plot_data[0]) ? $plot_data[0] : '';
        $this->appData['selected_village'] = $selected_village = isset($plot_data['vil_id']) ? trim($plot_data['vil_id']) : '';
        $this->appData['selected_block'] = $selected_block = VillageModel::where(['vil_id' => "$selected_village"])->value('block_id');
        $this->appData['selected_district'] = $selected_district = BlockModel::where(['id' => $selected_block])->value('dist_id');
        $this->appData['selected_state'] = $selected_state = DistrictModel::where(['id' => $selected_district])->value('state_id');
        $this->appData['selected_claims'] = $selected_claims = isset($plot_data['claim_id']) ? $plot_data['claim_id'] : '';

        $this->appData['id'] = $id;
        return view('managerecords.plot.edit', $this->appData);
    }

    public function plotList(Request $request) {

        $this->appData['appData']['metaTitle'] = $this->metaTitle;
        $this->appData['appData']['jsArr'] = array(
        );
        $posted_data = $request->all();
        //$this->pre($posted_data);
        $selected_claimants = isset($posted_data['claimant']) ? implode(',', array_filter($posted_data['claimant'])) : '';
        $selected_claims = isset($posted_data['claim']) ? implode(',', $posted_data['claim']) : '';
        $this->appData['selected_claimants'] = $selected_claimants;
        $this->appData['selected_claims'] = $selected_claims;
        if (empty($selected_claims) && !empty($selected_claimants)) {
            $village_id = $posted_data['village'];
            $res = ClaimModel::selectRaw("array_to_string(array_agg(id), ',') as id_str")->where(['village_id' => "$village_id"])
                            ->whereRaw("claimant_id in ($selected_claimants)")->get()->toArray();
            $selected_claims = isset($res[0]['id_str']) ? $res[0]['id_str'] : '';
        }
        if (isset($selected_claims) && $selected_claims) {
            $results = PlotModel::whereRaw("claim_id in ($selected_claims)")->where(['vil_id' => $posted_data['village']])->orderBy('plt_number', 'asc')->orderBy('ogc_fid', 'desc')->paginate(50);
        } else {
            $results = PlotModel::orderBy('ogc_fid', 'desc')->paginate(50);
        }
        $results->appends($posted_data);
        $links = $results->render();
        $results = $results->toArray();
        $plots = isset($results['data']) ? $results['data'] : '';
        //$this->pre($plots);exit;
        $this->appData['plots'] = $plots;
        $this->appData['links'] = $links;
        return view('managerecords.plot.list', $this->appData);
    }

    public function processData(Request $request) {
        $posted_data = $request->all();
        $posted_plot_data = isset($posted_data['plot']) ? $posted_data['plot'] : '';
        $id = $request->get('id');
        if ($id) {
            $res = PlotModel::select('is_old')->where(['ogc_fid' => $id])->get()->toArray();
        }
        if (isset($res[0]['is_old']) && $res[0]['is_old']) {
            $is_old_value = true;
            $claim_field = 'claim';
        } else {
            $is_old_value = false;
            $claim_field = 'claim_id';
        }

        $messages = [
            'plot.plt_number.required' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>The Plot No. field is required!</strong></div>',
            'plot.surveyno.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Survey No should be integer value!</strong></div>',
            'plot.compartment_no.integer' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Compartment  No should be integer value!</strong></div>',
            'plot.areaclaimedh.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Claimed Hectare should be numeric value!</strong></div>',
            'plot.areaapprovedgsh.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Approved GS Hectare should be numeric value!</strong></div>',
            'plot.areaapprovedsdlch.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Approved SDLC Hectare should be integer value!</strong></div>',
            'plot.areaapproveddlch.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area Approved DLC Hectare should be integer value!</strong></div>',
            'plot.areagpssurveyh.numeric' => '<div class="alert alert-danger"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Area GPS Survey Hectare  should be integer value!</strong></div>',
        ];
        $rules = [
            'plot.plt_number' => 'required',
            'plot.surveyno' => 'integer',
            'plot.compartment_no' => 'integer',
            'plot.areaclaimedh' => 'numeric',
            'plot.areaapprovedgsh' => 'numeric',
            'plot.areaapprovedsdlch' => 'numeric',
            'plot.areaapproveddlch' => 'numeric',
            'plot.areagpssurveyh' => 'numeric',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        $village_id = isset($posted_plot_data['vil_id']) ? $posted_plot_data['vil_id'] : '';
        $village_old_id = ($village_id) ? VillageModel::where(['vil_id' => "$village_id"])->value('old_id') : '';
        $claim_id = isset($posted_plot_data['claim_id']) ? $posted_plot_data['claim_id'] : '';
        $old_claim_id = ($claim_id) ? ClaimModel::where(['id' => $claim_id])->value('old_id') : '';

        if ($id) {
            if ($validator->fails()) {
                return redirect('plot/update/' . $id)
                                ->withErrors($validator, 'plot')
                                ->withInput();
            }
            $data_to_update = $posted_plot_data;
            $data_to_update['claim'] = $old_claim_id;
            $data_to_update['village'] = $village_old_id;
            $data_to_update['is_old'] = $is_old_value;
            $data_to_update['updated_at'] = Carbon::now();
            $upadte_data = PlotModel::where(['ogc_fid' => $id])->update($data_to_update);
            $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Plot updated successfully!</strong></div>';
            return redirect('plot/update/' . $id)->with('message', $msg);
        } else {
            if ($validator->fails()) {
                return redirect('plot/add')
                                ->withErrors($validator, 'plot')
                                ->withInput();
            }
            $data_to_insert = $posted_plot_data;
            $data_to_insert['claim'] = $old_claim_id;
            $data_to_insert['village'] = $village_old_id;
            $data_to_insert['is_old'] = $is_old_value;
            $data_to_insert['created_at'] = Carbon::now();
            $data_to_insert['updated_at'] = Carbon::now();
            // $this->pre($data_to_insert);exit;
            $insert_id = PlotModel::insert($data_to_insert);
            //$this->pre($insert_id);exit;
            if ($insert_id) {
                $msg = '<div class="alert alert-success"><a href="javascript:void(0);" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Plot added successfully!</strong></div>';
                return redirect('plot/list')->with('message', $msg);
            }
        }
    }

}
