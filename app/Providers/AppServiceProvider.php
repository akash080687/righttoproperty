<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;
use App\Models\Report\ClaimantModel;
use App\Models\Report\ClaimModel;
use Auth;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        Validator::extend('valid_claimant', function($field, $value, $parameters) {
            // echo $value;
            //$res = ClaimantModel::select('id')->where(['is_old' => true, 'old_id' => "$value"])->orWhereRaw("(is_old is false and id=$value)")->get()->toArray();
            $res = ClaimantModel::select('id')->where(['is_old' => true, 'old_id' => "$value"])->get()->toArray();
            if (empty($res)) {
                $res = ClaimantModel::select('id')->where(['is_old' => FALSE, 'id' => "$value"])->get()->toArray();
            }
            if (empty($res)) {
                $res = ClaimantModel::select('id')->where(['is_old' => true, 'id' => "$value"])->get()->toArray();
            }

            //->orWhere(['is_old' => FALSE, 'id' => "$value"])->orWhere(['is_old' => true, 'id' => "$value"])->get()->toArray();           
//            t($res);
//            exit;
            if (!empty($res)) {
                return true;
            } else {
                return false;
            }
        });
        Validator::extend('valid_claim', function($field, $value, $parameters) {
//            $res = ClaimModel::select('id')->where(['is_old' => true, 'old_id' => "$value"])->orWhereRaw("(is_old is not true and id=$value)")
//                            ->get()->toArray();

            $res = ClaimModel::select('id')->where(['is_old' => true, 'old_id' => "$value"])->get()->toArray();
            if (empty($res)) {
                $res = ClaimModel::select('id')->where(['is_old' => FALSE, 'id' => "$value"])->get()->toArray();
            }
            if (empty($res)) {
                $res = ClaimModel::select('id')->where(['is_old' => true, 'id' => "$value"])->get()->toArray();
            }
            if (!empty($res)) {
                return true;
            } else {
                return false;
            }
        });

        Validator::extend('validate_state', function($field, $value, $parameters) {
            if (Auth::check()) {
                $user = Auth::user();
                $user_boundary_state_id = $user->user_boubdary_id;
                $user_boundary_type = $user->user_boundary;
                if ($user_boundary_type != 'all') {
                    if ($user_boundary_state_id == $value) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            }
        });

        Validator::extend('validate_old_claimant', function($field, $value, $parameters) {
//            t($parameters);
//            exit;
            $mode = isset($parameters[0]) ? $parameters[0] : '';
            $existing_old_id = isset($parameters[1]) ? $parameters[1] : '';
            //$res = ClaimantModel::select('id')->where(['is_old' => true, 'old_id' => "$value"])->orWhereRaw("(is_old is false and id=$value)")->get()->toArray();
            $res = ClaimantModel::where(['old_id' => "$value"])->get()->toArray();
            if ($mode == 'add') {
                if (!empty($res)) {
                    return FALSE;
                } else {
                    return TRUE;
                }
            } else if ($mode == 'edit') {
                //$existing_old_id = ClaimantModel::select('old_id')
                if ($value != $existing_old_id) {
                  //  exit;
                    $res = ClaimantModel::where(['old_id' => "$value"])->get()->toArray();
                   // t($res);exit;
                    if (!empty($res)) {
                        return FALSE;
                    } else {
                        return TRUE;
                    }
                }
                return TRUE;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
