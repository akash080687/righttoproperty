<?php

namespace App\Models\Report;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class BlockModel extends BaseModel {

    protected $table = null;

    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_BLOCK'];
    }
    
     public function villages(){
        return $this->hasMany('App\Models\Report\VillageModel', 'block_id');
    }
    
     public function getDistrict(){
         return $this->belongsTo('App\Models\Report\DistrictModel', 'dist_id');
    }


}
