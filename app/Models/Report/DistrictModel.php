<?php

namespace App\Models\Report;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class DistrictModel extends BaseModel {

    protected $table = null;
    protected $primaryKey = 'ogc_fid';
    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_DISTRICT_MASTER'];
    }
    
     public function blocks(){
        return $this->hasMany('App\Models\Report\BlockModel', 'dist_id','ogc_fid')->where(['is_rtp' => TRUE])->whereRaw(" rtp_status is not FALSE ");
    }
     public function blocksMaster(){
        return $this->hasMany('App\Models\Report\BlockModel', 'dist_id','ogc_fid')->whereRaw("status is not false ");
    }
    
    public function getState(){
         return $this->belongsTo('App\Models\Report\StateModel', 'dist_id','stt_state_id');
    }

}
