<?php

namespace App\Models\Report;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class ClaimModel extends BaseModel {

    protected $table = null;

    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_CLAIM'];
    }

    public function plots() {
        return $this->hasMany('App\Models\Report\PlotModel', 'claim_id')->orderBy('plt_number','ASC');
    }

}
