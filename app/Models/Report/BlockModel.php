<?php

namespace App\Models\Report;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class BlockModel extends BaseModel {

    protected $table = null;
    protected $primaryKey = 'ogc_fid';
    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_BLOCK_MASTER'];
    }
    
     public function villages(){
        return $this->hasMany('App\Models\Report\VillageModel', 'block_id','ogc_fid')->where(['is_rtp' => TRUE])->whereRaw(" rtp_status is not FALSE ");
    }
     public function villagesForRtp(){
        return $this->hasMany('App\Models\Report\VillageModel', 'block_id','ogc_fid')->where(['is_rtp' => TRUE]);
    }
    
     public function villagesMaster(){
        return $this->hasMany('App\Models\Report\VillageModel', 'block_id','ogc_fid')->whereRaw(" status is not FALSE ");
    }
    
     public function getDistrict(){
         return $this->belongsTo('App\Models\Report\DistrictModel', 'dist_id');
    }


}
