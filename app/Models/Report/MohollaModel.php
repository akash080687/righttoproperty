<?php

namespace App\Models\Report;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class MohollaModel extends BaseModel {

    protected $table = null;
    protected $primaryKey = 'ogc_fid';

    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_MOHOLLA_MASTER'];
    }

    public function claimants() {
      //  return $this->hasMany('App\Models\Report\ClaimantModel', 'village_ogc_id')->orderBy('id', 'ASC');
        return $this->hasMany('App\Models\Report\ClaimantModel', 'village_ogc_fid_master')->orderBy('id', 'ASC');
    }

    public function getBlock() {
        return $this->belongsTo('App\Models\Report\BlockModel', 'block_id');
    }

    public function claims() {
        return $this->hasMany('App\Models\Report\ClaimModel', 'village_id', 'vil_id')->orderBy('id', 'ASC');
    }

}
