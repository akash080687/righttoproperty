<?php

namespace App\Models\Report;

use App\Models\BaseModel;
use DB;
use Input;
use Config;

class DistrictModel extends BaseModel {

    protected $table = null;

    public function __construct() {
        parent::__construct();
        $this->table = $this->appData['tables']['TABLE_DISTRICT'];
    }
    
     public function blocks(){
        return $this->hasMany('App\Models\Report\BlockModel', 'dist_id');
    }
    
    public function getState(){
         return $this->belongsTo('App\Models\Report\StateModel', 'dist_id','stt_state_id');
    }

}
