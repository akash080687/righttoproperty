<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use Config;

use Illuminate\Foundation\Auth\User as Authenticatable;
class RtpUserModel extends Authenticatable {

    protected $table = null;

    public function __construct() {
        parent::__construct();
 $this->table = 'RTP_USER';
    }

}
